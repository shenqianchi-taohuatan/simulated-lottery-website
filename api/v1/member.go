package v1

import "github.com/gogf/gf/v2/frame/g"

// 更新会员信息
type UpdatePersonReq struct {
	g.Meta `path:"/updatePerson" method:"POST"`
}
