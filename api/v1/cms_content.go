package v1

import "github.com/gogf/gf/v2/frame/g"

// 接口请求
type IndexReq struct {
	g.Meta `path:"/index" method:"GET"`
}

type Index2Req struct {
	g.Meta `path:"/index2" method:"GET"`
}

type Index3Req struct {
	g.Meta `path:"/index3" method:"GET"`
}

type GenerateNumberReq struct {
	g.Meta `path:"/generateNumber" method:"POST"`
}

type GetNowLotteryReq struct {
	g.Meta `path:"/getNowLottery" method:"POST"`
}
