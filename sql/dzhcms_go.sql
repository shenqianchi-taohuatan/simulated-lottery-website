/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 50742 (5.7.42)
 Source Host           : localhost:3306
 Source Schema         : 3114_go

 Target Server Type    : MySQL
 Target Server Version : 50742 (5.7.42)
 File Encoding         : 65001

 Date: 26/05/2023 05:36:37
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for base_eps_admin
-- ----------------------------
DROP TABLE IF EXISTS `base_eps_admin`;
CREATE TABLE `base_eps_admin` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `module` longtext,
  `method` longtext,
  `path` longtext,
  `prefix` longtext,
  `summary` longtext,
  `tag` longtext,
  `dts` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9271 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of base_eps_admin
-- ----------------------------
BEGIN;
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9170, 'base', 'POST', '/logout', '/admin/base/comm', 'logout', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9171, 'base', 'GET', '/permmenu', '/admin/base/comm', 'permmenu', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9172, 'base', 'GET', '/person', '/admin/base/comm', 'person', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9173, 'base', 'POST', '/personUpdate', '/admin/base/comm', 'personUpdate', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9174, 'base', 'POST', '/upload', '/admin/base/comm', 'upload', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9175, 'base', 'GET', '/uploadMode', '/admin/base/comm', 'uploadMode', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9176, 'base', 'GET', '/captcha', '/admin/base/open', 'captcha', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9177, 'base', 'GET', '/eps', '/admin/base/open', 'eps', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9178, 'base', 'POST', '/login', '/admin/base/open', 'login', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9179, 'base', 'GET', '/refreshToken', '/admin/base/open', 'refreshToken', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9180, 'base', 'POST', '/add', '/admin/base/sys/department', 'add', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9181, 'base', 'POST', '/delete', '/admin/base/sys/department', 'delete', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9182, 'base', 'GET', '/info', '/admin/base/sys/department', 'info', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9183, 'base', 'POST', '/list', '/admin/base/sys/department', 'list', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9184, 'base', 'GET', '/order', '/admin/base/sys/department', 'order', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9185, 'base', 'POST', '/page', '/admin/base/sys/department', 'page', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9186, 'base', 'POST', '/update', '/admin/base/sys/department', 'update', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9187, 'base', 'POST', '/add', '/admin/base/sys/log', 'add', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9188, 'base', 'POST', '/clear', '/admin/base/sys/log', 'clear', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9189, 'base', 'POST', '/delete', '/admin/base/sys/log', 'delete', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9190, 'base', 'GET', '/getKeep', '/admin/base/sys/log', 'getKeep', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9191, 'base', 'GET', '/info', '/admin/base/sys/log', 'info', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9192, 'base', 'POST', '/list', '/admin/base/sys/log', 'list', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9193, 'base', 'POST', '/page', '/admin/base/sys/log', 'page', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9194, 'base', 'POST', '/setKeep', '/admin/base/sys/log', 'setKeep', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9195, 'base', 'POST', '/update', '/admin/base/sys/log', 'update', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9196, 'base', 'POST', '/add', '/admin/base/sys/menu', 'add', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9197, 'base', 'POST', '/delete', '/admin/base/sys/menu', 'delete', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9198, 'base', 'GET', '/info', '/admin/base/sys/menu', 'info', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9199, 'base', 'POST', '/list', '/admin/base/sys/menu', 'list', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9200, 'base', 'POST', '/page', '/admin/base/sys/menu', 'page', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9201, 'base', 'POST', '/update', '/admin/base/sys/menu', 'update', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9202, 'base', 'POST', '/add', '/admin/base/sys/param', 'add', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9203, 'base', 'POST', '/delete', '/admin/base/sys/param', 'delete', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9204, 'base', 'GET', '/html', '/admin/base/sys/param', 'html', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9205, 'base', 'GET', '/info', '/admin/base/sys/param', 'info', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9206, 'base', 'POST', '/list', '/admin/base/sys/param', 'list', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9207, 'base', 'POST', '/page', '/admin/base/sys/param', 'page', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9208, 'base', 'POST', '/update', '/admin/base/sys/param', 'update', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9209, 'base', 'POST', '/add', '/admin/base/sys/role', 'add', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9210, 'base', 'POST', '/delete', '/admin/base/sys/role', 'delete', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9211, 'base', 'GET', '/info', '/admin/base/sys/role', 'info', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9212, 'base', 'POST', '/list', '/admin/base/sys/role', 'list', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9213, 'base', 'POST', '/page', '/admin/base/sys/role', 'page', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9214, 'base', 'POST', '/update', '/admin/base/sys/role', 'update', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9215, 'base', 'POST', '/add', '/admin/base/sys/user', 'add', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9216, 'base', 'POST', '/delete', '/admin/base/sys/user', 'delete', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9217, 'base', 'GET', '/info', '/admin/base/sys/user', 'info', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9218, 'base', 'POST', '/list', '/admin/base/sys/user', 'list', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9219, 'base', 'GET', '/move', '/admin/base/sys/user', 'move', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9220, 'base', 'POST', '/page', '/admin/base/sys/user', 'page', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9221, 'base', 'POST', '/update', '/admin/base/sys/user', 'update', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9222, 'cms', 'POST', '/add', '/admin/cms/form', 'add', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9223, 'cms', 'POST', '/delete', '/admin/cms/form', 'delete', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9224, 'cms', 'GET', '/info', '/admin/cms/form', 'info', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9225, 'cms', 'POST', '/list', '/admin/cms/form', 'list', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9226, 'cms', 'POST', '/page', '/admin/cms/form', 'page', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9227, 'cms', 'POST', '/update', '/admin/cms/form', 'update', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9228, 'cms', 'GET', '/welcome', '/admin/cms/form', 'welcome', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9229, 'dict', 'POST', '/add', '/admin/dict/info', 'add', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9230, 'dict', 'POST', '/data', '/admin/dict/info', 'data', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9231, 'dict', 'POST', '/delete', '/admin/dict/info', 'delete', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9232, 'dict', 'GET', '/info', '/admin/dict/info', 'info', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9233, 'dict', 'POST', '/list', '/admin/dict/info', 'list', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9234, 'dict', 'POST', '/page', '/admin/dict/info', 'page', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9235, 'dict', 'POST', '/update', '/admin/dict/info', 'update', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9236, 'dict', 'POST', '/add', '/admin/dict/type', 'add', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9237, 'dict', 'POST', '/delete', '/admin/dict/type', 'delete', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9238, 'dict', 'GET', '/info', '/admin/dict/type', 'info', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9239, 'dict', 'POST', '/list', '/admin/dict/type', 'list', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9240, 'dict', 'POST', '/page', '/admin/dict/type', 'page', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9241, 'dict', 'POST', '/update', '/admin/dict/type', 'update', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9242, 'member', 'POST', '/add', '/admin/member/user', 'add', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9243, 'member', 'POST', '/delete', '/admin/member/user', 'delete', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9244, 'member', 'GET', '/info', '/admin/member/user', 'info', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9245, 'member', 'POST', '/list', '/admin/member/user', 'list', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9246, 'member', 'POST', '/page', '/admin/member/user', 'page', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9247, 'member', 'POST', '/update', '/admin/member/user', 'update', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9248, 'member', 'GET', '/welcome', '/admin/member/user', 'welcome', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9249, 'space', 'POST', '/add', '/admin/space/info', 'add', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9250, 'space', 'POST', '/delete', '/admin/space/info', 'delete', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9251, 'space', 'GET', '/info', '/admin/space/info', 'info', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9252, 'space', 'POST', '/list', '/admin/space/info', 'list', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9253, 'space', 'POST', '/page', '/admin/space/info', 'page', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9254, 'space', 'POST', '/update', '/admin/space/info', 'update', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9255, 'space', 'POST', '/add', '/admin/space/type', 'add', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9256, 'space', 'POST', '/delete', '/admin/space/type', 'delete', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9257, 'space', 'GET', '/info', '/admin/space/type', 'info', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9258, 'space', 'POST', '/list', '/admin/space/type', 'list', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9259, 'space', 'POST', '/page', '/admin/space/type', 'page', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9260, 'space', 'POST', '/update', '/admin/space/type', 'update', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9261, 'task', 'POST', '/add', '/admin/task/info', 'add', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9262, 'task', 'POST', '/delete', '/admin/task/info', 'delete', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9263, 'task', 'GET', '/info', '/admin/task/info', 'info', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9264, 'task', 'POST', '/list', '/admin/task/info', 'list', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9265, 'task', 'GET', '/log', '/admin/task/info', 'log', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9266, 'task', 'POST', '/once', '/admin/task/info', 'once', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9267, 'task', 'POST', '/page', '/admin/task/info', 'page', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9268, 'task', 'GET', '/start', '/admin/task/info', 'start', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9269, 'task', 'GET', '/stop', '/admin/task/info', 'stop', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (9270, 'task', 'POST', '/update', '/admin/task/info', 'update', '', '');
COMMIT;

-- ----------------------------
-- Table structure for base_eps_app
-- ----------------------------
DROP TABLE IF EXISTS `base_eps_app`;
CREATE TABLE `base_eps_app` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `module` longtext,
  `method` longtext,
  `path` longtext,
  `prefix` longtext,
  `summary` longtext,
  `tag` longtext,
  `dts` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of base_eps_app
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for base_sys_conf
-- ----------------------------
DROP TABLE IF EXISTS `base_sys_conf`;
CREATE TABLE `base_sys_conf` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `cKey` varchar(255) NOT NULL,
  `cValue` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_base_sys_conf_deleted_at` (`deleted_at`),
  KEY `idx_base_sys_conf_c_key` (`cKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of base_sys_conf
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for base_sys_department
-- ----------------------------
DROP TABLE IF EXISTS `base_sys_department`;
CREATE TABLE `base_sys_department` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `parentId` bigint(20) DEFAULT NULL,
  `orderNum` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_base_sys_department_deleted_at` (`deleted_at`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of base_sys_department
-- ----------------------------
BEGIN;
INSERT INTO `base_sys_department` (`id`, `createTime`, `updateTime`, `deleted_at`, `name`, `parentId`, `orderNum`) VALUES (1, '2023-05-16 15:50:22.199', '2023-05-19 03:43:34.751', NULL, '大智汇', NULL, 0);
INSERT INTO `base_sys_department` (`id`, `createTime`, `updateTime`, `deleted_at`, `name`, `parentId`, `orderNum`) VALUES (13, '2023-05-16 15:50:22.199', '2023-05-19 03:43:04.053', NULL, '行政', 1, 0);
INSERT INTO `base_sys_department` (`id`, `createTime`, `updateTime`, `deleted_at`, `name`, `parentId`, `orderNum`) VALUES (14, '2023-05-26 05:32:51.128', '2023-05-26 05:32:51.128', NULL, '普通管理员', 1, 2);
COMMIT;

-- ----------------------------
-- Table structure for base_sys_init
-- ----------------------------
DROP TABLE IF EXISTS `base_sys_init`;
CREATE TABLE `base_sys_init` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `table` varchar(191) NOT NULL,
  `group` varchar(191) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_base_sys_init_table` (`table`),
  KEY `idx_base_sys_init_group` (`group`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of base_sys_init
-- ----------------------------
BEGIN;
INSERT INTO `base_sys_init` (`id`, `table`, `group`) VALUES (1, 'base_sys_menu', 'default');
INSERT INTO `base_sys_init` (`id`, `table`, `group`) VALUES (2, 'base_sys_user', 'default');
INSERT INTO `base_sys_init` (`id`, `table`, `group`) VALUES (3, 'base_sys_user_role', 'default');
INSERT INTO `base_sys_init` (`id`, `table`, `group`) VALUES (4, 'base_sys_role', 'default');
INSERT INTO `base_sys_init` (`id`, `table`, `group`) VALUES (5, 'base_sys_role_menu', 'default');
INSERT INTO `base_sys_init` (`id`, `table`, `group`) VALUES (6, 'base_sys_department', 'default');
INSERT INTO `base_sys_init` (`id`, `table`, `group`) VALUES (7, 'base_sys_role_department', 'default');
INSERT INTO `base_sys_init` (`id`, `table`, `group`) VALUES (8, 'base_sys_param', 'default');
INSERT INTO `base_sys_init` (`id`, `table`, `group`) VALUES (9, 'dict_info', 'default');
INSERT INTO `base_sys_init` (`id`, `table`, `group`) VALUES (10, 'dict_type', 'default');
INSERT INTO `base_sys_init` (`id`, `table`, `group`) VALUES (11, 'task_info', 'default');
COMMIT;

-- ----------------------------
-- Table structure for base_sys_log
-- ----------------------------
DROP TABLE IF EXISTS `base_sys_log`;
CREATE TABLE `base_sys_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `userId` bigint(20) unsigned DEFAULT NULL,
  `action` varchar(191) NOT NULL,
  `ip` varchar(191) DEFAULT NULL,
  `ipAddr` varchar(191) DEFAULT NULL,
  `params` longtext,
  PRIMARY KEY (`id`),
  KEY `IDX_24e18767659f8c7142580893f2` (`ip`),
  KEY `IDX_a03a27f75cf8d502b3060823e1` (`ipAddr`),
  KEY `idx_base_sys_log_deleted_at` (`deleted_at`),
  KEY `IDX_51a2caeb5713efdfcb343a8772` (`userId`),
  KEY `IDX_938f886fb40e163db174b7f6c3` (`action`)
) ENGINE=InnoDB AUTO_INCREMENT=2257 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of base_sys_log
-- ----------------------------
BEGIN;
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1356, '2023-05-23 03:02:50.067', '2023-05-23 03:02:50.067', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1357, '2023-05-23 03:03:42.909', '2023-05-23 03:03:42.909', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1358, '2023-05-23 03:03:45.736', '2023-05-23 03:03:45.736', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1359, '2023-05-23 03:03:46.422', '2023-05-23 03:03:46.422', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1360, '2023-05-23 03:03:46.423', '2023-05-23 03:03:46.423', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1361, '2023-05-23 03:03:46.565', '2023-05-23 03:03:46.565', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1362, '2023-05-23 03:03:47.559', '2023-05-23 03:03:47.559', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1363, '2023-05-23 03:03:55.084', '2023-05-23 03:03:55.084', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1364, '2023-05-23 03:06:17.953', '2023-05-23 03:06:17.953', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1365, '2023-05-23 03:06:35.376', '2023-05-23 03:06:35.376', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1366, '2023-05-23 03:09:04.421', '2023-05-23 03:09:04.421', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1367, '2023-05-23 03:09:05.029', '2023-05-23 03:09:05.029', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1368, '2023-05-23 03:09:05.029', '2023-05-23 03:09:05.029', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1369, '2023-05-23 03:09:05.125', '2023-05-23 03:09:05.125', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1370, '2023-05-23 03:09:05.903', '2023-05-23 03:09:05.903', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1371, '2023-05-23 03:10:41.396', '2023-05-23 03:10:41.396', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1372, '2023-05-23 03:10:41.757', '2023-05-23 03:10:41.757', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1373, '2023-05-23 03:10:41.757', '2023-05-23 03:10:41.757', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1374, '2023-05-23 03:10:41.880', '2023-05-23 03:10:41.880', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1375, '2023-05-23 03:10:43.561', '2023-05-23 03:10:43.561', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1376, '2023-05-23 03:10:46.315', '2023-05-23 03:10:46.315', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1377, '2023-05-23 03:10:46.629', '2023-05-23 03:10:46.629', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1378, '2023-05-23 03:10:46.629', '2023-05-23 03:10:46.629', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1379, '2023-05-23 03:10:46.685', '2023-05-23 03:10:46.685', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1380, '2023-05-23 03:10:48.471', '2023-05-23 03:10:48.471', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1381, '2023-05-23 03:11:50.398', '2023-05-23 03:11:50.398', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1382, '2023-05-23 03:11:50.906', '2023-05-23 03:11:50.906', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1383, '2023-05-23 03:11:50.906', '2023-05-23 03:11:50.906', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1384, '2023-05-23 03:11:51.006', '2023-05-23 03:11:51.006', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1385, '2023-05-23 03:11:51.856', '2023-05-23 03:11:51.856', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1386, '2023-05-23 03:13:15.220', '2023-05-23 03:13:15.220', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1387, '2023-05-23 03:13:48.483', '2023-05-23 03:13:48.483', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1388, '2023-05-23 03:14:15.658', '2023-05-23 03:14:15.658', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1389, '2023-05-23 03:15:15.522', '2023-05-23 03:15:15.522', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1390, '2023-05-23 03:15:15.951', '2023-05-23 03:15:15.951', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1391, '2023-05-23 03:15:15.952', '2023-05-23 03:15:15.952', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1392, '2023-05-23 03:15:16.053', '2023-05-23 03:15:16.053', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1393, '2023-05-23 03:15:16.851', '2023-05-23 03:15:16.851', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1394, '2023-05-23 03:15:31.037', '2023-05-23 03:15:31.037', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1395, '2023-05-23 03:15:31.522', '2023-05-23 03:15:31.522', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1396, '2023-05-23 03:15:31.522', '2023-05-23 03:15:31.522', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1397, '2023-05-23 03:15:31.612', '2023-05-23 03:15:31.612', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1398, '2023-05-23 03:15:32.399', '2023-05-23 03:15:32.399', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1399, '2023-05-23 03:15:46.441', '2023-05-23 03:15:46.441', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1400, '2023-05-23 03:15:46.969', '2023-05-23 03:15:46.969', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1401, '2023-05-23 03:15:46.969', '2023-05-23 03:15:46.969', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1402, '2023-05-23 03:15:47.027', '2023-05-23 03:15:47.027', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1403, '2023-05-23 03:15:47.899', '2023-05-23 03:15:47.899', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1404, '2023-05-23 03:18:01.882', '2023-05-23 03:18:01.882', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1405, '2023-05-23 03:18:02.266', '2023-05-23 03:18:02.266', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1406, '2023-05-23 03:18:02.266', '2023-05-23 03:18:02.266', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1407, '2023-05-23 03:18:02.359', '2023-05-23 03:18:02.359', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1408, '2023-05-23 03:18:03.151', '2023-05-23 03:18:03.151', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1409, '2023-05-23 03:18:04.979', '2023-05-23 03:18:04.979', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":16}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1410, '2023-05-23 03:19:21.752', '2023-05-23 03:19:21.752', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":16}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1411, '2023-05-23 03:19:27.933', '2023-05-23 03:19:27.933', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1412, '2023-05-23 03:19:28.454', '2023-05-23 03:19:28.454', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1413, '2023-05-23 03:19:28.454', '2023-05-23 03:19:28.454', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1414, '2023-05-23 03:19:28.552', '2023-05-23 03:19:28.552', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1415, '2023-05-23 03:19:29.377', '2023-05-23 03:19:29.377', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1416, '2023-05-23 03:19:34.463', '2023-05-23 03:19:34.463', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":16}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1417, '2023-05-23 03:24:36.729', '2023-05-23 03:24:36.729', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1418, '2023-05-23 03:24:37.080', '2023-05-23 03:24:37.080', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1419, '2023-05-23 03:24:37.080', '2023-05-23 03:24:37.080', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1420, '2023-05-23 03:24:37.267', '2023-05-23 03:24:37.267', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1421, '2023-05-23 03:24:38.164', '2023-05-23 03:24:38.164', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1422, '2023-05-23 03:24:42.412', '2023-05-23 03:24:42.412', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":16}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1423, '2023-05-23 03:39:24.063', '2023-05-23 03:39:24.063', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1424, '2023-05-23 03:39:24.608', '2023-05-23 03:39:24.608', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1425, '2023-05-23 03:39:24.608', '2023-05-23 03:39:24.608', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1426, '2023-05-23 03:39:24.677', '2023-05-23 03:39:24.677', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1427, '2023-05-23 03:39:25.494', '2023-05-23 03:39:25.494', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1428, '2023-05-23 03:43:52.041', '2023-05-23 03:43:52.041', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1429, '2023-05-23 03:43:52.988', '2023-05-23 03:43:52.988', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1430, '2023-05-23 03:43:52.988', '2023-05-23 03:43:52.988', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1431, '2023-05-23 03:43:53.037', '2023-05-23 03:43:53.037', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1432, '2023-05-23 03:43:54.438', '2023-05-23 03:43:54.438', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1433, '2023-05-23 03:44:04.401', '2023-05-23 03:44:04.401', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":16}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1434, '2023-05-23 03:44:49.953', '2023-05-23 03:44:49.953', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1435, '2023-05-23 03:44:50.345', '2023-05-23 03:44:50.345', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1436, '2023-05-23 03:44:50.346', '2023-05-23 03:44:50.346', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1437, '2023-05-23 03:44:50.551', '2023-05-23 03:44:50.551', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1438, '2023-05-23 03:44:51.488', '2023-05-23 03:44:51.488', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1439, '2023-05-23 03:44:53.243', '2023-05-23 03:44:53.243', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":16}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1440, '2023-05-23 03:46:16.715', '2023-05-23 03:46:16.715', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":18}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1441, '2023-05-23 03:46:26.637', '2023-05-23 03:46:26.637', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":18}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1442, '2023-05-23 03:46:30.702', '2023-05-23 03:46:30.702', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1443, '2023-05-23 03:46:31.170', '2023-05-23 03:46:31.170', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1444, '2023-05-23 03:46:31.170', '2023-05-23 03:46:31.170', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1445, '2023-05-23 03:46:31.437', '2023-05-23 03:46:31.437', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1446, '2023-05-23 03:46:32.294', '2023-05-23 03:46:32.294', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1447, '2023-05-23 03:46:35.420', '2023-05-23 03:46:35.420', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":16}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1448, '2023-05-23 03:46:49.837', '2023-05-23 03:46:49.837', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":17}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1449, '2023-05-23 03:46:55.615', '2023-05-23 03:46:55.615', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":18}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1450, '2023-05-23 04:33:20.757', '2023-05-23 04:33:20.757', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1451, '2023-05-23 04:33:21.008', '2023-05-23 04:33:21.008', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1452, '2023-05-23 04:33:21.008', '2023-05-23 04:33:21.008', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1453, '2023-05-23 04:33:21.040', '2023-05-23 04:33:21.040', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1454, '2023-05-23 04:33:23.239', '2023-05-23 04:33:23.239', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1455, '2023-05-23 08:08:04.078', '2023-05-23 08:08:04.078', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1456, '2023-05-23 08:08:05.090', '2023-05-23 08:08:05.090', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1457, '2023-05-23 08:08:05.091', '2023-05-23 08:08:05.091', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1458, '2023-05-23 08:08:05.250', '2023-05-23 08:08:05.250', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1459, '2023-05-23 08:08:09.686', '2023-05-23 08:08:09.686', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1460, '2023-05-23 09:08:15.456', '2023-05-23 09:08:15.456', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1461, '2023-05-23 09:08:15.795', '2023-05-23 09:08:15.795', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1462, '2023-05-23 09:08:15.796', '2023-05-23 09:08:15.796', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1463, '2023-05-23 09:08:15.838', '2023-05-23 09:08:15.838', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1464, '2023-05-23 09:08:17.101', '2023-05-23 09:08:17.101', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1465, '2023-05-23 12:15:53.290', '2023-05-23 12:15:53.290', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":16}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1466, '2023-05-23 12:29:42.544', '2023-05-23 12:29:42.544', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1467, '2023-05-23 12:29:43.614', '2023-05-23 12:29:43.614', NULL, 0, 'GET:/admin/base/open/captcha', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1468, '2023-05-23 12:29:46.806', '2023-05-23 12:29:46.806', NULL, 0, 'POST:/admin/base/open/login', '127.0.0.1', '127.0.0.1', '{\"username\":\"admin\",\"password\":\"adminwanyu\",\"captchaId\":\"p01cuu0ike0cstdhpn7ryog200eg4xt5\",\"verifyCode\":\"7913\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1469, '2023-05-23 12:29:46.837', '2023-05-23 12:29:46.837', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1470, '2023-05-23 12:29:46.838', '2023-05-23 12:29:46.838', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1471, '2023-05-23 12:29:46.838', '2023-05-23 12:29:46.838', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1472, '2023-05-23 12:29:54.398', '2023-05-23 12:29:54.398', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1473, '2023-05-23 12:29:56.353', '2023-05-23 12:29:56.353', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":16}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1474, '2023-05-23 13:16:42.975', '2023-05-23 13:16:42.975', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1475, '2023-05-23 13:16:43.404', '2023-05-23 13:16:43.404', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1476, '2023-05-23 13:16:43.404', '2023-05-23 13:16:43.404', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1477, '2023-05-23 13:16:43.431', '2023-05-23 13:16:43.431', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1478, '2023-05-23 13:16:45.013', '2023-05-23 13:16:45.013', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1479, '2023-05-23 16:07:52.597', '2023-05-23 16:07:52.597', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1480, '2023-05-23 16:07:53.238', '2023-05-23 16:07:53.238', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1481, '2023-05-23 16:07:53.238', '2023-05-23 16:07:53.238', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1482, '2023-05-23 16:07:53.482', '2023-05-23 16:07:53.482', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1483, '2023-05-23 16:08:01.566', '2023-05-23 16:08:01.566', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1484, '2023-05-23 16:08:08.027', '2023-05-23 16:08:08.027', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":16}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1485, '2023-05-23 16:12:42.930', '2023-05-23 16:12:42.930', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":16}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1486, '2023-05-23 16:12:56.094', '2023-05-23 16:12:56.094', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1487, '2023-05-23 16:12:56.824', '2023-05-23 16:12:56.824', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1488, '2023-05-23 16:12:56.824', '2023-05-23 16:12:56.824', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1489, '2023-05-23 16:12:56.854', '2023-05-23 16:12:56.854', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1490, '2023-05-23 16:12:58.206', '2023-05-23 16:12:58.206', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1491, '2023-05-23 16:13:01.424', '2023-05-23 16:13:01.424', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":16}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1492, '2023-05-23 16:13:44.905', '2023-05-23 16:13:44.905', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":16}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1493, '2023-05-23 16:14:25.003', '2023-05-23 16:14:25.003', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1494, '2023-05-23 17:52:08.930', '2023-05-23 17:52:08.930', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1495, '2023-05-23 17:52:09.233', '2023-05-23 17:52:09.233', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1496, '2023-05-23 17:52:09.234', '2023-05-23 17:52:09.234', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1497, '2023-05-23 17:52:09.306', '2023-05-23 17:52:09.306', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1498, '2023-05-23 17:52:11.010', '2023-05-23 17:52:11.010', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1499, '2023-05-23 20:23:44.383', '2023-05-23 20:23:44.383', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":16}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1500, '2023-05-23 20:23:45.785', '2023-05-23 20:23:45.785', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1501, '2023-05-23 20:25:27.896', '2023-05-23 20:25:27.896', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1502, '2023-05-23 20:25:28.209', '2023-05-23 20:25:28.209', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1503, '2023-05-23 20:25:28.209', '2023-05-23 20:25:28.209', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1504, '2023-05-23 20:25:28.343', '2023-05-23 20:25:28.343', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1505, '2023-05-23 20:25:28.959', '2023-05-23 20:25:28.959', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1506, '2023-05-23 20:25:31.000', '2023-05-23 20:25:31.000', NULL, 1, 'POST:/admin/base/comm/logout', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1507, '2023-05-23 20:25:31.203', '2023-05-23 20:25:31.203', NULL, 0, 'GET:/admin/base/open/captcha', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1508, '2023-05-23 20:25:34.847', '2023-05-23 20:25:34.847', NULL, 0, 'POST:/admin/base/open/login', '127.0.0.1', '127.0.0.1', '{\"username\":\"admin\",\"password\":\"adminwanyu\",\"captchaId\":\"10e8uks4up0cstnm084zzww200af333k\",\"verifyCode\":\"3223\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1509, '2023-05-23 20:25:34.862', '2023-05-23 20:25:34.862', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1510, '2023-05-23 20:25:34.863', '2023-05-23 20:25:34.863', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1511, '2023-05-23 20:25:34.864', '2023-05-23 20:25:34.864', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1512, '2023-05-23 20:25:58.285', '2023-05-23 20:25:58.285', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1513, '2023-05-23 20:35:17.025', '2023-05-23 20:35:17.025', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1514, '2023-05-23 20:35:17.358', '2023-05-23 20:35:17.358', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1515, '2023-05-23 20:35:17.360', '2023-05-23 20:35:17.360', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1516, '2023-05-23 20:35:17.423', '2023-05-23 20:35:17.423', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1517, '2023-05-23 20:35:18.721', '2023-05-23 20:35:18.721', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1518, '2023-05-23 20:35:20.652', '2023-05-23 20:35:20.652', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":16}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1519, '2023-05-23 20:35:22.277', '2023-05-23 20:35:22.277', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1520, '2023-05-23 20:35:46.913', '2023-05-23 20:35:46.913', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1521, '2023-05-23 20:35:47.191', '2023-05-23 20:35:47.191', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1522, '2023-05-23 20:35:47.192', '2023-05-23 20:35:47.192', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1523, '2023-05-23 20:35:47.322', '2023-05-23 20:35:47.322', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1524, '2023-05-23 20:35:47.999', '2023-05-23 20:35:47.999', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1525, '2023-05-23 20:35:49.631', '2023-05-23 20:35:49.631', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":16}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1526, '2023-05-23 20:35:56.778', '2023-05-23 20:35:56.778', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1527, '2023-05-23 20:36:10.826', '2023-05-23 20:36:10.826', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1528, '2023-05-23 20:36:10.846', '2023-05-23 20:36:10.846', NULL, 1, 'POST:/admin/crm/customer/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1529, '2023-05-23 20:37:06.505', '2023-05-23 20:37:06.505', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":16}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1530, '2023-05-23 20:37:12.017', '2023-05-23 20:37:12.017', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1531, '2023-05-23 20:37:12.354', '2023-05-23 20:37:12.354', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1532, '2023-05-23 20:37:12.355', '2023-05-23 20:37:12.355', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1533, '2023-05-23 20:37:12.531', '2023-05-23 20:37:12.531', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1534, '2023-05-23 20:37:13.338', '2023-05-23 20:37:13.338', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1535, '2023-05-23 20:37:14.604', '2023-05-23 20:37:14.604', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":16}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1536, '2023-05-23 20:37:40.786', '2023-05-23 20:37:40.786', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":16}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1537, '2023-05-23 20:38:06.808', '2023-05-23 20:38:06.808', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1538, '2023-05-23 20:46:51.085', '2023-05-23 20:46:51.085', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":16}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1539, '2023-05-23 20:47:35.724', '2023-05-23 20:47:35.724', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1540, '2023-05-23 20:47:35.946', '2023-05-23 20:47:35.946', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1541, '2023-05-23 20:47:35.946', '2023-05-23 20:47:35.946', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1542, '2023-05-23 20:47:36.069', '2023-05-23 20:47:36.069', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1543, '2023-05-23 20:47:37.403', '2023-05-23 20:47:37.403', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1544, '2023-05-23 20:47:58.745', '2023-05-23 20:47:58.745', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":16}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1545, '2023-05-23 20:48:00.994', '2023-05-23 20:48:00.994', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1546, '2023-05-23 20:48:33.789', '2023-05-23 20:48:33.789', NULL, 1, 'POST:/admin/crm/order/add', '127.0.0.1', '127.0.0.1', '{\"uid\":1,\"orderType\":\"subRenewal\",\"endDate\":\"2023-06-01\",\"price\":\"1000\",\"rebate\":\"\",\"paytype\":\"wxpay\",\"收款日期\":\"2023-05-23\",\"id\":16}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1547, '2023-05-23 20:50:14.577', '2023-05-23 20:50:14.577', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1548, '2023-05-23 20:50:17.483', '2023-05-23 20:50:17.483', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":16}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1549, '2023-05-23 20:52:48.538', '2023-05-23 20:52:48.538', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1550, '2023-05-23 20:52:48.565', '2023-05-23 20:52:48.565', NULL, 1, 'POST:/admin/crm/customer/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1551, '2023-05-23 20:53:33.072', '2023-05-23 20:53:33.072', NULL, 1, 'POST:/admin/crm/order/add', '127.0.0.1', '127.0.0.1', '{\"uid\":1,\"domain\":\"xxyev.cn\\nwww.xxyev.cn\",\"siteName\":\"雄心壮志\",\"customerId\":1,\"orderType\":\"app\",\"endDate\":\"2023-05-31\",\"isSelf\":1,\"isIcp\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1552, '2023-05-23 20:53:33.171', '2023-05-23 20:53:33.171', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1553, '2023-05-23 20:53:52.211', '2023-05-23 20:53:52.211', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":16}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1554, '2023-05-23 20:53:54.175', '2023-05-23 20:53:54.175', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1555, '2023-05-23 20:56:10.848', '2023-05-23 20:56:10.848', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1556, '2023-05-23 20:56:11.215', '2023-05-23 20:56:11.215', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1557, '2023-05-23 20:56:11.215', '2023-05-23 20:56:11.215', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1558, '2023-05-23 20:56:11.267', '2023-05-23 20:56:11.267', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1559, '2023-05-23 20:56:12.030', '2023-05-23 20:56:12.030', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1560, '2023-05-23 20:56:25.440', '2023-05-23 20:56:25.440', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1561, '2023-05-23 20:56:25.747', '2023-05-23 20:56:25.747', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1562, '2023-05-23 20:56:25.748', '2023-05-23 20:56:25.748', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1563, '2023-05-23 20:56:25.824', '2023-05-23 20:56:25.824', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1564, '2023-05-23 20:56:26.621', '2023-05-23 20:56:26.621', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1565, '2023-05-23 20:56:44.121', '2023-05-23 20:56:44.121', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1566, '2023-05-23 20:56:44.471', '2023-05-23 20:56:44.471', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1567, '2023-05-23 20:56:44.471', '2023-05-23 20:56:44.471', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1568, '2023-05-23 20:56:44.561', '2023-05-23 20:56:44.561', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1569, '2023-05-23 20:56:45.239', '2023-05-23 20:56:45.239', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1570, '2023-05-23 20:57:48.603', '2023-05-23 20:57:48.603', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1571, '2023-05-23 20:57:48.959', '2023-05-23 20:57:48.959', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1572, '2023-05-23 20:57:48.960', '2023-05-23 20:57:48.960', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1573, '2023-05-23 20:57:49.075', '2023-05-23 20:57:49.075', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1574, '2023-05-23 20:57:49.775', '2023-05-23 20:57:49.775', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1575, '2023-05-23 20:58:08.834', '2023-05-23 20:58:08.834', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1576, '2023-05-23 20:58:10.774', '2023-05-23 20:58:10.774', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1577, '2023-05-23 20:58:28.287', '2023-05-23 20:58:28.287', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1578, '2023-05-23 21:00:00.860', '2023-05-23 21:00:00.860', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1579, '2023-05-23 21:00:21.488', '2023-05-23 21:00:21.488', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1580, '2023-05-23 21:00:25.549', '2023-05-23 21:00:25.549', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1581, '2023-05-23 21:00:26.045', '2023-05-23 21:00:26.045', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1582, '2023-05-23 21:00:26.047', '2023-05-23 21:00:26.047', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1583, '2023-05-23 21:00:26.309', '2023-05-23 21:00:26.309', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1584, '2023-05-23 21:00:27.351', '2023-05-23 21:00:27.351', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1585, '2023-05-23 21:00:29.160', '2023-05-23 21:00:29.160', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1586, '2023-05-23 21:00:31.817', '2023-05-23 21:00:31.817', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1587, '2023-05-23 21:06:55.083', '2023-05-23 21:06:55.083', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1588, '2023-05-23 21:06:55.280', '2023-05-23 21:06:55.280', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1589, '2023-05-23 21:06:55.280', '2023-05-23 21:06:55.280', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1590, '2023-05-23 21:06:55.369', '2023-05-23 21:06:55.369', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1591, '2023-05-23 21:06:58.288', '2023-05-23 21:06:58.288', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1592, '2023-05-23 22:06:51.444', '2023-05-23 22:06:51.444', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1593, '2023-05-23 22:06:52.981', '2023-05-23 22:06:52.981', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1594, '2023-05-23 22:07:06.282', '2023-05-23 22:07:06.282', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1595, '2023-05-23 22:07:21.130', '2023-05-23 22:07:21.130', NULL, 1, 'POST:/admin/crm/order/add', '127.0.0.1', '127.0.0.1', '{\"uid\":1,\"orderType\":\"subRenewal\",\"endDate\":\"2023-06-01\",\"price\":\"1000\",\"rebate\":0,\"paytype\":\"wxpay\",\"payDate\":\"2023-05-23\",\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1596, '2023-05-23 22:21:50.244', '2023-05-23 22:21:50.244', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1597, '2023-05-23 22:21:50.702', '2023-05-23 22:21:50.702', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1598, '2023-05-23 22:21:50.703', '2023-05-23 22:21:50.703', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1599, '2023-05-23 22:21:50.730', '2023-05-23 22:21:50.730', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1600, '2023-05-23 22:21:52.248', '2023-05-23 22:21:52.248', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1601, '2023-05-24 00:09:16.535', '2023-05-24 00:09:16.535', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1602, '2023-05-24 00:09:16.800', '2023-05-24 00:09:16.800', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1603, '2023-05-24 00:09:16.800', '2023-05-24 00:09:16.800', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1604, '2023-05-24 00:09:16.832', '2023-05-24 00:09:16.832', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1605, '2023-05-24 00:09:18.046', '2023-05-24 00:09:18.046', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1606, '2023-05-24 10:29:55.771', '2023-05-24 10:29:55.771', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1607, '2023-05-24 10:29:55.977', '2023-05-24 10:29:55.977', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1608, '2023-05-24 10:29:55.977', '2023-05-24 10:29:55.977', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1609, '2023-05-24 10:29:56.030', '2023-05-24 10:29:56.030', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1610, '2023-05-24 10:29:57.072', '2023-05-24 10:29:57.072', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1611, '2023-05-24 10:54:40.524', '2023-05-24 10:54:40.524', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1612, '2023-05-24 10:54:40.879', '2023-05-24 10:54:40.879', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1613, '2023-05-24 10:54:40.880', '2023-05-24 10:54:40.880', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1614, '2023-05-24 10:54:41.069', '2023-05-24 10:54:41.069', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1615, '2023-05-24 10:54:41.770', '2023-05-24 10:54:41.770', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1616, '2023-05-24 10:54:48.323', '2023-05-24 10:54:48.323', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1617, '2023-05-24 10:54:50.218', '2023-05-24 10:54:50.218', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1618, '2023-05-24 10:55:12.868', '2023-05-24 10:55:12.868', NULL, 1, 'POST:/admin/crm/order/add', '127.0.0.1', '127.0.0.1', '{\"uid\":1,\"orderType\":\"subRenewal\",\"endDate\":\"2023-06-01\",\"price\":\"1000\",\"rebate\":0,\"paytype\":\"wxpay\",\"payDate\":\"2023-05-24\",\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1619, '2023-05-24 10:59:54.192', '2023-05-24 10:59:54.192', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1620, '2023-05-24 11:00:57.978', '2023-05-24 11:00:57.978', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1621, '2023-05-24 11:01:00.883', '2023-05-24 11:01:00.883', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1622, '2023-05-24 11:01:01.466', '2023-05-24 11:01:01.466', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1623, '2023-05-24 11:01:01.467', '2023-05-24 11:01:01.467', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1624, '2023-05-24 11:01:01.544', '2023-05-24 11:01:01.544', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1625, '2023-05-24 11:01:02.621', '2023-05-24 11:01:02.621', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1626, '2023-05-24 11:01:27.487', '2023-05-24 11:01:27.487', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1627, '2023-05-24 11:01:28.066', '2023-05-24 11:01:28.066', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1628, '2023-05-24 11:01:28.066', '2023-05-24 11:01:28.066', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1629, '2023-05-24 11:01:28.235', '2023-05-24 11:01:28.235', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1630, '2023-05-24 11:01:29.615', '2023-05-24 11:01:29.615', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1631, '2023-05-24 11:01:33.868', '2023-05-24 11:01:33.868', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1632, '2023-05-24 11:01:41.444', '2023-05-24 11:01:41.444', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1633, '2023-05-24 11:01:42.879', '2023-05-24 11:01:42.879', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1634, '2023-05-24 11:01:43.216', '2023-05-24 11:01:43.216', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1635, '2023-05-24 11:01:43.218', '2023-05-24 11:01:43.218', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1636, '2023-05-24 11:01:43.330', '2023-05-24 11:01:43.330', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1637, '2023-05-24 11:01:44.138', '2023-05-24 11:01:44.138', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1638, '2023-05-24 11:01:46.615', '2023-05-24 11:01:46.615', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1639, '2023-05-24 11:02:19.136', '2023-05-24 11:02:19.136', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1640, '2023-05-24 11:02:19.643', '2023-05-24 11:02:19.643', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1641, '2023-05-24 11:02:19.643', '2023-05-24 11:02:19.643', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1642, '2023-05-24 11:02:19.676', '2023-05-24 11:02:19.676', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1643, '2023-05-24 11:02:20.995', '2023-05-24 11:02:20.995', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1644, '2023-05-24 11:02:22.585', '2023-05-24 11:02:22.585', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1645, '2023-05-24 11:02:24.766', '2023-05-24 11:02:24.766', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1646, '2023-05-24 11:02:38.777', '2023-05-24 11:02:38.777', NULL, 1, 'POST:/admin/crm/order/add', '127.0.0.1', '127.0.0.1', '{\"uid\":1,\"orderType\":\"subRenewal\",\"endDate\":\"2023-06-01\",\"price\":\"1000\",\"rebate\":0,\"paytype\":\"wxpay\",\"payDate\":\"2023-05-24\",\"parentId\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1647, '2023-05-24 11:02:38.833', '2023-05-24 11:02:38.833', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1648, '2023-05-24 11:03:20.616', '2023-05-24 11:03:20.616', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1649, '2023-05-24 11:06:22.398', '2023-05-24 11:06:22.398', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1650, '2023-05-24 11:06:31.177', '2023-05-24 11:06:31.177', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1651, '2023-05-24 11:06:31.720', '2023-05-24 11:06:31.720', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1652, '2023-05-24 11:06:31.720', '2023-05-24 11:06:31.720', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1653, '2023-05-24 11:06:32.069', '2023-05-24 11:06:32.069', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1654, '2023-05-24 11:06:33.270', '2023-05-24 11:06:33.270', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1655, '2023-05-24 11:06:37.883', '2023-05-24 11:06:37.883', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1656, '2023-05-24 11:10:06.715', '2023-05-24 11:10:06.715', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1657, '2023-05-24 11:10:22.934', '2023-05-24 11:10:22.934', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1658, '2023-05-24 11:10:22.934', '2023-05-24 11:10:22.934', NULL, 1, 'POST:/admin/crm/order/add', '127.0.0.1', '127.0.0.1', '{\"uid\":1,\"orderType\":\"subRenewal\",\"endDate\":\"2023-06-02\",\"price\":\"1000\",\"rebate\":0,\"paytype\":\"wxpay\",\"payDate\":\"2023-05-24\",\"parentId\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1659, '2023-05-24 11:10:23.059', '2023-05-24 11:10:23.059', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1660, '2023-05-24 11:10:50.210', '2023-05-24 11:10:50.210', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1661, '2023-05-24 11:11:03.410', '2023-05-24 11:11:03.410', NULL, 1, 'POST:/admin/crm/order/add', '127.0.0.1', '127.0.0.1', '{\"uid\":1,\"orderType\":\"subRenewal\",\"endDate\":\"2023-06-03\",\"price\":\"1000\",\"rebate\":0,\"paytype\":\"wxpay\",\"payDate\":\"2023-05-24\",\"parentId\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1662, '2023-05-24 11:11:03.410', '2023-05-24 11:11:03.410', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1663, '2023-05-24 11:11:03.528', '2023-05-24 11:11:03.528', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1664, '2023-05-24 11:12:20.779', '2023-05-24 11:12:20.779', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1665, '2023-05-24 11:12:45.815', '2023-05-24 11:12:45.815', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1666, '2023-05-24 11:12:59.466', '2023-05-24 11:12:59.466', NULL, 1, 'GET:/admin/crm/order/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1667, '2023-05-24 11:12:59.586', '2023-05-24 11:12:59.586', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1668, '2023-05-24 11:13:26.027', '2023-05-24 11:13:26.027', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":18}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1669, '2023-05-24 11:13:34.906', '2023-05-24 11:13:34.906', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1670, '2023-05-24 12:03:37.253', '2023-05-24 12:03:37.253', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1671, '2023-05-24 12:03:37.610', '2023-05-24 12:03:37.610', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1672, '2023-05-24 12:03:37.610', '2023-05-24 12:03:37.610', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1673, '2023-05-24 12:03:37.768', '2023-05-24 12:03:37.768', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1674, '2023-05-24 12:03:39.762', '2023-05-24 12:03:39.762', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1675, '2023-05-24 12:38:54.683', '2023-05-24 12:38:54.683', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1676, '2023-05-24 12:38:57.957', '2023-05-24 12:38:57.957', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":18}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1677, '2023-05-24 12:39:01.408', '2023-05-24 12:39:01.408', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":17}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1678, '2023-05-24 12:39:03.966', '2023-05-24 12:39:03.966', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1679, '2023-05-24 12:41:57.382', '2023-05-24 12:41:57.382', NULL, 1, 'GET:/admin/crm/order/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1680, '2023-05-24 12:41:57.561', '2023-05-24 12:41:57.561', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1681, '2023-05-24 12:43:37.675', '2023-05-24 12:43:37.675', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1682, '2023-05-24 12:45:02.922', '2023-05-24 12:45:02.922', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1683, '2023-05-24 12:48:54.375', '2023-05-24 12:48:54.375', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1684, '2023-05-24 12:49:03.377', '2023-05-24 12:49:03.377', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1685, '2023-05-24 12:49:47.382', '2023-05-24 12:49:47.382', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1686, '2023-05-24 12:49:50.323', '2023-05-24 12:49:50.323', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1687, '2023-05-24 12:49:50.929', '2023-05-24 12:49:50.929', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1688, '2023-05-24 12:49:50.929', '2023-05-24 12:49:50.929', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1689, '2023-05-24 12:49:51.049', '2023-05-24 12:49:51.049', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1690, '2023-05-24 12:49:52.183', '2023-05-24 12:49:52.183', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1691, '2023-05-24 12:49:54.384', '2023-05-24 12:49:54.384', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1692, '2023-05-24 12:49:55.696', '2023-05-24 12:49:55.696', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1693, '2023-05-24 12:50:45.883', '2023-05-24 12:50:45.883', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1694, '2023-05-24 12:50:53.211', '2023-05-24 12:50:53.211', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1695, '2023-05-24 12:50:58.737', '2023-05-24 12:50:58.737', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1696, '2023-05-24 12:51:01.483', '2023-05-24 12:51:01.483', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1697, '2023-05-24 12:51:01.811', '2023-05-24 12:51:01.811', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1698, '2023-05-24 12:51:01.811', '2023-05-24 12:51:01.811', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1699, '2023-05-24 12:51:01.864', '2023-05-24 12:51:01.864', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1700, '2023-05-24 12:51:03.230', '2023-05-24 12:51:03.230', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1701, '2023-05-24 12:51:24.711', '2023-05-24 12:51:24.711', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1702, '2023-05-24 12:51:26.076', '2023-05-24 12:51:26.076', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1703, '2023-05-24 12:51:44.434', '2023-05-24 12:51:44.434', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1704, '2023-05-24 12:52:34.314', '2023-05-24 12:52:34.314', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1705, '2023-05-24 12:52:39.541', '2023-05-24 12:52:39.541', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1706, '2023-05-24 12:52:39.892', '2023-05-24 12:52:39.892', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1707, '2023-05-24 12:52:39.892', '2023-05-24 12:52:39.892', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1708, '2023-05-24 12:52:40.094', '2023-05-24 12:52:40.094', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1709, '2023-05-24 12:52:41.768', '2023-05-24 12:52:41.768', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1710, '2023-05-24 12:52:44.192', '2023-05-24 12:52:44.192', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1711, '2023-05-24 12:52:44.218', '2023-05-24 12:52:44.218', NULL, 1, 'POST:/admin/crm/customer/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1712, '2023-05-24 12:53:46.565', '2023-05-24 12:53:46.565', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1713, '2023-05-24 12:53:50.245', '2023-05-24 12:53:50.245', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1714, '2023-05-24 12:53:50.272', '2023-05-24 12:53:50.272', NULL, 1, 'POST:/admin/crm/customer/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1715, '2023-05-24 12:54:00.318', '2023-05-24 12:54:00.318', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1716, '2023-05-24 12:54:02.365', '2023-05-24 12:54:02.365', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1717, '2023-05-24 12:54:02.397', '2023-05-24 12:54:02.397', NULL, 1, 'POST:/admin/crm/customer/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1718, '2023-05-24 12:55:04.264', '2023-05-24 12:55:04.264', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1719, '2023-05-24 12:55:06.569', '2023-05-24 12:55:06.569', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1720, '2023-05-24 12:55:06.612', '2023-05-24 12:55:06.612', NULL, 1, 'POST:/admin/crm/customer/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1721, '2023-05-24 12:55:48.713', '2023-05-24 12:55:48.713', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1722, '2023-05-24 12:55:56.146', '2023-05-24 12:55:56.146', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1723, '2023-05-24 12:55:56.182', '2023-05-24 12:55:56.182', NULL, 1, 'POST:/admin/crm/customer/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1724, '2023-05-24 12:57:43.519', '2023-05-24 12:57:43.519', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1725, '2023-05-24 12:57:46.135', '2023-05-24 12:57:46.135', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1726, '2023-05-24 12:57:46.172', '2023-05-24 12:57:46.172', NULL, 1, 'POST:/admin/crm/customer/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1727, '2023-05-24 12:58:19.516', '2023-05-24 12:58:19.516', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1728, '2023-05-24 12:58:21.466', '2023-05-24 12:58:21.466', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1729, '2023-05-24 12:58:21.488', '2023-05-24 12:58:21.488', NULL, 1, 'POST:/admin/crm/customer/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1730, '2023-05-24 12:58:37.186', '2023-05-24 12:58:37.186', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1731, '2023-05-24 12:58:41.297', '2023-05-24 12:58:41.297', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1732, '2023-05-24 12:58:41.334', '2023-05-24 12:58:41.334', NULL, 1, 'POST:/admin/crm/customer/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1733, '2023-05-24 12:58:55.467', '2023-05-24 12:58:55.467', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1734, '2023-05-24 12:58:58.989', '2023-05-24 12:58:58.989', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1735, '2023-05-24 12:58:59.017', '2023-05-24 12:58:59.017', NULL, 1, 'POST:/admin/crm/customer/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1736, '2023-05-24 12:59:14.167', '2023-05-24 12:59:14.167', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1737, '2023-05-24 12:59:22.320', '2023-05-24 12:59:22.320', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1738, '2023-05-24 12:59:22.750', '2023-05-24 12:59:22.750', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1739, '2023-05-24 12:59:22.750', '2023-05-24 12:59:22.750', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1740, '2023-05-24 12:59:22.933', '2023-05-24 12:59:22.933', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1741, '2023-05-24 12:59:23.721', '2023-05-24 12:59:23.721', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1742, '2023-05-24 12:59:28.118', '2023-05-24 12:59:28.118', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1743, '2023-05-24 12:59:28.139', '2023-05-24 12:59:28.139', NULL, 1, 'POST:/admin/crm/customer/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1744, '2023-05-24 13:00:51.241', '2023-05-24 13:00:51.241', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1745, '2023-05-24 13:00:57.970', '2023-05-24 13:00:57.970', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1746, '2023-05-24 13:00:57.990', '2023-05-24 13:00:57.990', NULL, 1, 'POST:/admin/crm/customer/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1747, '2023-05-24 13:01:16.596', '2023-05-24 13:01:16.596', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1748, '2023-05-24 13:01:19.130', '2023-05-24 13:01:19.130', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1749, '2023-05-24 13:01:19.170', '2023-05-24 13:01:19.170', NULL, 1, 'POST:/admin/crm/customer/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1750, '2023-05-24 13:01:25.842', '2023-05-24 13:01:25.842', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1751, '2023-05-24 13:01:26.224', '2023-05-24 13:01:26.224', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1752, '2023-05-24 13:01:26.224', '2023-05-24 13:01:26.224', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1753, '2023-05-24 13:01:26.470', '2023-05-24 13:01:26.470', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1754, '2023-05-24 13:01:27.820', '2023-05-24 13:01:27.820', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1755, '2023-05-24 13:01:31.036', '2023-05-24 13:01:31.036', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1756, '2023-05-24 13:01:31.070', '2023-05-24 13:01:31.070', NULL, 1, 'POST:/admin/crm/customer/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1757, '2023-05-24 13:01:45.986', '2023-05-24 13:01:45.986', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1758, '2023-05-24 13:01:47.671', '2023-05-24 13:01:47.671', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1759, '2023-05-24 13:01:47.697', '2023-05-24 13:01:47.697', NULL, 1, 'POST:/admin/crm/customer/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1760, '2023-05-24 13:04:46.587', '2023-05-24 13:04:46.587', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1761, '2023-05-24 13:04:52.581', '2023-05-24 13:04:52.581', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1762, '2023-05-24 13:04:54.315', '2023-05-24 13:04:54.315', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1763, '2023-05-24 13:04:54.339', '2023-05-24 13:04:54.339', NULL, 1, 'POST:/admin/crm/customer/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1764, '2023-05-24 13:05:11.994', '2023-05-24 13:05:11.994', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1765, '2023-05-24 13:05:15.375', '2023-05-24 13:05:15.375', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1766, '2023-05-24 13:05:15.416', '2023-05-24 13:05:15.416', NULL, 1, 'POST:/admin/crm/customer/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1767, '2023-05-24 13:05:21.893', '2023-05-24 13:05:21.893', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1768, '2023-05-24 13:05:25.071', '2023-05-24 13:05:25.071', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1769, '2023-05-24 13:05:25.116', '2023-05-24 13:05:25.116', NULL, 1, 'POST:/admin/crm/customer/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1770, '2023-05-24 13:08:33.423', '2023-05-24 13:08:33.423', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1771, '2023-05-24 13:08:36.801', '2023-05-24 13:08:36.801', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1772, '2023-05-24 13:08:36.830', '2023-05-24 13:08:36.830', NULL, 1, 'POST:/admin/crm/customer/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1773, '2023-05-24 13:09:38.972', '2023-05-24 13:09:38.972', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1774, '2023-05-24 13:09:46.735', '2023-05-24 13:09:46.735', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1775, '2023-05-24 13:09:46.767', '2023-05-24 13:09:46.767', NULL, 1, 'POST:/admin/crm/customer/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1776, '2023-05-24 13:10:53.819', '2023-05-24 13:10:53.819', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1777, '2023-05-24 13:11:14.631', '2023-05-24 13:11:14.631', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1778, '2023-05-24 13:11:14.650', '2023-05-24 13:11:14.650', NULL, 1, 'POST:/admin/crm/customer/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1779, '2023-05-24 13:11:53.622', '2023-05-24 13:11:53.622', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1780, '2023-05-24 13:11:58.739', '2023-05-24 13:11:58.739', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1781, '2023-05-24 13:11:58.775', '2023-05-24 13:11:58.775', NULL, 1, 'POST:/admin/crm/customer/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1782, '2023-05-24 13:12:26.896', '2023-05-24 13:12:26.896', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1783, '2023-05-24 13:12:28.871', '2023-05-24 13:12:28.871', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1784, '2023-05-24 13:12:28.895', '2023-05-24 13:12:28.895', NULL, 1, 'POST:/admin/crm/customer/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1785, '2023-05-24 13:12:35.221', '2023-05-24 13:12:35.221', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1786, '2023-05-24 13:12:36.786', '2023-05-24 13:12:36.786', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1787, '2023-05-24 13:12:36.821', '2023-05-24 13:12:36.821', NULL, 1, 'POST:/admin/crm/customer/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1788, '2023-05-24 13:13:00.677', '2023-05-24 13:13:00.677', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1789, '2023-05-24 13:13:04.338', '2023-05-24 13:13:04.338', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1790, '2023-05-24 13:13:04.381', '2023-05-24 13:13:04.381', NULL, 1, 'POST:/admin/crm/customer/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1791, '2023-05-24 13:13:13.444', '2023-05-24 13:13:13.444', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1792, '2023-05-24 13:13:13.972', '2023-05-24 13:13:13.972', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1793, '2023-05-24 13:13:13.973', '2023-05-24 13:13:13.973', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1794, '2023-05-24 13:13:14.050', '2023-05-24 13:13:14.050', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1795, '2023-05-24 13:13:14.836', '2023-05-24 13:13:14.836', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1796, '2023-05-24 13:13:16.734', '2023-05-24 13:13:16.734', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1797, '2023-05-24 13:13:16.772', '2023-05-24 13:13:16.772', NULL, 1, 'POST:/admin/crm/customer/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1798, '2023-05-24 13:13:27.622', '2023-05-24 13:13:27.622', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1799, '2023-05-24 13:13:34.421', '2023-05-24 13:13:34.421', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1800, '2023-05-24 13:13:34.441', '2023-05-24 13:13:34.441', NULL, 1, 'POST:/admin/crm/customer/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1801, '2023-05-24 13:13:59.169', '2023-05-24 13:13:59.169', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1802, '2023-05-24 13:14:01.827', '2023-05-24 13:14:01.827', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1803, '2023-05-24 13:14:01.849', '2023-05-24 13:14:01.849', NULL, 1, 'POST:/admin/crm/customer/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1804, '2023-05-24 13:16:25.972', '2023-05-24 13:16:25.972', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1805, '2023-05-24 13:16:29.494', '2023-05-24 13:16:29.494', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1806, '2023-05-24 13:16:29.522', '2023-05-24 13:16:29.522', NULL, 1, 'POST:/admin/crm/customer/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1807, '2023-05-24 13:17:02.274', '2023-05-24 13:17:02.274', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1808, '2023-05-24 13:17:06.651', '2023-05-24 13:17:06.651', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1809, '2023-05-24 13:17:06.673', '2023-05-24 13:17:06.673', NULL, 1, 'POST:/admin/crm/customer/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1810, '2023-05-24 13:17:14.130', '2023-05-24 13:17:14.130', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1811, '2023-05-24 13:23:31.455', '2023-05-24 13:23:31.455', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1812, '2023-05-24 13:25:40.704', '2023-05-24 13:25:40.704', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1813, '2023-05-24 13:25:45.735', '2023-05-24 13:25:45.735', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1814, '2023-05-24 13:25:52.789', '2023-05-24 13:25:52.789', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1815, '2023-05-24 13:25:57.760', '2023-05-24 13:25:57.760', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1816, '2023-05-24 13:25:59.792', '2023-05-24 13:25:59.792', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1817, '2023-05-24 13:26:21.289', '2023-05-24 13:26:21.289', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1818, '2023-05-24 13:26:21.289', '2023-05-24 13:26:21.289', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1819, '2023-05-24 13:26:32.223', '2023-05-24 13:26:32.223', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1820, '2023-05-24 13:26:32.223', '2023-05-24 13:26:32.223', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1821, '2023-05-24 13:26:34.337', '2023-05-24 13:26:34.337', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1822, '2023-05-24 13:26:34.337', '2023-05-24 13:26:34.337', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1823, '2023-05-24 13:26:37.550', '2023-05-24 13:26:37.550', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1824, '2023-05-24 13:26:37.991', '2023-05-24 13:26:37.991', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1825, '2023-05-24 13:26:37.991', '2023-05-24 13:26:37.991', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1826, '2023-05-24 13:26:38.186', '2023-05-24 13:26:38.186', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1827, '2023-05-24 13:26:39.203', '2023-05-24 13:26:39.203', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1828, '2023-05-24 13:26:43.075', '2023-05-24 13:26:43.075', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":18}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1829, '2023-05-24 13:26:49.977', '2023-05-24 13:26:49.977', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1830, '2023-05-24 13:27:25.110', '2023-05-24 13:27:25.110', NULL, 1, 'POST:/admin/crm/order/add', '127.0.0.1', '127.0.0.1', '{\"uid\":1,\"orderType\":\"subRenewal\",\"endDate\":\"2023-06-01\",\"price\":\"1000\",\"rebate\":0,\"paytype\":\"wxpay\",\"payDate\":\"2023-05-24\",\"parentId\":18}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1831, '2023-05-24 13:27:25.110', '2023-05-24 13:27:25.110', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":18}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1832, '2023-05-24 13:27:25.171', '2023-05-24 13:27:25.171', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":18}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1833, '2023-05-24 13:27:46.076', '2023-05-24 13:27:46.076', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":18}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1834, '2023-05-24 13:27:48.290', '2023-05-24 13:27:48.290', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1835, '2023-05-24 13:27:52.371', '2023-05-24 13:27:52.371', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1836, '2023-05-24 13:27:52.912', '2023-05-24 13:27:52.912', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1837, '2023-05-24 13:27:52.913', '2023-05-24 13:27:52.913', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1838, '2023-05-24 13:27:52.974', '2023-05-24 13:27:52.974', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1839, '2023-05-24 13:27:54.072', '2023-05-24 13:27:54.072', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1840, '2023-05-24 13:27:55.873', '2023-05-24 13:27:55.873', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":17}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1841, '2023-05-24 13:27:57.321', '2023-05-24 13:27:57.321', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1842, '2023-05-24 13:28:43.924', '2023-05-24 13:28:43.924', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":17}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1843, '2023-05-24 13:28:45.507', '2023-05-24 13:28:45.507', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1844, '2023-05-24 13:28:56.177', '2023-05-24 13:28:56.177', NULL, 1, 'POST:/admin/crm/order/add', '127.0.0.1', '127.0.0.1', '{\"orderType\":\"subRenewal\",\"endDate\":\"2024-05-24\",\"price\":\"1000\",\"rebate\":0,\"paytype\":\"wxpay\",\"payDate\":\"2023-05-24\",\"parentId\":17}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1845, '2023-05-24 13:28:56.177', '2023-05-24 13:28:56.177', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":17}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1846, '2023-05-24 13:30:16.608', '2023-05-24 13:30:16.608', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":17}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1847, '2023-05-24 13:30:32.666', '2023-05-24 13:30:32.666', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":17}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1848, '2023-05-24 13:35:02.315', '2023-05-24 13:35:02.315', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":17}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1849, '2023-05-24 13:41:09.723', '2023-05-24 13:41:09.723', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1850, '2023-05-24 13:41:10.016', '2023-05-24 13:41:10.016', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1851, '2023-05-24 13:41:10.016', '2023-05-24 13:41:10.016', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1852, '2023-05-24 13:41:10.132', '2023-05-24 13:41:10.132', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1853, '2023-05-24 13:41:11.600', '2023-05-24 13:41:11.600', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1854, '2023-05-24 13:41:13.613', '2023-05-24 13:41:13.613', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1855, '2023-05-24 13:41:15.999', '2023-05-24 13:41:15.999', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1856, '2023-05-24 13:41:51.348', '2023-05-24 13:41:51.348', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1857, '2023-05-24 13:42:00.628', '2023-05-24 13:42:00.628', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1858, '2023-05-24 13:42:02.819', '2023-05-24 13:42:02.819', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1859, '2023-05-24 13:42:19.256', '2023-05-24 13:42:19.256', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1860, '2023-05-24 13:42:27.711', '2023-05-24 13:42:27.711', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1861, '2023-05-24 13:42:30.604', '2023-05-24 13:42:30.604', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1862, '2023-05-24 13:42:35.105', '2023-05-24 13:42:35.105', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1863, '2023-05-24 13:42:35.409', '2023-05-24 13:42:35.409', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1864, '2023-05-24 13:42:35.409', '2023-05-24 13:42:35.409', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1865, '2023-05-24 13:42:35.509', '2023-05-24 13:42:35.509', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1866, '2023-05-24 13:42:36.953', '2023-05-24 13:42:36.953', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1867, '2023-05-24 13:42:40.276', '2023-05-24 13:42:40.276', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1868, '2023-05-24 13:42:41.551', '2023-05-24 13:42:41.551', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1869, '2023-05-24 13:43:03.953', '2023-05-24 13:43:03.953', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1870, '2023-05-24 13:43:06.728', '2023-05-24 13:43:06.728', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1871, '2023-05-24 14:00:38.127', '2023-05-24 14:00:38.127', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1872, '2023-05-24 14:00:38.351', '2023-05-24 14:00:38.351', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1873, '2023-05-24 14:00:38.351', '2023-05-24 14:00:38.351', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1874, '2023-05-24 14:00:38.405', '2023-05-24 14:00:38.405', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1875, '2023-05-24 14:00:40.432', '2023-05-24 14:00:40.432', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1876, '2023-05-24 14:27:58.399', '2023-05-24 14:27:58.399', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1877, '2023-05-24 14:28:01.716', '2023-05-24 14:28:01.716', NULL, 1, 'GET:/admin/crm/order/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1878, '2023-05-24 14:28:01.876', '2023-05-24 14:28:01.876', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1879, '2023-05-24 14:28:07.871', '2023-05-24 14:28:07.871', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1880, '2023-05-24 14:28:22.422', '2023-05-24 14:28:22.422', NULL, 1, 'POST:/admin/crm/order/add', '127.0.0.1', '127.0.0.1', '{\"uid\":1,\"orderType\":\"subExtend\",\"price\":\"2000\",\"rebate\":0,\"paytype\":\"wxpay\",\"payDate\":\"2023-05-24\",\"parentId\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1881, '2023-05-24 14:28:22.422', '2023-05-24 14:28:22.422', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1882, '2023-05-24 14:28:22.487', '2023-05-24 14:28:22.487', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1883, '2023-05-24 14:30:19.477', '2023-05-24 14:30:19.477', NULL, 1, 'POST:/admin/crm/domain/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1884, '2023-05-24 14:30:40.004', '2023-05-24 14:30:40.004', NULL, 1, 'POST:/admin/crm/domain/delete', '127.0.0.1', '127.0.0.1', '{\"ids\":[23,24,25,26,27]}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1885, '2023-05-24 14:30:40.042', '2023-05-24 14:30:40.042', NULL, 1, 'POST:/admin/crm/domain/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1886, '2023-05-24 15:16:05.464', '2023-05-24 15:16:05.464', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1887, '2023-05-24 15:16:09.911', '2023-05-24 15:16:09.911', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":18}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1888, '2023-05-24 15:16:23.198', '2023-05-24 15:16:23.198', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1889, '2023-05-24 15:16:23.795', '2023-05-24 15:16:23.795', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1890, '2023-05-24 15:16:23.804', '2023-05-24 15:16:23.804', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1891, '2023-05-24 15:16:23.865', '2023-05-24 15:16:23.865', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1892, '2023-05-24 15:16:24.938', '2023-05-24 15:16:24.938', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1893, '2023-05-24 15:16:26.738', '2023-05-24 15:16:26.738', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1894, '2023-05-24 15:16:38.789', '2023-05-24 15:16:38.789', NULL, 1, 'GET:/admin/crm/order/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1895, '2023-05-24 15:16:38.795', '2023-05-24 15:16:38.795', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1896, '2023-05-24 15:18:38.310', '2023-05-24 15:18:38.310', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1897, '2023-05-24 15:18:56.419', '2023-05-24 15:18:56.419', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1898, '2023-05-24 15:19:12.511', '2023-05-24 15:19:12.511', NULL, 1, 'GET:/admin/crm/order/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1899, '2023-05-24 15:19:12.747', '2023-05-24 15:19:12.747', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1900, '2023-05-24 15:19:12.788', '2023-05-24 15:19:12.788', NULL, 1, 'POST:/admin/crm/customer/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1901, '2023-05-24 15:19:24.508', '2023-05-24 15:19:24.508', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1902, '2023-05-24 15:19:36.509', '2023-05-24 15:19:36.509', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1903, '2023-05-24 15:19:36.989', '2023-05-24 15:19:36.989', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1904, '2023-05-24 15:19:36.989', '2023-05-24 15:19:36.989', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1905, '2023-05-24 15:19:37.088', '2023-05-24 15:19:37.088', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1906, '2023-05-24 15:19:38.138', '2023-05-24 15:19:38.138', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1907, '2023-05-24 15:19:39.537', '2023-05-24 15:19:39.537', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1908, '2023-05-24 15:20:10.016', '2023-05-24 15:20:10.016', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1909, '2023-05-24 15:20:18.091', '2023-05-24 15:20:18.091', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1910, '2023-05-24 15:20:23.360', '2023-05-24 15:20:23.360', NULL, 1, 'POST:/admin/crm/order/delete', '127.0.0.1', '127.0.0.1', '{\"ids\":[20,21,22,25]}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1911, '2023-05-24 15:20:23.388', '2023-05-24 15:20:23.388', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1912, '2023-05-24 15:20:27.069', '2023-05-24 15:20:27.069', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":18}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1913, '2023-05-24 15:20:30.207', '2023-05-24 15:20:30.207', NULL, 1, 'POST:/admin/crm/order/delete', '127.0.0.1', '127.0.0.1', '{\"ids\":[23]}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1914, '2023-05-24 15:20:30.238', '2023-05-24 15:20:30.238', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":18}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1915, '2023-05-24 15:20:32.502', '2023-05-24 15:20:32.502', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":17}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1916, '2023-05-24 15:20:35.488', '2023-05-24 15:20:35.488', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":16}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1917, '2023-05-24 15:20:37.939', '2023-05-24 15:20:37.939', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1918, '2023-05-24 15:20:39.339', '2023-05-24 15:20:39.339', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1919, '2023-05-24 15:20:51.863', '2023-05-24 15:20:51.863', NULL, 1, 'POST:/admin/crm/order/add', '127.0.0.1', '127.0.0.1', '{\"uid\":1,\"orderType\":\"subRenewal\",\"endDate\":\"2024-05-24\",\"price\":\"1000\",\"rebate\":0,\"paytype\":\"wxpay\",\"payDate\":\"2023-05-24\",\"parentId\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1920, '2023-05-24 15:20:51.864', '2023-05-24 15:20:51.864', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1921, '2023-05-24 15:20:51.939', '2023-05-24 15:20:51.939', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1922, '2023-05-24 15:23:29.761', '2023-05-24 15:23:29.761', NULL, 1, 'POST:/admin/crm/order/delete', '127.0.0.1', '127.0.0.1', '{\"ids\":[26]}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1923, '2023-05-24 15:23:29.792', '2023-05-24 15:23:29.792', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1924, '2023-05-24 15:23:31.402', '2023-05-24 15:23:31.402', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1925, '2023-05-24 15:23:38.625', '2023-05-24 15:23:38.625', NULL, 1, 'POST:/admin/crm/order/add', '127.0.0.1', '127.0.0.1', '{\"uid\":1,\"orderType\":\"subRenewal\",\"endDate\":\"2024-05-24\",\"price\":\"1000\",\"rebate\":0,\"paytype\":\"wxpay\",\"payDate\":\"2023-05-24\",\"parentId\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1926, '2023-05-24 15:23:38.626', '2023-05-24 15:23:38.626', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1927, '2023-05-24 15:23:38.674', '2023-05-24 15:23:38.674', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1928, '2023-05-24 15:28:57.616', '2023-05-24 15:28:57.616', NULL, 1, 'POST:/admin/crm/order/delete', '127.0.0.1', '127.0.0.1', '{\"ids\":[27]}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1929, '2023-05-24 15:28:57.636', '2023-05-24 15:28:57.636', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1930, '2023-05-24 15:28:58.781', '2023-05-24 15:28:58.781', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1931, '2023-05-24 15:29:05.995', '2023-05-24 15:29:05.995', NULL, 1, 'POST:/admin/crm/order/add', '127.0.0.1', '127.0.0.1', '{\"uid\":1,\"orderType\":\"subRenewal\",\"endDate\":\"2024-05-24\",\"price\":\"1000\",\"rebate\":0,\"paytype\":\"wxpay\",\"payDate\":\"2023-05-24\",\"parentId\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1932, '2023-05-24 15:29:05.995', '2023-05-24 15:29:05.995', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1933, '2023-05-24 15:29:06.127', '2023-05-24 15:29:06.127', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1934, '2023-05-24 15:30:24.671', '2023-05-24 15:30:24.671', NULL, 1, 'POST:/admin/crm/order/delete', '127.0.0.1', '127.0.0.1', '{\"ids\":[28]}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1935, '2023-05-24 15:30:24.689', '2023-05-24 15:30:24.689', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1936, '2023-05-24 15:30:27.089', '2023-05-24 15:30:27.089', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1937, '2023-05-24 15:30:33.718', '2023-05-24 15:30:33.718', NULL, 1, 'POST:/admin/crm/order/add', '127.0.0.1', '127.0.0.1', '{\"uid\":1,\"orderType\":\"subRenewal\",\"endDate\":\"2024-05-24\",\"price\":\"1000\",\"rebate\":0,\"paytype\":\"wxpay\",\"payDate\":\"2023-05-24\",\"parentId\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1938, '2023-05-24 15:30:33.718', '2023-05-24 15:30:33.718', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1939, '2023-05-24 15:30:33.834', '2023-05-24 15:30:33.834', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1940, '2023-05-24 15:34:43.042', '2023-05-24 15:34:43.042', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1941, '2023-05-24 15:34:50.522', '2023-05-24 15:34:50.522', NULL, 1, 'POST:/admin/crm/order/add', '127.0.0.1', '127.0.0.1', '{\"uid\":1,\"orderType\":\"subRenewal\",\"endDate\":\"2024-05-24\",\"price\":\"1000\",\"rebate\":0,\"paytype\":\"wxpay\",\"payDate\":\"2023-05-24\",\"parentId\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1942, '2023-05-24 15:34:50.522', '2023-05-24 15:34:50.522', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1943, '2023-05-24 15:34:50.584', '2023-05-24 15:34:50.584', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1944, '2023-05-24 15:44:36.768', '2023-05-24 15:44:36.768', NULL, 1, 'POST:/admin/crm/order/delete', '127.0.0.1', '127.0.0.1', '{\"ids\":[29,30]}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1945, '2023-05-24 15:44:36.786', '2023-05-24 15:44:36.786', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1946, '2023-05-24 16:34:34.001', '2023-05-24 16:34:34.001', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1947, '2023-05-24 16:34:34.961', '2023-05-24 16:34:34.961', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1948, '2023-05-24 16:34:34.961', '2023-05-24 16:34:34.961', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1949, '2023-05-24 16:34:35.036', '2023-05-24 16:34:35.036', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1950, '2023-05-24 16:34:36.069', '2023-05-24 16:34:36.069', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1951, '2023-05-24 16:54:42.986', '2023-05-24 16:54:42.986', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1952, '2023-05-24 16:54:43.500', '2023-05-24 16:54:43.500', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1953, '2023-05-24 16:54:43.500', '2023-05-24 16:54:43.500', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1954, '2023-05-24 16:54:43.679', '2023-05-24 16:54:43.679', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1955, '2023-05-24 16:54:44.198', '2023-05-24 16:54:44.198', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1956, '2023-05-24 16:54:45.887', '2023-05-24 16:54:45.887', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1957, '2023-05-24 16:54:47.529', '2023-05-24 16:54:47.529', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1958, '2023-05-24 16:54:59.306', '2023-05-24 16:54:59.306', NULL, 1, 'POST:/admin/crm/order/add', '127.0.0.1', '127.0.0.1', '{\"uid\":1,\"orderType\":\"subRenewal\",\"endDate\":\"2024-05-24\",\"price\":\"1000\",\"rebate\":0,\"paytype\":\"wxpay\",\"payDate\":\"2023-05-24\",\"parentId\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1959, '2023-05-24 16:54:59.306', '2023-05-24 16:54:59.306', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1960, '2023-05-24 16:54:59.383', '2023-05-24 16:54:59.383', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1961, '2023-05-24 16:56:05.229', '2023-05-24 16:56:05.229', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1962, '2023-05-24 16:58:36.892', '2023-05-24 16:58:36.892', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1963, '2023-05-24 16:58:55.666', '2023-05-24 16:58:55.666', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1964, '2023-05-24 16:59:21.288', '2023-05-24 16:59:21.288', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1965, '2023-05-24 16:59:37.224', '2023-05-24 16:59:37.224', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1966, '2023-05-24 17:00:19.915', '2023-05-24 17:00:19.915', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1967, '2023-05-24 17:08:45.322', '2023-05-24 17:08:45.322', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1968, '2023-05-24 17:08:46.205', '2023-05-24 17:08:46.205', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1969, '2023-05-24 17:08:56.980', '2023-05-24 17:08:56.980', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1970, '2023-05-24 17:08:56.980', '2023-05-24 17:08:56.980', NULL, 1, 'POST:/admin/crm/order/add', '127.0.0.1', '127.0.0.1', '{\"uid\":1,\"orderType\":\"subRenewal\",\"endDate\":\"2024-05-24\",\"price\":\"1000\",\"rebate\":0,\"paytype\":\"wxpay\",\"payDate\":\"2023-05-24\",\"parentId\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1971, '2023-05-24 17:08:57.051', '2023-05-24 17:08:57.051', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1972, '2023-05-24 17:10:13.700', '2023-05-24 17:10:13.700', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1973, '2023-05-24 17:10:17.371', '2023-05-24 17:10:17.371', NULL, 1, 'POST:/admin/crm/order/delete', '127.0.0.1', '127.0.0.1', '{\"ids\":[32]}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1974, '2023-05-24 17:10:17.399', '2023-05-24 17:10:17.399', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1975, '2023-05-24 17:10:18.909', '2023-05-24 17:10:18.909', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1976, '2023-05-24 17:10:25.405', '2023-05-24 17:10:25.405', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1977, '2023-05-24 17:10:25.405', '2023-05-24 17:10:25.405', NULL, 1, 'POST:/admin/crm/order/add', '127.0.0.1', '127.0.0.1', '{\"uid\":1,\"orderType\":\"subRenewal\",\"endDate\":\"2024-05-24\",\"price\":\"1000\",\"rebate\":0,\"paytype\":\"wxpay\",\"payDate\":\"2023-05-24\",\"parentId\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1978, '2023-05-24 17:10:25.466', '2023-05-24 17:10:25.466', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1979, '2023-05-24 17:28:53.222', '2023-05-24 17:28:53.222', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1980, '2023-05-24 17:28:53.717', '2023-05-24 17:28:53.717', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1981, '2023-05-24 17:28:53.718', '2023-05-24 17:28:53.718', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1982, '2023-05-24 17:28:53.773', '2023-05-24 17:28:53.773', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1983, '2023-05-24 17:28:54.710', '2023-05-24 17:28:54.710', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1984, '2023-05-24 17:28:58.234', '2023-05-24 17:28:58.234', NULL, 1, 'POST:/admin/crm/order/subPage', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1985, '2023-05-24 17:33:30.953', '2023-05-24 17:33:30.953', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1986, '2023-05-24 17:33:36.996', '2023-05-24 17:33:36.996', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1987, '2023-05-24 17:33:37.616', '2023-05-24 17:33:37.616', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1988, '2023-05-24 17:33:37.617', '2023-05-24 17:33:37.617', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1989, '2023-05-24 17:33:37.693', '2023-05-24 17:33:37.693', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1990, '2023-05-24 17:33:38.862', '2023-05-24 17:33:38.862', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1991, '2023-05-24 17:33:43.156', '2023-05-24 17:33:43.156', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1992, '2023-05-24 17:42:53.192', '2023-05-24 17:42:53.192', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1993, '2023-05-24 17:42:53.622', '2023-05-24 17:42:53.622', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1994, '2023-05-24 17:42:53.623', '2023-05-24 17:42:53.623', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1995, '2023-05-24 17:42:53.749', '2023-05-24 17:42:53.749', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1996, '2023-05-24 17:42:54.472', '2023-05-24 17:42:54.472', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1997, '2023-05-24 17:42:58.022', '2023-05-24 17:42:58.022', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1998, '2023-05-24 17:43:23.035', '2023-05-24 17:43:23.035', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1999, '2023-05-24 17:43:23.572', '2023-05-24 17:43:23.572', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2000, '2023-05-24 17:43:23.572', '2023-05-24 17:43:23.572', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2001, '2023-05-24 17:43:23.788', '2023-05-24 17:43:23.788', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2002, '2023-05-24 17:43:24.299', '2023-05-24 17:43:24.299', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2003, '2023-05-24 17:43:26.182', '2023-05-24 17:43:26.182', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2004, '2023-05-24 17:44:56.804', '2023-05-24 17:44:56.804', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2005, '2023-05-24 17:44:57.366', '2023-05-24 17:44:57.366', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2006, '2023-05-24 17:44:57.367', '2023-05-24 17:44:57.367', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2007, '2023-05-24 17:44:57.466', '2023-05-24 17:44:57.466', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2008, '2023-05-24 17:44:57.993', '2023-05-24 17:44:57.993', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2009, '2023-05-24 17:44:59.865', '2023-05-24 17:44:59.865', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2010, '2023-05-24 17:45:03.033', '2023-05-24 17:45:03.033', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2011, '2023-05-24 17:45:06.482', '2023-05-24 17:45:06.482', NULL, 1, 'POST:/admin/base/sys/user/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2012, '2023-05-24 17:45:14.480', '2023-05-24 17:45:14.480', NULL, 1, 'POST:/admin/crm/order/add', '127.0.0.1', '127.0.0.1', '{\"uid\":1,\"orderType\":\"subRenewal\",\"endDate\":\"2024-05-24\",\"price\":\"1000\",\"rebate\":0,\"paytype\":\"wxpay\",\"payDate\":\"2023-05-24\",\"parentId\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2013, '2023-05-24 17:45:14.480', '2023-05-24 17:45:14.480', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2014, '2023-05-24 17:45:14.564', '2023-05-24 17:45:14.564', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2015, '2023-05-24 17:47:25.609', '2023-05-24 17:47:25.609', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2016, '2023-05-24 17:47:26.113', '2023-05-24 17:47:26.113', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2017, '2023-05-24 17:47:26.113', '2023-05-24 17:47:26.113', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2018, '2023-05-24 17:47:26.243', '2023-05-24 17:47:26.243', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2019, '2023-05-24 17:47:26.791', '2023-05-24 17:47:26.791', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2020, '2023-05-24 17:47:31.511', '2023-05-24 17:47:31.511', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2021, '2023-05-24 17:47:39.782', '2023-05-24 17:47:39.782', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19,\"keyWord\":\"1745\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2022, '2023-05-24 17:47:42.077', '2023-05-24 17:47:42.077', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2023, '2023-05-24 17:47:42.080', '2023-05-24 17:47:42.080', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2024, '2023-05-24 17:47:45.011', '2023-05-24 17:47:45.011', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19,\"keyWord\":\"3103\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2025, '2023-05-24 17:47:45.014', '2023-05-24 17:47:45.014', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19,\"keyWord\":\"3103\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2026, '2023-05-24 17:47:46.944', '2023-05-24 17:47:46.944', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19,\"keyWord\":\"3102\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2027, '2023-05-24 17:47:46.961', '2023-05-24 17:47:46.961', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19,\"keyWord\":\"3102\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2028, '2023-05-24 17:47:48.644', '2023-05-24 17:47:48.644', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2029, '2023-05-24 17:48:18.044', '2023-05-24 17:48:18.044', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2030, '2023-05-24 17:48:18.509', '2023-05-24 17:48:18.509', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2031, '2023-05-24 17:48:18.509', '2023-05-24 17:48:18.509', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2032, '2023-05-24 17:48:18.621', '2023-05-24 17:48:18.621', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2033, '2023-05-24 17:48:19.872', '2023-05-24 17:48:19.872', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2034, '2023-05-24 17:49:09.377', '2023-05-24 17:49:09.377', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2035, '2023-05-24 17:49:12.553', '2023-05-24 17:49:12.553', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2036, '2023-05-24 17:49:23.313', '2023-05-24 17:49:23.313', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19,\"keyWord\":\"1745\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2037, '2023-05-24 17:49:23.316', '2023-05-24 17:49:23.316', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19,\"keyWord\":\"1745\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2038, '2023-05-24 17:50:54.834', '2023-05-24 17:50:54.834', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"id\":19}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2039, '2023-05-24 17:52:41.948', '2023-05-24 17:52:41.948', NULL, 1, 'POST:/admin/dict/type/list', '127.0.0.1', '127.0.0.1', '{\"order\":\"createTime\",\"sort\":\"asc\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2040, '2023-05-24 17:52:42.049', '2023-05-24 17:52:42.049', NULL, 1, 'POST:/admin/dict/info/list', '127.0.0.1', '127.0.0.1', '{\"typeId\":1,\"sort\":\"desc\",\"order\":\"orderNum\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2041, '2023-05-24 18:48:51.073', '2023-05-24 18:48:51.073', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2042, '2023-05-24 18:48:51.380', '2023-05-24 18:48:51.380', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2043, '2023-05-24 18:48:51.380', '2023-05-24 18:48:51.380', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2044, '2023-05-24 18:48:51.429', '2023-05-24 18:48:51.429', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2045, '2023-05-24 18:48:52.697', '2023-05-24 18:48:52.697', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2046, '2023-05-24 20:04:09.569', '2023-05-24 20:04:09.569', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2047, '2023-05-24 20:04:09.779', '2023-05-24 20:04:09.779', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2048, '2023-05-24 20:04:09.779', '2023-05-24 20:04:09.779', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2049, '2023-05-24 20:04:09.831', '2023-05-24 20:04:09.831', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2050, '2023-05-24 20:04:11.064', '2023-05-24 20:04:11.064', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2051, '2023-05-24 21:54:21.761', '2023-05-24 21:54:21.761', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2052, '2023-05-24 21:54:22.098', '2023-05-24 21:54:22.098', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2053, '2023-05-24 21:54:22.098', '2023-05-24 21:54:22.098', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2054, '2023-05-24 21:54:22.139', '2023-05-24 21:54:22.139', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2055, '2023-05-24 21:54:23.396', '2023-05-24 21:54:23.396', NULL, 1, 'POST:/admin/crm/order/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2056, '2023-05-26 03:54:17.917', '2023-05-26 03:54:17.917', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2057, '2023-05-26 03:54:19.298', '2023-05-26 03:54:19.298', NULL, 0, 'GET:/admin/base/open/captcha', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2058, '2023-05-26 03:54:24.165', '2023-05-26 03:54:24.165', NULL, 0, 'POST:/admin/base/open/login', '127.0.0.1', '127.0.0.1', '{\"username\":\"admin\",\"password\":\"adminwanyu\",\"captchaId\":\"10e8uks11n9csvmeq1oa6kg200ol1ogz\",\"verifyCode\":\"6605\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2059, '2023-05-26 03:54:24.188', '2023-05-26 03:54:24.188', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2060, '2023-05-26 03:54:24.189', '2023-05-26 03:54:24.189', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2061, '2023-05-26 03:54:24.188', '2023-05-26 03:54:24.188', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2062, '2023-05-26 03:54:35.748', '2023-05-26 03:54:35.748', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2063, '2023-05-26 03:54:39.998', '2023-05-26 03:54:39.998', NULL, 1, 'POST:/admin/base/sys/menu/delete', '127.0.0.1', '127.0.0.1', '{\"ids\":[216]}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2064, '2023-05-26 03:54:40.031', '2023-05-26 03:54:40.031', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2065, '2023-05-26 03:54:48.302', '2023-05-26 03:54:48.302', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2066, '2023-05-26 03:54:48.592', '2023-05-26 03:54:48.592', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2067, '2023-05-26 03:54:48.592', '2023-05-26 03:54:48.592', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2068, '2023-05-26 03:54:48.631', '2023-05-26 03:54:48.631', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2069, '2023-05-26 03:54:49.434', '2023-05-26 03:54:49.434', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2070, '2023-05-26 03:55:01.050', '2023-05-26 03:55:01.050', NULL, 1, 'POST:/admin/base/sys/menu/delete', '127.0.0.1', '127.0.0.1', '{\"ids\":[240]}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2071, '2023-05-26 03:55:01.117', '2023-05-26 03:55:01.117', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2072, '2023-05-26 03:55:51.930', '2023-05-26 03:55:51.930', NULL, 1, 'POST:/admin/base/sys/menu/delete', '127.0.0.1', '127.0.0.1', '{\"ids\":[218]}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2073, '2023-05-26 03:55:51.977', '2023-05-26 03:55:51.977', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2074, '2023-05-26 03:55:54.043', '2023-05-26 03:55:54.043', NULL, 1, 'POST:/admin/base/sys/menu/delete', '127.0.0.1', '127.0.0.1', '{\"ids\":[219]}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2075, '2023-05-26 03:55:54.091', '2023-05-26 03:55:54.091', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2076, '2023-05-26 03:55:55.533', '2023-05-26 03:55:55.533', NULL, 1, 'POST:/admin/base/sys/menu/delete', '127.0.0.1', '127.0.0.1', '{\"ids\":[220]}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2077, '2023-05-26 03:55:55.579', '2023-05-26 03:55:55.579', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2078, '2023-05-26 03:55:56.872', '2023-05-26 03:55:56.872', NULL, 1, 'POST:/admin/base/sys/menu/delete', '127.0.0.1', '127.0.0.1', '{\"ids\":[221]}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2079, '2023-05-26 03:55:56.920', '2023-05-26 03:55:56.920', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2080, '2023-05-26 03:55:59.568', '2023-05-26 03:55:59.568', NULL, 1, 'POST:/admin/base/sys/menu/delete', '127.0.0.1', '127.0.0.1', '{\"ids\":[222]}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2081, '2023-05-26 03:55:59.615', '2023-05-26 03:55:59.615', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2082, '2023-05-26 03:56:01.126', '2023-05-26 03:56:01.126', NULL, 1, 'POST:/admin/base/sys/menu/delete', '127.0.0.1', '127.0.0.1', '{\"ids\":[223]}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2083, '2023-05-26 03:56:01.167', '2023-05-26 03:56:01.167', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2084, '2023-05-26 03:56:02.517', '2023-05-26 03:56:02.517', NULL, 1, 'POST:/admin/base/sys/menu/delete', '127.0.0.1', '127.0.0.1', '{\"ids\":[224]}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2085, '2023-05-26 03:56:02.565', '2023-05-26 03:56:02.565', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2086, '2023-05-26 03:56:03.837', '2023-05-26 03:56:03.837', NULL, 1, 'POST:/admin/base/sys/menu/delete', '127.0.0.1', '127.0.0.1', '{\"ids\":[226]}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2087, '2023-05-26 03:56:03.876', '2023-05-26 03:56:03.876', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2088, '2023-05-26 03:56:05.190', '2023-05-26 03:56:05.190', NULL, 1, 'POST:/admin/base/sys/menu/delete', '127.0.0.1', '127.0.0.1', '{\"ids\":[227]}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2089, '2023-05-26 03:56:05.230', '2023-05-26 03:56:05.230', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2090, '2023-05-26 03:56:06.365', '2023-05-26 03:56:06.365', NULL, 1, 'POST:/admin/base/sys/menu/delete', '127.0.0.1', '127.0.0.1', '{\"ids\":[228]}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2091, '2023-05-26 03:56:06.415', '2023-05-26 03:56:06.415', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2092, '2023-05-26 03:56:07.465', '2023-05-26 03:56:07.465', NULL, 1, 'POST:/admin/base/sys/menu/delete', '127.0.0.1', '127.0.0.1', '{\"ids\":[229]}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2093, '2023-05-26 03:56:07.515', '2023-05-26 03:56:07.515', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2094, '2023-05-26 03:56:08.631', '2023-05-26 03:56:08.631', NULL, 1, 'POST:/admin/base/sys/menu/delete', '127.0.0.1', '127.0.0.1', '{\"ids\":[230]}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2095, '2023-05-26 03:56:08.672', '2023-05-26 03:56:08.672', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2096, '2023-05-26 03:56:09.965', '2023-05-26 03:56:09.965', NULL, 1, 'POST:/admin/base/sys/menu/delete', '127.0.0.1', '127.0.0.1', '{\"ids\":[231]}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2097, '2023-05-26 03:56:09.997', '2023-05-26 03:56:09.997', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2098, '2023-05-26 03:56:11.114', '2023-05-26 03:56:11.114', NULL, 1, 'POST:/admin/base/sys/menu/delete', '127.0.0.1', '127.0.0.1', '{\"ids\":[232]}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2099, '2023-05-26 03:56:11.146', '2023-05-26 03:56:11.146', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2100, '2023-05-26 03:56:12.314', '2023-05-26 03:56:12.314', NULL, 1, 'POST:/admin/base/sys/menu/delete', '127.0.0.1', '127.0.0.1', '{\"ids\":[234]}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2101, '2023-05-26 03:56:12.345', '2023-05-26 03:56:12.345', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2102, '2023-05-26 03:56:13.714', '2023-05-26 03:56:13.714', NULL, 1, 'POST:/admin/base/sys/menu/delete', '127.0.0.1', '127.0.0.1', '{\"ids\":[235]}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2103, '2023-05-26 03:56:13.743', '2023-05-26 03:56:13.743', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2104, '2023-05-26 03:56:14.974', '2023-05-26 03:56:14.974', NULL, 1, 'POST:/admin/base/sys/menu/delete', '127.0.0.1', '127.0.0.1', '{\"ids\":[236]}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2105, '2023-05-26 03:56:15.014', '2023-05-26 03:56:15.014', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2106, '2023-05-26 03:56:15.964', '2023-05-26 03:56:15.964', NULL, 1, 'POST:/admin/base/sys/menu/delete', '127.0.0.1', '127.0.0.1', '{\"ids\":[237]}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2107, '2023-05-26 03:56:16.026', '2023-05-26 03:56:16.026', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2108, '2023-05-26 03:56:17.164', '2023-05-26 03:56:17.164', NULL, 1, 'POST:/admin/base/sys/menu/delete', '127.0.0.1', '127.0.0.1', '{\"ids\":[238]}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2109, '2023-05-26 03:56:17.195', '2023-05-26 03:56:17.195', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2110, '2023-05-26 03:56:18.614', '2023-05-26 03:56:18.614', NULL, 1, 'POST:/admin/base/sys/menu/delete', '127.0.0.1', '127.0.0.1', '{\"ids\":[239]}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2111, '2023-05-26 03:56:18.643', '2023-05-26 03:56:18.643', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2112, '2023-05-26 03:57:38.442', '2023-05-26 03:57:38.442', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2113, '2023-05-26 03:57:38.911', '2023-05-26 03:57:38.911', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2114, '2023-05-26 03:57:38.912', '2023-05-26 03:57:38.912', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2115, '2023-05-26 03:57:39.011', '2023-05-26 03:57:39.011', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2116, '2023-05-26 03:57:41.226', '2023-05-26 03:57:41.226', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2117, '2023-05-26 04:45:31.942', '2023-05-26 04:45:31.942', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2118, '2023-05-26 04:45:32.427', '2023-05-26 04:45:32.427', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2119, '2023-05-26 04:45:32.427', '2023-05-26 04:45:32.427', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2120, '2023-05-26 04:45:32.566', '2023-05-26 04:45:32.566', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2121, '2023-05-26 04:45:33.170', '2023-05-26 04:45:33.170', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2122, '2023-05-26 04:45:37.846', '2023-05-26 04:45:37.846', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2123, '2023-05-26 04:46:29.780', '2023-05-26 04:46:29.780', NULL, 1, 'POST:/admin/base/sys/menu/add', '127.0.0.1', '127.0.0.1', '{\"type\":0,\"name\":\"表单数据\",\"isShow\":1,\"icon\":\"icon-favor\",\"orderNum\":2}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2124, '2023-05-26 04:46:29.829', '2023-05-26 04:46:29.829', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2125, '2023-05-26 04:46:33.347', '2023-05-26 04:46:33.347', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2126, '2023-05-26 04:46:33.501', '2023-05-26 04:46:33.501', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2127, '2023-05-26 04:47:04.993', '2023-05-26 04:47:04.993', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2128, '2023-05-26 04:47:05.181', '2023-05-26 04:47:05.181', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2129, '2023-05-26 04:47:33.012', '2023-05-26 04:47:33.012', NULL, 1, 'POST:/admin/base/sys/menu/add', '127.0.0.1', '127.0.0.1', '{\"type\":1,\"isShow\":true,\"viewPath\":\"modules/cms/views/form.vue\",\"module\":\"cms\",\"entity\":[\"cms\",\"form\"],\"name\":\"留言列表\",\"router\":\"/cms/form\",\"parentId\":241,\"keepAlive\":true,\"icon\":\"icon-app\",\"orderNum\":1}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2130, '2023-05-26 04:47:33.148', '2023-05-26 04:47:33.148', NULL, 1, 'POST:/admin/base/sys/menu/add', '127.0.0.1', '127.0.0.1', '[{\"type\":2,\"parentId\":242,\"name\":\"add\",\"perms\":\"cms:form:add\"},{\"type\":2,\"parentId\":242,\"name\":\"delete\",\"perms\":\"cms:form:delete\"},{\"type\":2,\"parentId\":242,\"name\":\"info\",\"perms\":\"cms:form:info\"},{\"type\":2,\"parentId\":242,\"name\":\"list\",\"perms\":\"cms:form:list\"},{\"type\":2,\"parentId\":242,\"name\":\"page\",\"perms\":\"cms:form:page\"},{\"type\":2,\"parentId\":242,\"name\":\"update\",\"perms\":\"cms:form:update,cms:form:info\"},{\"type\":2,\"parentId\":242,\"name\":\"welcome\",\"perms\":\"cms:form:welcome\"}]');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2131, '2023-05-26 04:47:34.825', '2023-05-26 04:47:34.825', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2132, '2023-05-26 04:47:35.332', '2023-05-26 04:47:35.332', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2133, '2023-05-26 04:47:35.333', '2023-05-26 04:47:35.333', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2134, '2023-05-26 04:47:35.453', '2023-05-26 04:47:35.453', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2135, '2023-05-26 04:47:36.481', '2023-05-26 04:47:36.481', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2136, '2023-05-26 04:47:38.734', '2023-05-26 04:47:38.734', NULL, 1, 'POST:/admin/cms/form/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2137, '2023-05-26 04:48:05.157', '2023-05-26 04:48:05.157', NULL, 1, 'POST:/admin/cms/form/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2138, '2023-05-26 04:48:16.383', '2023-05-26 04:48:16.383', NULL, 1, 'POST:/admin/cms/form/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2139, '2023-05-26 04:48:53.408', '2023-05-26 04:48:53.408', NULL, 1, 'POST:/admin/cms/form/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2140, '2023-05-26 04:49:02.014', '2023-05-26 04:49:02.014', NULL, 1, 'POST:/admin/cms/form/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2141, '2023-05-26 04:49:13.296', '2023-05-26 04:49:13.296', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2142, '2023-05-26 04:49:13.785', '2023-05-26 04:49:13.785', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2143, '2023-05-26 04:49:13.785', '2023-05-26 04:49:13.785', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2144, '2023-05-26 04:49:13.837', '2023-05-26 04:49:13.837', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2145, '2023-05-26 04:49:14.484', '2023-05-26 04:49:14.484', NULL, 1, 'POST:/admin/cms/form/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2146, '2023-05-26 04:49:23.385', '2023-05-26 04:49:23.385', NULL, 1, 'POST:/admin/cms/form/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2147, '2023-05-26 04:49:31.535', '2023-05-26 04:49:31.535', NULL, 1, 'POST:/admin/cms/form/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2148, '2023-05-26 05:25:06.445', '2023-05-26 05:25:06.445', NULL, 1, 'POST:/admin/cms/form/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2149, '2023-05-26 05:25:09.466', '2023-05-26 05:25:09.466', NULL, 1, 'GET:/admin/cms/form/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2150, '2023-05-26 05:26:52.639', '2023-05-26 05:26:52.639', NULL, 1, 'POST:/admin/cms/form/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2151, '2023-05-26 05:26:56.129', '2023-05-26 05:26:56.129', NULL, 1, 'GET:/admin/cms/form/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2152, '2023-05-26 05:27:38.217', '2023-05-26 05:27:38.217', NULL, 1, 'POST:/admin/cms/form/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2153, '2023-05-26 05:27:39.989', '2023-05-26 05:27:39.989', NULL, 1, 'GET:/admin/cms/form/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2154, '2023-05-26 05:28:32.368', '2023-05-26 05:28:32.368', NULL, 1, 'POST:/admin/cms/form/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"isExport\":true}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2155, '2023-05-26 05:28:56.574', '2023-05-26 05:28:56.574', NULL, 1, 'POST:/admin/cms/form/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2156, '2023-05-26 05:28:59.543', '2023-05-26 05:28:59.543', NULL, 1, 'POST:/admin/cms/form/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"isExport\":true}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2157, '2023-05-26 05:29:56.621', '2023-05-26 05:29:56.621', NULL, 1, 'GET:/admin/cms/form/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2158, '2023-05-26 05:30:13.238', '2023-05-26 05:30:13.238', NULL, 1, 'POST:/admin/base/sys/department/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2159, '2023-05-26 05:30:13.324', '2023-05-26 05:30:13.324', NULL, 1, 'POST:/admin/base/sys/user/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"departmentIds\":[1,13],\"sort\":\"desc\",\"order\":\"createTime\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2160, '2023-05-26 05:30:19.973', '2023-05-26 05:30:19.973', NULL, 1, 'POST:/admin/base/sys/role/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"sort\":\"desc\",\"order\":\"createTime\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2161, '2023-05-26 05:30:23.579', '2023-05-26 05:30:23.579', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2162, '2023-05-26 05:30:23.580', '2023-05-26 05:30:23.580', NULL, 1, 'POST:/admin/base/sys/department/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2163, '2023-05-26 05:31:25.538', '2023-05-26 05:31:25.538', NULL, 1, 'POST:/admin/base/sys/department/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2164, '2023-05-26 05:31:25.538', '2023-05-26 05:31:25.538', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2165, '2023-05-26 05:32:08.475', '2023-05-26 05:32:08.475', NULL, 1, 'POST:/admin/base/sys/role/add', '127.0.0.1', '127.0.0.1', '{\"name\":\"普通管理员\",\"label\":\"admin\",\"remark\":\"普通管理员\",\"menuIdList\":[241,242,243,244,245,246,247,248,249,85,84],\"relevance\":0,\"departmentIdList\":[]}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2166, '2023-05-26 05:32:17.903', '2023-05-26 05:32:17.903', NULL, 1, 'POST:/admin/base/sys/role/add', '127.0.0.1', '127.0.0.1', '{\"name\":\"普通管理员\",\"label\":\"admin-vip\",\"remark\":\"普通管理员\",\"menuIdList\":[241,242,243,244,245,246,247,248,249,85,84],\"relevance\":0,\"departmentIdList\":[]}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2167, '2023-05-26 05:32:17.953', '2023-05-26 05:32:17.953', NULL, 1, 'POST:/admin/base/sys/role/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"sort\":\"desc\",\"order\":\"createTime\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2168, '2023-05-26 05:32:51.126', '2023-05-26 05:32:51.126', NULL, 1, 'POST:/admin/base/sys/department/add', '127.0.0.1', '127.0.0.1', '{\"parentId\":1,\"name\":\"普通管理员\",\"orderNum\":2}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2169, '2023-05-26 05:32:51.275', '2023-05-26 05:32:51.275', NULL, 1, 'POST:/admin/base/sys/department/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2170, '2023-05-26 05:32:54.067', '2023-05-26 05:32:54.067', NULL, 1, 'POST:/admin/base/sys/user/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"departmentIds\":[14],\"sort\":\"desc\",\"order\":\"createTime\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2171, '2023-05-26 05:32:55.078', '2023-05-26 05:32:55.078', NULL, 1, 'POST:/admin/base/sys/role/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2172, '2023-05-26 05:33:04.554', '2023-05-26 05:33:04.554', NULL, 1, 'GET:/admin/base/comm/uploadMode', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2173, '2023-05-26 05:33:04.563', '2023-05-26 05:33:04.563', NULL, 1, 'POST:/admin/base/comm/upload', '127.0.0.1', '127.0.0.1', '------WebKitFormBoundaryh7SZZXzqkctAroyv\r\nContent-Disposition: form-data; name=\"key\"\r\n\r\n3e5ba1f1-930b-4f08-89a1-7fafebb96188_name_ico.png\r\n------WebKitFormBoundaryh7SZZXzqkctAroyv\r\nContent-Disposition: form-data; name=\"file\"; filename=\"name_ico.png\"\r\nContent-Type: image/png\r\n\r\n?PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0J\0\0\0J\0\0\0??q\0\0\0	pHYs\0\0\0\0\0??\0\0\Z?IDATx?͜y?]U???߽Ϲ??!?ez?2?@	?tي?,\nm?BJJ?*?ի?j]v?k?ViU??-J??lQʡ\nA@?	?@F????M??s??????w?t???Y?$??=~?~????{?Ҋ??l?sI`??a??s??`??q?? ?0@` хXh?b??(?+ޒN\'%???H?%???v?Y??j???b?\nPT?B?#g?>??mO?s-qHZ?=?4x???`%?b`?L?	i	H̐\')02Æ0?????N ?>\Z??\0?8??????@?4??:W?\0??????C?[?????s?G????d?%?f?<?C@??Q??*??Kˀefv?WH????1p?8t`?@?+?^po1?=?{v?=\n?n?1Г2;j?	j??H??Z$??`?@k?U@GS?sM???=?n\0??N???z?\'??	?Ǽ\r?7????v??aM????A?-c??T?s?4%?,?l?ǈ??7q?????]?ͤ+??I??!?v;?1)?+G\0Dp(??\"b\Z??p???\\\r0<k0?\0?\r8g?.?h??q?K???kO??GScCK?{?Z??\n??jz?????O?q??m???! ????iD5?j??NE\Zi\0p4t7?\r??w?:????V??HǄ?N?O\'M?\nx	@!???\ZF?!??:܇???Z?1??y?j????!??@?\'???3?7???\0?c?3P???$$?(f?????n3??J??SnS??I>??T]of??%??T?????;???\'\r?H?o\Z?Ϙ.?(?L???id\ZqfP?p?l?(??>&?n???????I暄^(P??#m>f裘><7U?8u?B???r?/?r???M7??\"C?4=>?.?f????>?u????2eWcO???G??P??R?~7u??T_??7?v?ߚ^m ?[K?S?t????H?zJ?mįҺ?2Q?K???:?&)8?W/??G??M?6?1Z????E??N?J?S??)s\n????Ɋa??d?\"???e??I*qmӳ??/i??I(&ZM?6??Tt? pw?B:?Sjy??aq`?V?? ?=\0????uѣ?e??\'*?o????<?O???ɊI?dc*?F\\VQoG??d.???|?6??\rĩ????s5?L?+?wo@????%????)`p-ps+B\0??1????4?1\'?\"?b%????o????TI^?T??	?0??k&}Ś?mMG???CB-+Q?C?I?????????4(5??V??\n\\|??F????hw??p?????F`PA????????\\??F8??)?L?h? w?` ?hf??Z?R-???g?l?????ٗd?3D;S3??ζ{v??^??V??K?3?W?&N?I?,j?C~L?=??D)???~,?\\?0?n?I!?_?1?\"?@????\Z?]h?b?\nŤ?8Rp%?ޮ????i2???U?-?rv??dE?`G{?J?7	?EU\r??،?[V?0?B?:?%5f??c??o;G????????7???\'???֌?w~0???M\n?N??0?7???\nl?\rַ?}??i???[??D4?ON?L??Ft?8?4x_s\"??\rm?r	>+????Yo???????,?z????d?\\t7@2?T?獊?H??w?Z^\Z6?6??y?3??W ??ĽN?좲???????\Z???m???^Y/{???\Z?6?$?(????M?[??y?+?g\'?E?dH?Մd??5???Y?+Y???	-?9!3???? ?IP????\Z?މ+???T??R??Wx?5y-??t9??.}??\"???v]q[Ё`|#?y?>?j1	-0o?6??Ɍ?DS?@Vߩ?g???w	hC_??????K9P\nsM8	?,?!݀??k????.?????\'f	???????\'Zu,?ͦ4(???c????^9??x?d?b ??A?Ө?q&ݾ???$?q??b?fS}ߐ?h7?$$?o??\Z\0??pN8?<??6]??=?(SMʊ?d\n?????!??b~??;k??1<(???Ρ?I\\y??}]\\???8? ~=?]䜼s?5l?f????ʸ=ڊ&?zOjYN??1??xF?i?ࠡ?1???Mb???а?i0#t?&?\"?ɓ?g???5??U?lm0????N??)??$??;???e=3h/???(?ؐIU?b??<??10X?k?VS?q?w?#?S!?????7q3#?l?`????Z?:]?u???L????Y????x?%g???%}?U??1??0l?XG?*1R r?ꢴ?G-?c0?(???,??+?{Z?S?6??W??4y?%?B?X0?6\n\Z?^?&uO?E ???^E??.?ˎ??b??y<??>??8@?P??r???`цn???=?2D???q?OA?4 ???\0f?????iuJK`08?U?ͷR?!j\n???`???\'B?X7?E?#?®?J?\r?x͙+Y?n1?b\'R???.??\r?y?ѽ???????D)?(?R??????3C?N#\Z??P%??^SL??g???c??Y)?Gmf??#Ye¶?G<+??dh???.\'-I?Wg??)?6?	???jβ?3y??\'?-(?W?`?e?:6?]̉s{????~?S???^I??ӿ??a????#??2?1/??kd???>??է???5?SOx$???T:f??S??X??f0@?x?qN??a?i???x??r?yr?9??U??邉F{`(????y?]û߶??VΣo(g?Z\\42-??????8??V2?E???/\r?sӛ??H#??;A?~\\?&d?w3b????p,?5?dNO?9x??36?????/??)??,???+{???3x?y???-?U?`?[E]?t?&???c???#2}????K??b?}?4?OS?~Ĉ?f,O2_t&?7v??vč	?ʋ@?O9o?J?J	?Z?F!?5ʕ???[??U??랧>|ߣ?W?9?-?.%?O?????\'%?,??????<8??3??1?i?9M*`?xW(t\'???_?Vm?-??F?P????8aQ?j?@r?u??LT.??3(???e??[??????s>{?]O??#C?I?\0?}\rx??7?6? ??`??f?S?h?f3?dht?Ba??,? !DAўT?r\'\"???LѠ?(訔8{?\nH?d???Ha??&*4?????\ZT?|?I?,[4??woz???=??K??Al?????d?\0??6E??٨^y] n&?W?C\r#??#?\\b#??iLLd5+ش|1??ĵ?4????b\\Έ?:a??S??\ZO<?w?cO?Xͼ????Q??1?7Q??97?\\~?s??$????g:?`??6??T?@?|RS???RA??P?o	Ȋ@{%??,G??????^E??=bS??;(y??>?/n?G?栗??fTk??;?*i???kp(?KS?{??d?K*?????Ш?(4??izA\r#`?i	\0?\Z?jN>??҂L???F?Y?sڊX?x^M?69??*`~˪Rfܻe7߹??(????o??6?.??ʥ?Y??;??f?????/>?䁾#?Y??,??9kD??ʼ ?k!?Uj? ?????(\ZVR??\rQX ????zJ???D???p??Ew<??j??n{?۷???(??,/X?3?ͧ,e?I??;?J	@郯X>??Ƕ??O<?3????^2??N??ұ&???W#i~?(? ?ķ&???\'\rI?8??Kt%??? ˍY3ژ???!p8]??*?rŗ??5n??A?<??R??#M=瞵?ͧ.cvό8???Y.??w׮Y???5kz?=??0wܻ?Gw?h_???$?~?UXq\rѻ|љ?2?(???X7h????db_\"i???V\0???`??l<q3V??B?Z?}?2՚d???m?sσ?h+\'Բ???\\????_?8T?&(?\\\n???څs??^???yU? {???;???~?R4z???ZMJu#?,???;??&m??nCkA?M\Z?Guғ΂=Ϩ@+;Q??]`??nHKP8??!2$??l???=C??P?V,??{.>???@-?j>1H#?و?S??2s?a??՜?r:?t????裳??D???l?E?V?l??4?????g?v:?L????k\n??̃?????????TNس?y~q?6̌,+X??ť???e???aj?d]??B^ZC?@??(??i=??9?,?qf\nf|??SM?W9??K??y&b?)?	?p!2#[?^?].\Z??>F??j+luG????????\n?ǎ?&?Ռ[??GqNt??y??k????k?G?8?Bq9?\0+?V×?ް?E???0?7?z????Ǯj?ÿ4(??$.???N=?v?{L?6?;x?`???\01??-??i??+,Y0????)??z}?9???4?(?s7??ēL?DON`?x?ƌ?n?oX??9??c-?ZZ3[0?7·\r???#?Y\r᣸!ZLl有8u?	??r?x???y?l?ƁC}??,n??՜???Z+(????9봥q?Mg?MDb?6SR??AG???????.?U?]I?]I??s??M?O?Ҫxӄ???8? iˈ?I??w???2P=q??????);?zv????]?H????Ӗ?>?=.?/?<?^CaQ??g\'?ͦ?UB{?ўv?smX?:?ɮ7O?r??x?q#?w?????n?7??@o=?\\???b`ƣO?X?`0ov\'?V??-?h\nZ>*?f$?g?_?O<??|5掘?ߚ??*3V???L?jG??75u??f2G?Q?G0?-?-?\"\\?̾ó??̖?s???]???ecɂn:fT ?gu@̬:?????t????-?Ä??i0???l??YQ??D??KS,?\"??zIC???\r|??!?lβ??h???}?o?????μ???A?GN$ޱ|?????صiY7??Q?W?J??JJ???j\0??8?????I\\?}N??r2?S??d?k?->????徍J~??}\0????Ã?\r?Ug?[h,N?????J9a?????&INŨx3???`???Y?k\r?Ƭh}(?&y??4¹??m?	lӵ?o?\Zظ??`w??k\0i??s???Ƕ?_F2??FG?Zw??A???^I[;?????Z?Z?ͬb?IcSz???!??>+%>?????V?j?;??_4??Џ??q^1?????x?9\nK???C?\r\rB\Z??!Z?*	?rr|?]?(\nF??????P????pb?s?ӹ?s??\n22j\n?>?8??ګ?86\n?bP???CUWV.y??s?_??T,???e??m??m?????Ô??!??(<V@\Z?f\Z9?%J??K2??q.?w?tI1\\e??bP????gm??)m????S%?b???,??A?0A\0g[???\Z?????+Y??;??^:???l?؅???y???8x??R)?-?N??Ⱦ????3?x??p?%q?^b?\n>P??>vWL|Ǆ*??oB?L?3T???-?1gV?ϊ?m?????~?)T????r???????]נ$??l??Ƥ?%?va:??Se?Dk)u?????&???ˤ????????І3Ʊ?????$q?=:ġ??Y>???4zĘ?ē?ҺZ???i?σ?\Z\'W?R?,????Q?sc?T?q?S,??K?<v??}??o?q?3?&??9?h?_??i??Jʣ????=D6??_܋p<GH????\Z?R??%(??	????	?#?.???ki???t???iu?wb?28Sb????{?P?e?????x?E???????@?h??	>??~? ????^?D??&\n?\Z???`2^gp???q?{?nm??t_Z??㕒;?xzW/?UV-??o+????E1J?:?|??j???P\rj`????w>?O??N?@?R??r?HG#?\Z??1??\"?!????G?oj\n??tO?!ڡ?s?<???;{9t??ͤ??v?F??+?}??j????3?|/???I?l?2*?ӿ	(???\0J#@???jF???	?? ???(?A?g8i???]???c?!z?ۘ5?~??K??\0އ??j??R®?G?ɖ???g??7D[?D?SZ?4S??>???*?/6*>?@~?\"ie?x`ێ???E??p???????O??\"??$P????G???{?f??#?G%M??c=Wk?6K\\M??4r?M??Gf?;PO??X.?.M<?՜ǟ:??G??Y?{vg\r?]?n????QN??x??έ;???O????d??(%Q???*??8??ė????,p%???Q4P/??}B????\'K?o/?????xf?a6?????3o~W?ֳb??l????(???#88???o??ևv???>?D[9i,??iW؅&>??{Zn?K??1~???D?5?K??~?)?\n?\"0gV;?W/?ԵY<??/I?^???=L?t\'?<?;0H???\'w??w?oދ4Mp0?S?f*VO?iZ,?7#??rLS???	???;4????֧??P??C????S`?dyAQ?:+,?7c??|?????,/~58T>\ZfOo?ǎ֨?u3?????45?t??????k\0?????Hn???/P??\07㸜hR?\r???,/0?L?.`??=(l+??.Mv*?s)?F???l?̖?8tq?9?????ׅ??q_??PE?3%{?ֺ?\r?y?z-???)j??b?Z??x7UO???n7???982??ė\0??rǝ?Y??Y???J?.$އ2gL?6?m5??????)?\0nR(~?ݦ?q?h?????ħ??J\\cp2???06c??1*???BOJ}?n???\0[%m1??_??????\0?????K?X,7c03??9\\;(	?AD?37 ??݂??E????C??Ы{??\0\0\0\0IEND?B`?\r\n------WebKitFormBoundaryh7SZZXzqkctAroyv--\r\n');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2174, '2023-05-26 05:33:22.866', '2023-05-26 05:33:22.866', NULL, 1, 'POST:/admin/base/sys/user/add', '127.0.0.1', '127.0.0.1', '{\"headImg\":\"/dzh/public/uploads/20230526/csvoic2qiqfsx4jw7v.png\",\"name\":\"普通管理员\",\"nickName\":\"普通管理员\",\"username\":\"king\",\"password\":\"king1234\",\"roleIdList\":[11],\"status\":1,\"departmentId\":14}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2175, '2023-05-26 05:33:22.929', '2023-05-26 05:33:22.929', NULL, 1, 'POST:/admin/base/sys/user/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"departmentIds\":[14],\"sort\":\"desc\",\"order\":\"createTime\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2176, '2023-05-26 05:33:26.850', '2023-05-26 05:33:26.850', NULL, 1, 'POST:/admin/base/comm/logout', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2177, '2023-05-26 05:33:27.007', '2023-05-26 05:33:27.007', NULL, 0, 'GET:/admin/base/open/captcha', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2178, '2023-05-26 05:33:32.246', '2023-05-26 05:33:32.246', NULL, 0, 'POST:/admin/base/open/login', '127.0.0.1', '127.0.0.1', '{\"username\":\"king\",\"password\":\"king1234\",\"captchaId\":\"10e8uks19qucsvoimdwkq7c200th9284\",\"verifyCode\":\"5492\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2179, '2023-05-26 05:33:32.260', '2023-05-26 05:33:32.260', NULL, 0, 'GET:/admin/base/open/captcha', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2180, '2023-05-26 05:33:40.891', '2023-05-26 05:33:40.891', NULL, 0, 'POST:/admin/base/open/login', '127.0.0.1', '127.0.0.1', '{\"username\":\"admin\",\"password\":\"adminwanyu\",\"captchaId\":\"10e8uks19qucsvoiosrywts300gpg14v\",\"verifyCode\":\"4687\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2181, '2023-05-26 05:33:40.911', '2023-05-26 05:33:40.911', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2182, '2023-05-26 05:33:40.911', '2023-05-26 05:33:40.911', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2183, '2023-05-26 05:33:40.912', '2023-05-26 05:33:40.912', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2184, '2023-05-26 05:33:46.449', '2023-05-26 05:33:46.449', NULL, 1, 'POST:/admin/base/sys/department/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2185, '2023-05-26 05:33:46.504', '2023-05-26 05:33:46.504', NULL, 1, 'POST:/admin/base/sys/user/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"departmentIds\":[1,13,14],\"sort\":\"desc\",\"order\":\"createTime\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2186, '2023-05-26 05:33:48.777', '2023-05-26 05:33:48.777', NULL, 1, 'GET:/admin/base/sys/user/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2187, '2023-05-26 05:33:48.826', '2023-05-26 05:33:48.826', NULL, 1, 'POST:/admin/base/sys/role/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2188, '2023-05-26 05:33:53.771', '2023-05-26 05:33:53.771', NULL, 1, 'POST:/admin/base/sys/user/update', '127.0.0.1', '127.0.0.1', '{\"headImg\":\"/dzh/public/uploads/20230526/csvoic2qiqfsx4jw7v.png\",\"name\":\"普通管理员\",\"nickName\":\"普通管理员\",\"username\":\"king\",\"password\":\"king1234\",\"roleIdList\":[11],\"phone\":null,\"email\":null,\"remark\":null,\"status\":1,\"createTime\":\"2023-05-26 05:33:22\",\"deleted_at\":null,\"departmentId\":1,\"id\":30,\"passwordV\":1,\"socketId\":null,\"updateTime\":\"2023-05-26 05:33:22\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2189, '2023-05-26 05:33:53.820', '2023-05-26 05:33:53.820', NULL, 1, 'POST:/admin/base/sys/user/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"departmentIds\":[1,13,14],\"sort\":\"desc\",\"order\":\"createTime\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2190, '2023-05-26 05:33:57.227', '2023-05-26 05:33:57.227', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2191, '2023-05-26 05:33:57.729', '2023-05-26 05:33:57.729', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2192, '2023-05-26 05:33:57.730', '2023-05-26 05:33:57.730', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2193, '2023-05-26 05:33:57.823', '2023-05-26 05:33:57.823', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2194, '2023-05-26 05:33:58.649', '2023-05-26 05:33:58.649', NULL, 1, 'POST:/admin/base/sys/department/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2195, '2023-05-26 05:33:58.723', '2023-05-26 05:33:58.723', NULL, 1, 'POST:/admin/base/sys/user/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"departmentIds\":[1,13,14],\"sort\":\"desc\",\"order\":\"createTime\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2196, '2023-05-26 05:34:00.858', '2023-05-26 05:34:00.858', NULL, 1, 'POST:/admin/base/comm/logout', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2197, '2023-05-26 05:34:00.924', '2023-05-26 05:34:00.924', NULL, 0, 'GET:/admin/base/open/captcha', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2198, '2023-05-26 05:34:06.895', '2023-05-26 05:34:06.895', NULL, 0, 'POST:/admin/base/open/login', '127.0.0.1', '127.0.0.1', '{\"username\":\"king\",\"password\":\"king1234\",\"captchaId\":\"10e8uks19qucsvoj1ytnk54500e6hahu\",\"verifyCode\":\"6122\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2199, '2023-05-26 05:34:06.914', '2023-05-26 05:34:06.914', NULL, 30, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2200, '2023-05-26 05:34:06.915', '2023-05-26 05:34:06.915', NULL, 30, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2201, '2023-05-26 05:34:09.009', '2023-05-26 05:34:09.009', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2202, '2023-05-26 05:34:09.164', '2023-05-26 05:34:09.164', NULL, 30, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2203, '2023-05-26 05:34:09.164', '2023-05-26 05:34:09.164', NULL, 30, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2204, '2023-05-26 05:34:10.965', '2023-05-26 05:34:10.965', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2205, '2023-05-26 05:34:11.148', '2023-05-26 05:34:11.148', NULL, 30, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2206, '2023-05-26 05:34:11.148', '2023-05-26 05:34:11.148', NULL, 30, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2207, '2023-05-26 05:34:14.952', '2023-05-26 05:34:14.952', NULL, 30, 'POST:/admin/base/comm/logout', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2208, '2023-05-26 05:34:14.998', '2023-05-26 05:34:14.998', NULL, 0, 'GET:/admin/base/open/captcha', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2209, '2023-05-26 05:34:20.519', '2023-05-26 05:34:20.519', NULL, 0, 'POST:/admin/base/open/login', '127.0.0.1', '127.0.0.1', '{\"username\":\"admin\",\"password\":\"adminwanyu\",\"captchaId\":\"10e8uks19qucsvoj8fl8vj4600ngxsy2\",\"verifyCode\":\"0909\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2210, '2023-05-26 05:34:20.529', '2023-05-26 05:34:20.529', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2211, '2023-05-26 05:34:20.529', '2023-05-26 05:34:20.529', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2212, '2023-05-26 05:34:20.530', '2023-05-26 05:34:20.530', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2213, '2023-05-26 05:34:25.308', '2023-05-26 05:34:25.308', NULL, 1, 'POST:/admin/base/sys/role/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"sort\":\"desc\",\"order\":\"createTime\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2214, '2023-05-26 05:34:27.748', '2023-05-26 05:34:27.748', NULL, 1, 'GET:/admin/base/sys/role/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2215, '2023-05-26 05:34:27.774', '2023-05-26 05:34:27.774', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2216, '2023-05-26 05:34:27.774', '2023-05-26 05:34:27.774', NULL, 1, 'POST:/admin/base/sys/department/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2217, '2023-05-26 05:34:41.228', '2023-05-26 05:34:41.228', NULL, 1, 'POST:/admin/base/sys/role/update', '127.0.0.1', '127.0.0.1', '{\"name\":\"普通管理员\",\"label\":\"admin-vip\",\"remark\":\"普通管理员\",\"menuIdList\":[1,96,45,43,49,86,47,48,85,197,198,199,200,201,202,203,204,205,206,84],\"relevance\":0,\"departmentIdList\":[14],\"createTime\":\"2023-05-26 05:32:17\",\"deleted_at\":null,\"id\":11,\"updateTime\":\"2023-05-26 05:32:17\",\"userId\":\"1\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2218, '2023-05-26 05:34:41.297', '2023-05-26 05:34:41.297', NULL, 1, 'POST:/admin/base/sys/role/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"sort\":\"desc\",\"order\":\"createTime\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2219, '2023-05-26 05:34:44.121', '2023-05-26 05:34:44.121', NULL, 1, 'POST:/admin/base/comm/logout', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2220, '2023-05-26 05:34:44.190', '2023-05-26 05:34:44.190', NULL, 0, 'GET:/admin/base/open/captcha', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2221, '2023-05-26 05:34:50.230', '2023-05-26 05:34:50.230', NULL, 0, 'POST:/admin/base/open/login', '127.0.0.1', '127.0.0.1', '{\"username\":\"king\",\"password\":\"king1234\",\"captchaId\":\"10e8uks19qucsvojludir6g700ucsfn4\",\"verifyCode\":\"3325\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2222, '2023-05-26 05:34:50.255', '2023-05-26 05:34:50.255', NULL, 30, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2223, '2023-05-26 05:34:50.255', '2023-05-26 05:34:50.255', NULL, 30, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2224, '2023-05-26 05:34:50.256', '2023-05-26 05:34:50.256', NULL, 30, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2225, '2023-05-26 05:34:53.442', '2023-05-26 05:34:53.442', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2226, '2023-05-26 05:34:53.569', '2023-05-26 05:34:53.569', NULL, 30, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2227, '2023-05-26 05:34:53.569', '2023-05-26 05:34:53.569', NULL, 30, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2228, '2023-05-26 05:34:53.617', '2023-05-26 05:34:53.617', NULL, 30, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2229, '2023-05-26 05:34:57.266', '2023-05-26 05:34:57.266', NULL, 30, 'POST:/admin/base/comm/logout', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2230, '2023-05-26 05:34:57.311', '2023-05-26 05:34:57.311', NULL, 0, 'GET:/admin/base/open/captcha', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2231, '2023-05-26 05:35:03.525', '2023-05-26 05:35:03.525', NULL, 0, 'POST:/admin/base/open/login', '127.0.0.1', '127.0.0.1', '{\"username\":\"admin\",\"password\":\"adminwanyu\",\"captchaId\":\"10e8uks19qucsvojrvd49fc800cm9b3p\",\"verifyCode\":\"2617\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2232, '2023-05-26 05:35:03.546', '2023-05-26 05:35:03.546', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2233, '2023-05-26 05:35:03.547', '2023-05-26 05:35:03.547', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2234, '2023-05-26 05:35:03.547', '2023-05-26 05:35:03.547', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2235, '2023-05-26 05:35:13.277', '2023-05-26 05:35:13.277', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2236, '2023-05-26 05:35:14.746', '2023-05-26 05:35:14.746', NULL, 1, 'POST:/admin/base/sys/role/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"sort\":\"desc\",\"order\":\"createTime\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2237, '2023-05-26 05:35:21.095', '2023-05-26 05:35:21.095', NULL, 1, 'GET:/admin/base/sys/role/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2238, '2023-05-26 05:35:21.122', '2023-05-26 05:35:21.122', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2239, '2023-05-26 05:35:21.123', '2023-05-26 05:35:21.123', NULL, 1, 'POST:/admin/base/sys/department/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2240, '2023-05-26 05:35:27.418', '2023-05-26 05:35:27.418', NULL, 1, 'POST:/admin/base/sys/role/update', '127.0.0.1', '127.0.0.1', '{\"name\":\"普通管理员\",\"label\":\"admin-vip\",\"remark\":\"普通管理员\",\"menuIdList\":[241,242,243,244,245,246,247,248,249,1,96,45,43,49,86,47,48,85,197,198,199,200,201,202,203,204,205,206,84],\"relevance\":0,\"departmentIdList\":[14],\"createTime\":\"2023-05-26 05:32:17\",\"deleted_at\":null,\"id\":11,\"updateTime\":\"2023-05-26 05:34:41\",\"userId\":\"1\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2241, '2023-05-26 05:35:27.466', '2023-05-26 05:35:27.466', NULL, 1, 'POST:/admin/base/sys/role/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"sort\":\"desc\",\"order\":\"createTime\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2242, '2023-05-26 05:35:29.445', '2023-05-26 05:35:29.445', NULL, 1, 'POST:/admin/base/sys/department/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2243, '2023-05-26 05:35:29.495', '2023-05-26 05:35:29.495', NULL, 1, 'POST:/admin/base/sys/user/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"departmentIds\":[1,13,14],\"sort\":\"desc\",\"order\":\"createTime\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2244, '2023-05-26 05:35:31.848', '2023-05-26 05:35:31.848', NULL, 1, 'GET:/admin/base/sys/user/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2245, '2023-05-26 05:35:31.918', '2023-05-26 05:35:31.918', NULL, 1, 'POST:/admin/base/sys/role/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2246, '2023-05-26 05:35:35.274', '2023-05-26 05:35:35.274', NULL, 1, 'POST:/admin/base/comm/logout', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2247, '2023-05-26 05:35:35.351', '2023-05-26 05:35:35.351', NULL, 0, 'GET:/admin/base/open/captcha', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2248, '2023-05-26 05:35:41.259', '2023-05-26 05:35:41.259', NULL, 0, 'POST:/admin/base/open/login', '127.0.0.1', '127.0.0.1', '{\"username\":\"king\",\"password\":\"king1234\",\"captchaId\":\"10e8uks19qucsvok9chfq3s900zeogt7\",\"verifyCode\":\"5584\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2249, '2023-05-26 05:35:41.270', '2023-05-26 05:35:41.270', NULL, 30, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2250, '2023-05-26 05:35:41.271', '2023-05-26 05:35:41.271', NULL, 30, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2251, '2023-05-26 05:35:41.271', '2023-05-26 05:35:41.271', NULL, 30, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2252, '2023-05-26 05:35:44.976', '2023-05-26 05:35:44.976', NULL, 30, 'POST:/admin/cms/form/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2253, '2023-05-26 05:36:02.225', '2023-05-26 05:36:02.225', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2254, '2023-05-26 05:36:02.368', '2023-05-26 05:36:02.368', NULL, 30, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2255, '2023-05-26 05:36:02.368', '2023-05-26 05:36:02.368', NULL, 30, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2256, '2023-05-26 05:36:02.444', '2023-05-26 05:36:02.444', NULL, 30, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
COMMIT;

-- ----------------------------
-- Table structure for base_sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `base_sys_menu`;
CREATE TABLE `base_sys_menu` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `parentId` bigint(20) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `router` varchar(255) DEFAULT NULL,
  `perms` varchar(255) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `orderNum` int(11) NOT NULL DEFAULT '0',
  `viewPath` varchar(255) DEFAULT NULL,
  `keepAlive` int(11) NOT NULL DEFAULT '1',
  `isShow` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_base_sys_menu_deleted_at` (`deleted_at`)
) ENGINE=InnoDB AUTO_INCREMENT=250 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of base_sys_menu
-- ----------------------------
BEGIN;
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (1, '2023-05-06 09:11:38.000', '2023-05-06 09:51:05.355', NULL, NULL, '工作台', '/', NULL, 0, 'icon-workbench', 99, NULL, 1, 0);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (2, '2023-05-06 09:11:38.000', '2023-05-06 09:15:07.680', NULL, NULL, '系统管理', '/sys', NULL, 0, 'icon-system', 99, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (8, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 27, '菜单列表', '/sys/menu', NULL, 1, 'icon-menu', 2, 'cool/modules/base/views/menu.vue', 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (10, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 8, '新增', NULL, 'base:sys:menu:add', 2, NULL, 1, NULL, 0, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (11, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 8, '删除', NULL, 'base:sys:menu:delete', 2, NULL, 2, NULL, 0, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (12, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 8, '修改', NULL, 'base:sys:menu:update', 2, NULL, 3, NULL, 0, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (13, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 8, '查询', NULL, 'base:sys:menu:page,base:sys:menu:list,base:sys:menu:info', 2, NULL, 4, NULL, 0, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (22, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 27, '角色列表', '/sys/role', NULL, 1, 'icon-common', 3, 'cool/modules/base/views/role.vue', 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (23, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 22, '新增', NULL, 'base:sys:role:add', 2, NULL, 1, NULL, 0, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (24, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 22, '删除', NULL, 'base:sys:role:delete', 2, NULL, 2, NULL, 0, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (25, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 22, '修改', NULL, 'base:sys:role:update', 2, NULL, 3, NULL, 0, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (26, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 22, '查询', NULL, 'base:sys:role:page,base:sys:role:list,base:sys:role:info', 2, NULL, 4, NULL, 0, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (27, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 2, '权限管理', NULL, NULL, 0, 'icon-auth', 1, NULL, 0, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (29, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 105, '请求日志', '/sys/log', NULL, 1, 'icon-log', 1, 'cool/modules/base/views/log.vue', 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (30, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 29, '权限', NULL, 'base:sys:log:page,base:sys:log:clear,base:sys:log:getKeep,base:sys:log:setKeep', 2, NULL, 1, NULL, 0, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (43, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 45, 'crud 示例', '/crud', NULL, 1, 'icon-favor', 1, 'cool/modules/demo/views/crud.vue', 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (45, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 1, '组件库', '/ui-lib', NULL, 0, 'icon-common', 2, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (47, '2023-05-06 09:11:38.000', '2023-05-06 09:40:37.980', NULL, NULL, '框架教程', '/tutorial', NULL, 0, 'icon-task', 99, NULL, 1, 0);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (48, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 47, '文档', '/tutorial/doc', NULL, 1, 'icon-log', 0, 'https://cool-js.com', 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (49, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 45, 'quill 富文本编辑器', '/editor-quill', NULL, 1, 'icon-favor', 2, 'cool/modules/demo/views/editor-quill.vue', 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (59, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 97, '部门列表', NULL, 'base:sys:department:list', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (60, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 97, '新增部门', NULL, 'base:sys:department:add', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (61, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 97, '更新部门', NULL, 'base:sys:department:update', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (62, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 97, '删除部门', NULL, 'base:sys:department:delete', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (63, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 97, '部门排序', NULL, 'base:sys:department:order', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (65, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 97, '用户转移', NULL, 'base:sys:user:move', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (78, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 2, '参数配置', NULL, NULL, 0, 'icon-common', 4, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (79, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 78, '参数列表', '/sys/param', NULL, 1, 'icon-menu', 0, 'cool/modules/base/views/param.vue', 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (80, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 79, '新增', NULL, 'base:sys:param:add', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (81, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 79, '修改', NULL, 'base:sys:param:info,base:sys:param:update', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (82, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 79, '删除', NULL, 'base:sys:param:delete', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (83, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 79, '查看', NULL, 'base:sys:param:page,base:sys:param:list,base:sys:param:info', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (84, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, NULL, '通用', NULL, NULL, 0, 'icon-radioboxfill', 99, NULL, 1, 0);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (85, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 84, '图片上传', NULL, 'space:info:page,space:info:list,space:info:info,space:info:add,space:info:delete,space:info:update,space:type:page,space:type:list,space:type:info,space:type:add,space:type:delete,space:type:update', 2, NULL, 1, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (86, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 45, '文件上传', '/upload', NULL, 1, 'icon-favor', 3, 'cool/modules/demo/views/upload.vue', 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (90, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 84, '客服聊天', NULL, 'base:app:im:message:read,base:app:im:message:page,base:app:im:session:page,base:app:im:session:list,base:app:im:session:unreadCount,base:app:im:session:delete', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (96, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 1, '组件预览', '/demo', NULL, 1, 'icon-favor', 0, 'cool/modules/demo/views/demo.vue', 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (97, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 27, '用户列表', '/sys/user', NULL, 1, 'icon-user', 0, 'cool/modules/base/views/user.vue', 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (98, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 97, '新增', NULL, 'base:sys:user:add', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (99, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 97, '删除', NULL, 'base:sys:user:delete', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (100, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 97, '修改', NULL, 'base:sys:user:delete,base:sys:user:update', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (101, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 97, '查询', NULL, 'base:sys:user:page,base:sys:user:list,base:sys:user:info', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (105, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 2, '监控管理', NULL, NULL, 0, 'icon-rank', 6, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (117, '2023-05-06 09:11:38.000', '2023-05-06 09:15:41.799', NULL, NULL, '任务管理', NULL, NULL, 0, 'icon-activity', 99, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (118, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 117, '任务列表', '/task', NULL, 1, 'icon-menu', 0, 'cool/modules/task/views/task.vue', 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (119, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 118, '权限', NULL, 'task:info:page,task:info:list,task:info:info,task:info:add,task:info:delete,task:info:update,task:info:stop,task:info:start,task:info:once,task:info:log', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (197, '2023-05-06 09:11:38.000', '2023-05-06 09:15:17.220', NULL, NULL, '字典管理', NULL, NULL, 0, 'icon-log', 99, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (198, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 197, '字典列表', '/dict/list', NULL, 1, 'icon-menu', 1, 'modules/dict/views/list.vue', 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (199, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 198, '删除', NULL, 'dict:info:delete', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (200, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 198, '修改', NULL, 'dict:info:update,dict:info:info', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (201, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 198, '获得字典数据', NULL, 'dict:info:data', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (202, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 198, '单个信息', NULL, 'dict:info:info', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (203, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 198, '列表查询', NULL, 'dict:info:list', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (204, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 198, '分页查询', NULL, 'dict:info:page', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (205, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 198, '新增', NULL, 'dict:info:add', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (206, '2023-05-06 09:11:38.038', '2023-05-06 09:11:38.038', NULL, 198, '组权限', NULL, 'dict:type:list,dict:type:update,dict:type:delete,dict:type:add', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (207, '2023-05-06 09:13:18.667', '2023-05-06 09:13:18.667', NULL, NULL, '会员管理', NULL, NULL, 0, 'icon-dept', 1, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (208, '2023-05-06 09:33:40.388', '2023-05-06 09:33:40.388', NULL, 207, '会员列表', '/member/user', NULL, 1, 'icon-approve', 1, 'modules/member/views/user.vue', 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (209, '2023-05-06 09:33:40.397', '2023-05-06 09:33:40.397', NULL, 208, 'add', NULL, 'member:user:add', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (210, '2023-05-06 09:33:40.397', '2023-05-06 09:33:40.397', NULL, 208, 'delete', NULL, 'member:user:delete', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (211, '2023-05-06 09:33:40.397', '2023-05-06 09:33:40.397', NULL, 208, 'info', NULL, 'member:user:info', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (212, '2023-05-06 09:33:40.397', '2023-05-06 09:33:40.397', NULL, 208, 'list', NULL, 'member:user:list', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (213, '2023-05-06 09:33:40.397', '2023-05-06 09:33:40.397', NULL, 208, 'page', NULL, 'member:user:page', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (214, '2023-05-06 09:33:40.397', '2023-05-06 09:33:40.397', NULL, 208, 'update', NULL, 'member:user:update,member:user:info', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (215, '2023-05-06 09:33:40.397', '2023-05-06 09:33:40.397', NULL, 208, 'welcome', NULL, 'member:user:welcome', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (241, '2023-05-26 04:46:29.782', '2023-05-26 04:46:29.782', NULL, NULL, '表单数据', NULL, NULL, 0, 'icon-favor', 2, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (242, '2023-05-26 04:47:33.014', '2023-05-26 04:47:33.014', NULL, 241, '留言列表', '/cms/form', NULL, 1, 'icon-app', 1, 'modules/cms/views/form.vue', 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (243, '2023-05-26 04:47:33.150', '2023-05-26 04:47:33.150', NULL, 242, 'add', NULL, 'cms:form:add', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (244, '2023-05-26 04:47:33.150', '2023-05-26 04:47:33.150', NULL, 242, 'delete', NULL, 'cms:form:delete', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (245, '2023-05-26 04:47:33.150', '2023-05-26 04:47:33.150', NULL, 242, 'info', NULL, 'cms:form:info', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (246, '2023-05-26 04:47:33.150', '2023-05-26 04:47:33.150', NULL, 242, 'list', NULL, 'cms:form:list', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (247, '2023-05-26 04:47:33.150', '2023-05-26 04:47:33.150', NULL, 242, 'page', NULL, 'cms:form:page', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (248, '2023-05-26 04:47:33.150', '2023-05-26 04:47:33.150', NULL, 242, 'update', NULL, 'cms:form:update,cms:form:info', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (249, '2023-05-26 04:47:33.150', '2023-05-26 04:47:33.150', NULL, 242, 'welcome', NULL, 'cms:form:welcome', 2, NULL, 0, NULL, 1, 1);
COMMIT;

-- ----------------------------
-- Table structure for base_sys_param
-- ----------------------------
DROP TABLE IF EXISTS `base_sys_param`;
CREATE TABLE `base_sys_param` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `keyName` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `data` text NOT NULL,
  `dataType` int(11) NOT NULL DEFAULT '0',
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_base_sys_param_deleted_at` (`deleted_at`),
  KEY `IDX_cf19b5e52d8c71caa9c4534454` (`keyName`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of base_sys_param
-- ----------------------------
BEGIN;
INSERT INTO `base_sys_param` (`id`, `createTime`, `updateTime`, `deleted_at`, `keyName`, `name`, `data`, `dataType`, `remark`) VALUES (1, '2023-05-16 15:50:22.230', '2023-05-16 15:50:22.230', NULL, 'text', '富文本参数', '<p><strong class=\"ql-size-huge\">111xxxxx2222<span class=\"ql-cursor\">﻿﻿</span></strong></p>', 0, NULL);
INSERT INTO `base_sys_param` (`id`, `createTime`, `updateTime`, `deleted_at`, `keyName`, `name`, `data`, `dataType`, `remark`) VALUES (2, '2023-05-16 15:50:22.230', '2023-05-16 15:50:22.230', NULL, 'json', 'JSON参数', '{\n    code: 111\n}', 0, NULL);
COMMIT;

-- ----------------------------
-- Table structure for base_sys_role
-- ----------------------------
DROP TABLE IF EXISTS `base_sys_role`;
CREATE TABLE `base_sys_role` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `userId` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `label` varchar(50) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `relevance` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_base_sys_role_deleted_at` (`deleted_at`),
  KEY `IDX_469d49a5998170e9550cf113da` (`name`),
  KEY `IDX_f3f24fbbccf00192b076e549a7` (`label`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of base_sys_role
-- ----------------------------
BEGIN;
INSERT INTO `base_sys_role` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `name`, `label`, `remark`, `relevance`) VALUES (1, '2023-05-16 15:50:22.143', '2023-05-16 15:50:22.143', NULL, '1', '超管', 'admin', '最高权限的角色', 1);
INSERT INTO `base_sys_role` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `name`, `label`, `remark`, `relevance`) VALUES (10, '2023-05-16 15:50:22.000', '2023-05-19 03:06:48.852', NULL, '1', '系统管理员', 'admin-sys', NULL, 1);
INSERT INTO `base_sys_role` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `name`, `label`, `remark`, `relevance`) VALUES (11, '2023-05-26 05:32:17.000', '2023-05-26 05:35:27.421', NULL, '1', '普通管理员', 'admin-vip', '普通管理员', 0);
COMMIT;

-- ----------------------------
-- Table structure for base_sys_role_department
-- ----------------------------
DROP TABLE IF EXISTS `base_sys_role_department`;
CREATE TABLE `base_sys_role_department` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `roleId` bigint(20) NOT NULL,
  `departmentId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_base_sys_role_department_deleted_at` (`deleted_at`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of base_sys_role_department
-- ----------------------------
BEGIN;
INSERT INTO `base_sys_role_department` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `departmentId`) VALUES (1, '2023-05-16 15:50:22.214', '2023-05-16 15:50:22.214', NULL, 8, 4);
INSERT INTO `base_sys_role_department` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `departmentId`) VALUES (2, '2023-05-16 15:50:22.214', '2023-05-16 15:50:22.214', NULL, 9, 1);
INSERT INTO `base_sys_role_department` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `departmentId`) VALUES (3, '2023-05-16 15:50:22.214', '2023-05-16 15:50:22.214', NULL, 9, 4);
INSERT INTO `base_sys_role_department` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `departmentId`) VALUES (4, '2023-05-16 15:50:22.214', '2023-05-16 15:50:22.214', NULL, 9, 5);
INSERT INTO `base_sys_role_department` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `departmentId`) VALUES (5, '2023-05-16 15:50:22.214', '2023-05-16 15:50:22.214', NULL, 9, 8);
INSERT INTO `base_sys_role_department` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `departmentId`) VALUES (6, '2023-05-16 15:50:22.214', '2023-05-16 15:50:22.214', NULL, 9, 9);
INSERT INTO `base_sys_role_department` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `departmentId`) VALUES (23, '2023-05-16 15:50:22.214', '2023-05-16 15:50:22.214', NULL, 12, 11);
INSERT INTO `base_sys_role_department` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `departmentId`) VALUES (27, '2023-05-16 15:50:22.214', '2023-05-16 15:50:22.214', NULL, 13, 12);
INSERT INTO `base_sys_role_department` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `departmentId`) VALUES (28, '2023-05-19 03:06:48.856', '2023-05-19 03:06:48.856', NULL, 10, 1);
INSERT INTO `base_sys_role_department` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `departmentId`) VALUES (30, '2023-05-26 05:35:27.425', '2023-05-26 05:35:27.425', NULL, 11, 14);
COMMIT;

-- ----------------------------
-- Table structure for base_sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `base_sys_role_menu`;
CREATE TABLE `base_sys_role_menu` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `roleId` bigint(20) NOT NULL,
  `menuId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_base_sys_role_menu_deleted_at` (`deleted_at`)
) ENGINE=InnoDB AUTO_INCREMENT=651 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of base_sys_role_menu
-- ----------------------------
BEGIN;
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (1, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 1);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (2, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 96);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (3, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 45);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (4, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 43);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (5, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 49);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (6, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 86);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (7, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 2);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (8, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 27);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (9, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 97);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (10, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 59);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (11, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 60);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (12, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 61);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (13, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 62);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (14, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 63);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (15, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 65);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (16, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 98);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (17, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 99);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (18, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 100);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (19, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 101);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (20, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 8);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (21, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 10);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (22, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 11);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (23, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 12);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (24, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 13);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (25, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 22);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (26, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 23);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (27, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 24);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (28, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 25);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (29, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 26);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (30, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 69);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (31, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 70);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (32, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 71);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (33, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 72);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (34, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 73);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (35, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 74);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (36, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 75);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (37, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 76);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (38, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 77);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (39, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 78);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (40, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 79);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (41, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 80);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (42, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 81);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (43, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 82);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (44, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 83);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (45, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 105);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (46, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 102);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (47, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 103);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (48, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 29);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (49, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 30);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (50, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 47);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (51, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 48);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (52, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 84);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (53, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 90);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (54, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 8, 85);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (55, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 1);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (56, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 96);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (57, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 45);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (58, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 43);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (59, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 49);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (60, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 86);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (61, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 2);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (62, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 27);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (63, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 97);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (64, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 59);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (65, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 60);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (66, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 61);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (67, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 62);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (68, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 63);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (69, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 65);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (70, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 98);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (71, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 99);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (72, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 100);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (73, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 101);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (74, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 8);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (75, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 10);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (76, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 11);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (77, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 12);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (78, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 13);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (79, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 22);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (80, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 23);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (81, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 24);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (82, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 25);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (83, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 26);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (84, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 69);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (85, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 70);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (86, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 71);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (87, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 72);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (88, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 73);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (89, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 74);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (90, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 75);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (91, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 76);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (92, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 77);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (93, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 78);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (94, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 79);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (95, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 80);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (96, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 81);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (97, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 82);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (98, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 83);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (99, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 105);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (100, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 102);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (101, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 103);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (102, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 29);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (103, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 30);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (104, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 47);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (105, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 48);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (106, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 84);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (107, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 90);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (108, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 9, 85);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (290, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 1);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (291, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 96);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (292, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 45);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (293, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 43);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (294, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 49);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (295, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 86);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (296, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 2);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (297, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 27);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (298, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 97);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (299, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 59);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (300, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 60);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (301, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 61);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (302, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 62);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (303, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 63);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (304, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 65);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (305, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 98);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (306, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 99);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (307, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 100);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (308, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 101);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (309, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 8);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (310, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 10);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (311, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 11);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (312, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 12);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (313, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 13);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (314, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 22);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (315, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 23);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (316, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 24);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (317, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 25);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (318, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 26);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (319, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 69);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (320, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 70);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (321, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 71);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (322, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 72);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (323, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 73);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (324, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 74);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (325, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 75);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (326, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 76);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (327, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 77);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (328, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 78);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (329, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 79);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (330, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 80);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (331, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 81);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (332, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 82);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (333, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 83);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (334, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 105);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (335, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 102);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (336, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 103);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (337, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 29);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (338, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 30);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (339, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 47);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (340, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 48);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (341, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 84);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (342, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 90);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (343, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 12, 85);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (463, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 1);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (464, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 96);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (465, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 45);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (466, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 43);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (467, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 49);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (468, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 86);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (469, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 2);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (470, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 27);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (471, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 97);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (472, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 59);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (473, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 60);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (474, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 61);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (475, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 62);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (476, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 63);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (477, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 65);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (478, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 98);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (479, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 99);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (480, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 100);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (481, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 101);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (482, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 8);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (483, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 10);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (484, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 11);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (485, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 12);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (486, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 13);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (487, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 22);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (488, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 23);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (489, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 24);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (490, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 25);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (491, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 26);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (492, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 69);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (493, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 70);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (494, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 71);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (495, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 72);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (496, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 73);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (497, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 74);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (498, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 75);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (499, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 76);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (500, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 77);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (501, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 78);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (502, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 79);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (503, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 80);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (504, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 81);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (505, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 82);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (506, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 83);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (507, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 105);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (508, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 102);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (509, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 103);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (510, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 29);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (511, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 30);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (512, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 47);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (513, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 48);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (514, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 84);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (515, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 90);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (516, '2023-05-16 15:50:22.159', '2023-05-16 15:50:22.159', NULL, 13, 85);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (517, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 207);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (518, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 208);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (519, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 209);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (520, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 210);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (521, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 211);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (522, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 212);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (523, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 213);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (524, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 214);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (525, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 215);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (526, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 216);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (527, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 217);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (528, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 218);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (529, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 219);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (530, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 220);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (531, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 221);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (532, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 222);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (533, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 223);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (534, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 224);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (535, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 225);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (536, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 226);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (537, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 227);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (538, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 228);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (539, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 229);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (540, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 230);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (541, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 231);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (542, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 232);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (543, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 233);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (544, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 234);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (545, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 235);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (546, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 236);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (547, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 237);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (548, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 238);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (549, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 239);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (550, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 240);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (551, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 1);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (552, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 96);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (553, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 45);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (554, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 43);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (555, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 49);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (556, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 86);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (557, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 2);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (558, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 27);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (559, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 97);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (560, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 59);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (561, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 60);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (562, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 61);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (563, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 62);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (564, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 63);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (565, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 65);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (566, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 98);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (567, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 99);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (568, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 100);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (569, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 101);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (570, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 8);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (571, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 10);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (572, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 11);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (573, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 12);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (574, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 13);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (575, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 22);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (576, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 23);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (577, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 24);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (578, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 25);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (579, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 26);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (580, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 78);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (581, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 79);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (582, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 80);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (583, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 81);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (584, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 82);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (585, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 83);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (586, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 105);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (587, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 29);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (588, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 30);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (589, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 84);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (590, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 90);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (591, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 85);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (592, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 197);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (593, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 198);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (594, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 199);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (595, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 200);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (596, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 201);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (597, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 202);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (598, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 203);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (599, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 204);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (600, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 205);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (601, '2023-05-19 03:06:48.853', '2023-05-19 03:06:48.853', NULL, 10, 206);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (622, '2023-05-26 05:35:27.423', '2023-05-26 05:35:27.423', NULL, 11, 241);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (623, '2023-05-26 05:35:27.423', '2023-05-26 05:35:27.423', NULL, 11, 242);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (624, '2023-05-26 05:35:27.423', '2023-05-26 05:35:27.423', NULL, 11, 243);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (625, '2023-05-26 05:35:27.423', '2023-05-26 05:35:27.423', NULL, 11, 244);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (626, '2023-05-26 05:35:27.423', '2023-05-26 05:35:27.423', NULL, 11, 245);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (627, '2023-05-26 05:35:27.423', '2023-05-26 05:35:27.423', NULL, 11, 246);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (628, '2023-05-26 05:35:27.423', '2023-05-26 05:35:27.423', NULL, 11, 247);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (629, '2023-05-26 05:35:27.423', '2023-05-26 05:35:27.423', NULL, 11, 248);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (630, '2023-05-26 05:35:27.423', '2023-05-26 05:35:27.423', NULL, 11, 249);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (631, '2023-05-26 05:35:27.423', '2023-05-26 05:35:27.423', NULL, 11, 1);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (632, '2023-05-26 05:35:27.423', '2023-05-26 05:35:27.423', NULL, 11, 96);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (633, '2023-05-26 05:35:27.423', '2023-05-26 05:35:27.423', NULL, 11, 45);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (634, '2023-05-26 05:35:27.423', '2023-05-26 05:35:27.423', NULL, 11, 43);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (635, '2023-05-26 05:35:27.423', '2023-05-26 05:35:27.423', NULL, 11, 49);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (636, '2023-05-26 05:35:27.423', '2023-05-26 05:35:27.423', NULL, 11, 86);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (637, '2023-05-26 05:35:27.423', '2023-05-26 05:35:27.423', NULL, 11, 47);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (638, '2023-05-26 05:35:27.423', '2023-05-26 05:35:27.423', NULL, 11, 48);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (639, '2023-05-26 05:35:27.423', '2023-05-26 05:35:27.423', NULL, 11, 85);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (640, '2023-05-26 05:35:27.423', '2023-05-26 05:35:27.423', NULL, 11, 197);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (641, '2023-05-26 05:35:27.423', '2023-05-26 05:35:27.423', NULL, 11, 198);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (642, '2023-05-26 05:35:27.423', '2023-05-26 05:35:27.423', NULL, 11, 199);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (643, '2023-05-26 05:35:27.423', '2023-05-26 05:35:27.423', NULL, 11, 200);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (644, '2023-05-26 05:35:27.423', '2023-05-26 05:35:27.423', NULL, 11, 201);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (645, '2023-05-26 05:35:27.423', '2023-05-26 05:35:27.423', NULL, 11, 202);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (646, '2023-05-26 05:35:27.423', '2023-05-26 05:35:27.423', NULL, 11, 203);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (647, '2023-05-26 05:35:27.423', '2023-05-26 05:35:27.423', NULL, 11, 204);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (648, '2023-05-26 05:35:27.423', '2023-05-26 05:35:27.423', NULL, 11, 205);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (649, '2023-05-26 05:35:27.423', '2023-05-26 05:35:27.423', NULL, 11, 206);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (650, '2023-05-26 05:35:27.423', '2023-05-26 05:35:27.423', NULL, 11, 84);
COMMIT;

-- ----------------------------
-- Table structure for base_sys_user
-- ----------------------------
DROP TABLE IF EXISTS `base_sys_user`;
CREATE TABLE `base_sys_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `departmentId` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `passwordV` int(11) NOT NULL DEFAULT '1',
  `nickName` varchar(255) DEFAULT NULL,
  `headImg` varchar(255) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `remark` varchar(255) DEFAULT NULL,
  `socketId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_base_sys_user_deleted_at` (`deleted_at`),
  KEY `idx_base_sys_user_department_id` (`departmentId`),
  KEY `idx_base_sys_user_username` (`username`),
  KEY `idx_base_sys_user_phone` (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of base_sys_user
-- ----------------------------
BEGIN;
INSERT INTO `base_sys_user` (`id`, `createTime`, `updateTime`, `deleted_at`, `departmentId`, `name`, `username`, `password`, `passwordV`, `nickName`, `headImg`, `phone`, `email`, `status`, `remark`, `socketId`) VALUES (1, '2023-05-16 15:50:22.092', '2023-05-19 03:43:11.979', NULL, 13, '超级管理员', 'admin', 'eebd18c49e04906c39a78d70e0be0abd', 4, '管理员', '/dzh/public/uploads/20230516/csnk09qqzjw0fjyzcl.jpg', '18000000000', 'team@cool-js.com', 1, '拥有最高权限的用户', NULL);
INSERT INTO `base_sys_user` (`id`, `createTime`, `updateTime`, `deleted_at`, `departmentId`, `name`, `username`, `password`, `passwordV`, `nickName`, `headImg`, `phone`, `email`, `status`, `remark`, `socketId`) VALUES (29, '2023-05-19 03:04:26.000', '2023-05-19 03:43:11.979', NULL, 13, '李政', 'viplie', 'ca4cb35839e8e630f64feb1efe840c13', 2, '政', '/dzh/public/uploads/20230519/cspmy68oz2g8byqpcl.jpeg', NULL, 'liliewin@163.com', 1, NULL, NULL);
INSERT INTO `base_sys_user` (`id`, `createTime`, `updateTime`, `deleted_at`, `departmentId`, `name`, `username`, `password`, `passwordV`, `nickName`, `headImg`, `phone`, `email`, `status`, `remark`, `socketId`) VALUES (30, '2023-05-26 05:33:22.000', '2023-05-26 05:33:53.774', NULL, 1, '普通管理员', 'king', '73b69218288a489369dac1b430672a53', 2, '普通管理员', '/dzh/public/uploads/20230526/csvoic2qiqfsx4jw7v.png', NULL, NULL, 1, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for base_sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `base_sys_user_role`;
CREATE TABLE `base_sys_user_role` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `userId` bigint(20) NOT NULL,
  `roleId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_base_sys_user_role_deleted_at` (`deleted_at`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of base_sys_user_role
-- ----------------------------
BEGIN;
INSERT INTO `base_sys_user_role` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `roleId`) VALUES (1, '2023-05-16 15:50:22.112', '2023-05-16 15:50:22.112', NULL, 1, 1);
INSERT INTO `base_sys_user_role` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `roleId`) VALUES (45, '2023-05-19 03:05:15.994', '2023-05-19 03:05:15.994', NULL, 29, 10);
INSERT INTO `base_sys_user_role` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `roleId`) VALUES (46, '2023-05-26 05:33:53.774', '2023-05-26 05:33:53.774', NULL, 30, 11);
COMMIT;

-- ----------------------------
-- Table structure for dict_info
-- ----------------------------
DROP TABLE IF EXISTS `dict_info`;
CREATE TABLE `dict_info` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `typeId` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `orderNum` int(11) NOT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_dict_info_deleted_at` (`deleted_at`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dict_info
-- ----------------------------
BEGIN;
INSERT INTO `dict_info` (`id`, `createTime`, `updateTime`, `deleted_at`, `typeId`, `name`, `orderNum`, `remark`, `parentId`) VALUES (1, '2023-05-16 15:50:22.284', '2023-05-16 15:50:22.284', NULL, 1, '衣服', 2, NULL, NULL);
INSERT INTO `dict_info` (`id`, `createTime`, `updateTime`, `deleted_at`, `typeId`, `name`, `orderNum`, `remark`, `parentId`) VALUES (2, '2023-05-16 15:50:22.284', '2023-05-16 15:50:22.284', NULL, 1, '裤子', 1, NULL, NULL);
INSERT INTO `dict_info` (`id`, `createTime`, `updateTime`, `deleted_at`, `typeId`, `name`, `orderNum`, `remark`, `parentId`) VALUES (3, '2023-05-16 15:50:22.284', '2023-05-16 15:50:22.284', NULL, 1, '鞋子', 3, NULL, NULL);
INSERT INTO `dict_info` (`id`, `createTime`, `updateTime`, `deleted_at`, `typeId`, `name`, `orderNum`, `remark`, `parentId`) VALUES (4, '2023-05-16 15:50:22.284', '2023-05-16 15:50:22.284', NULL, 2, '闪酷', 2, NULL, NULL);
INSERT INTO `dict_info` (`id`, `createTime`, `updateTime`, `deleted_at`, `typeId`, `name`, `orderNum`, `remark`, `parentId`) VALUES (5, '2023-05-16 15:50:22.284', '2023-05-16 15:50:22.284', NULL, 2, 'COOL', 1, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for dict_type
-- ----------------------------
DROP TABLE IF EXISTS `dict_type`;
CREATE TABLE `dict_type` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_dict_type_deleted_at` (`deleted_at`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dict_type
-- ----------------------------
BEGIN;
INSERT INTO `dict_type` (`id`, `createTime`, `updateTime`, `deleted_at`, `name`, `key`) VALUES (1, '2023-05-16 15:50:22.310', '2023-05-16 15:50:22.310', NULL, '类别', 'type');
INSERT INTO `dict_type` (`id`, `createTime`, `updateTime`, `deleted_at`, `name`, `key`) VALUES (2, '2023-05-16 15:50:22.310', '2023-05-16 15:50:22.310', NULL, '品牌', 'brand');
COMMIT;

-- ----------------------------
-- Table structure for dzh_cms_form
-- ----------------------------
DROP TABLE IF EXISTS `dzh_cms_form`;
CREATE TABLE `dzh_cms_form` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `name` varchar(10) NOT NULL COMMENT '姓名',
  `phone` varchar(11) NOT NULL COMMENT '号码',
  `remark` longtext NOT NULL COMMENT '题目',
  PRIMARY KEY (`id`),
  KEY `idx_dzh_cms_form_deleted_at` (`deleted_at`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dzh_cms_form
-- ----------------------------
BEGIN;
INSERT INTO `dzh_cms_form` (`id`, `createTime`, `updateTime`, `deleted_at`, `name`, `phone`, `remark`) VALUES (1, '2023-05-26 05:24:51.649', '2023-05-26 05:24:51.649', NULL, '陈', '134', '1月度线上投放金额表收入表的(1)万元</br>2月度线上客资获取数量()个</br>3月度线上客资进店数量()个</br>4月度线上签单数量()个</br>5月度线上签单总金额()万元</br>6月度其他渠道投放金额()万元</br>7月度其他渠道客资获取数量()个</br>8月度其他渠道客资获取数量(11)个</br>9月度其他渠道客资进店数量()个</br>10月度其他渠道线上签单总金额()万元</br>11销售人员总人数()个</br>12平均可售卖月子房问数量()个</br>1月度月子平均利润表收入(111)万元</br>2月度产康平均利润表收入()万元</br>3月度其他收入平均利润表收入()万元</br>3月度其他收入平均利润表收入()万元</br>4月度月子运营管理人员薪酬福利()万元</br>5月度月子护理师(月嫂）薪酬福利()万元</br>6月度房租物业费金额()万元</br>7月度水电燃气金额()万元</br>8月度月子餐食成本支出()万元</br>1月度月子物料耗材支出(1111)万元</br>2月度产康合作机构回款()万元</br>3月度产康人员薪酬福利()万元</br>4月度产康耗材成本支出()万元</br>5月度其他收入涉及的成本支出()万元</br>6月度平均销售和人员薪酬福利费()万元</br>7月度平均销售活动及其他支出()万元</br>8月度平均管理费用()万元</br>');
COMMIT;

-- ----------------------------
-- Table structure for dzh_crm_customer
-- ----------------------------
DROP TABLE IF EXISTS `dzh_crm_customer`;
CREATE TABLE `dzh_crm_customer` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `uid` bigint(20) NOT NULL COMMENT '会员',
  `customerName` varchar(50) DEFAULT NULL COMMENT '客户名称',
  `company` varchar(50) DEFAULT NULL COMMENT '公司名称',
  `address` varchar(200) DEFAULT NULL COMMENT '公司地址',
  `phone` bigint(20) DEFAULT NULL COMMENT '电话',
  `source` varchar(50) DEFAULT NULL COMMENT '来源',
  `consultation` varchar(50) DEFAULT NULL COMMENT '咨询品类',
  `isCommon` bigint(20) DEFAULT NULL COMMENT '是否公共库',
  `wx` varchar(50) DEFAULT NULL COMMENT '微信',
  `qq` varchar(50) DEFAULT NULL COMMENT 'qq',
  `email` varchar(50) DEFAULT NULL COMMENT 'email',
  `companyDomain` varchar(50) DEFAULT NULL COMMENT '公司网址',
  `orderNum` bigint(20) DEFAULT NULL COMMENT '排序',
  `remark` longtext COMMENT '备注',
  `status` int(10) DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `idx_dzh_crm_customer_deleted_at` (`deleted_at`),
  KEY `idx_dzh_crm_customer_uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dzh_crm_customer
-- ----------------------------
BEGIN;
INSERT INTO `dzh_crm_customer` (`id`, `createTime`, `updateTime`, `deleted_at`, `uid`, `customerName`, `company`, `address`, `phone`, `source`, `consultation`, `isCommon`, `wx`, `qq`, `email`, `companyDomain`, `orderNum`, `remark`, `status`) VALUES (1, '2023-05-18 17:29:55.493', '2023-05-18 17:29:55.493', NULL, 1, '测试甲方', '广州花海园林', NULL, NULL, '百度推广', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1);
COMMIT;

-- ----------------------------
-- Table structure for dzh_crm_domain
-- ----------------------------
DROP TABLE IF EXISTS `dzh_crm_domain`;
CREATE TABLE `dzh_crm_domain` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `uid` bigint(20) NOT NULL COMMENT '会员',
  `orderId` varchar(191) NOT NULL COMMENT '订单',
  `projectNum` bigint(20) DEFAULT NULL COMMENT '项目编号',
  `domain` varchar(50) DEFAULT NULL COMMENT '域名',
  `domainCompany` varchar(50) DEFAULT NULL COMMENT '域名商',
  `domainOwer` varchar(50) DEFAULT NULL COMMENT '域名所有者',
  `orderNum` bigint(20) DEFAULT '99' COMMENT '排序',
  `status` int(10) DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `idx_dzh_crm_domain_uid` (`uid`),
  KEY `idx_dzh_crm_domain_deleted_at` (`deleted_at`),
  KEY `idx_dzh_crm_domain_project_num` (`projectNum`),
  KEY `idx_dzh_crm_domain_order_id` (`orderId`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dzh_crm_domain
-- ----------------------------
BEGIN;
INSERT INTO `dzh_crm_domain` (`id`, `createTime`, `updateTime`, `deleted_at`, `uid`, `orderId`, `projectNum`, `domain`, `domainCompany`, `domainOwer`, `orderNum`, `status`) VALUES (12, '2023-05-19 15:12:12.525', '2023-05-19 15:12:12.525', NULL, 1, '16', 3100, '360.cn', NULL, NULL, 99, 1);
INSERT INTO `dzh_crm_domain` (`id`, `createTime`, `updateTime`, `deleted_at`, `uid`, `orderId`, `projectNum`, `domain`, `domainCompany`, `domainOwer`, `orderNum`, `status`) VALUES (13, '2023-05-19 15:12:12.525', '2023-05-19 15:12:12.525', NULL, 1, '16', 3100, '361.cn', NULL, NULL, 99, 1);
INSERT INTO `dzh_crm_domain` (`id`, `createTime`, `updateTime`, `deleted_at`, `uid`, `orderId`, `projectNum`, `domain`, `domainCompany`, `domainOwer`, `orderNum`, `status`) VALUES (14, '2023-05-19 15:12:12.525', '2023-05-19 15:12:12.525', NULL, 1, '16', 3100, '362.cn', NULL, NULL, 99, 1);
INSERT INTO `dzh_crm_domain` (`id`, `createTime`, `updateTime`, `deleted_at`, `uid`, `orderId`, `projectNum`, `domain`, `domainCompany`, `domainOwer`, `orderNum`, `status`) VALUES (15, '2023-05-19 15:12:12.525', '2023-05-19 15:12:12.525', NULL, 1, '16', 3100, '363.cn', NULL, NULL, 99, 1);
INSERT INTO `dzh_crm_domain` (`id`, `createTime`, `updateTime`, `deleted_at`, `uid`, `orderId`, `projectNum`, `domain`, `domainCompany`, `domainOwer`, `orderNum`, `status`) VALUES (16, '2023-05-19 15:12:12.525', '2023-05-19 15:12:12.525', NULL, 1, '16', 3100, '364.cn', NULL, NULL, 99, 1);
INSERT INTO `dzh_crm_domain` (`id`, `createTime`, `updateTime`, `deleted_at`, `uid`, `orderId`, `projectNum`, `domain`, `domainCompany`, `domainOwer`, `orderNum`, `status`) VALUES (17, '2023-05-19 15:12:39.370', '2023-05-19 15:12:39.370', NULL, 1, '17', 3101, 'baidu.com', NULL, NULL, 99, 1);
INSERT INTO `dzh_crm_domain` (`id`, `createTime`, `updateTime`, `deleted_at`, `uid`, `orderId`, `projectNum`, `domain`, `domainCompany`, `domainOwer`, `orderNum`, `status`) VALUES (18, '2023-05-19 15:12:39.370', '2023-05-19 15:12:39.370', NULL, 1, '17', 3101, 'www.baidu.com', NULL, NULL, 99, 1);
INSERT INTO `dzh_crm_domain` (`id`, `createTime`, `updateTime`, `deleted_at`, `uid`, `orderId`, `projectNum`, `domain`, `domainCompany`, `domainOwer`, `orderNum`, `status`) VALUES (19, '2023-05-19 15:12:42.471', '2023-05-19 15:12:42.471', NULL, 1, '18', 3102, 'sougou.com', NULL, NULL, 99, 1);
INSERT INTO `dzh_crm_domain` (`id`, `createTime`, `updateTime`, `deleted_at`, `uid`, `orderId`, `projectNum`, `domain`, `domainCompany`, `domainOwer`, `orderNum`, `status`) VALUES (20, '2023-05-19 15:12:42.471', '2023-05-19 15:12:42.471', NULL, 1, '18', 3102, 'www.sougou.com', NULL, NULL, 99, 1);
INSERT INTO `dzh_crm_domain` (`id`, `createTime`, `updateTime`, `deleted_at`, `uid`, `orderId`, `projectNum`, `domain`, `domainCompany`, `domainOwer`, `orderNum`, `status`) VALUES (21, '2023-05-23 20:53:33.075', '2023-05-23 20:53:33.075', NULL, 1, '19', 3103, 'xxyev.cn', NULL, NULL, 99, 1);
INSERT INTO `dzh_crm_domain` (`id`, `createTime`, `updateTime`, `deleted_at`, `uid`, `orderId`, `projectNum`, `domain`, `domainCompany`, `domainOwer`, `orderNum`, `status`) VALUES (22, '2023-05-23 20:53:33.075', '2023-05-23 20:53:33.075', NULL, 1, '19', 3103, 'www.xxyev.cn', NULL, NULL, 99, 1);
COMMIT;

-- ----------------------------
-- Table structure for dzh_crm_order
-- ----------------------------
DROP TABLE IF EXISTS `dzh_crm_order`;
CREATE TABLE `dzh_crm_order` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `uid` bigint(20) NOT NULL COMMENT '会员',
  `projectNum` bigint(20) DEFAULT NULL COMMENT '项目编号',
  `orderCode` varchar(191) DEFAULT NULL COMMENT '项目订单',
  `customerId` bigint(20) DEFAULT NULL COMMENT '客户',
  `orderType` varchar(50) DEFAULT NULL COMMENT '订单类型',
  `completeDate` varchar(50) DEFAULT NULL COMMENT '交单日期',
  `endDate` varchar(50) DEFAULT NULL COMMENT '到期时间',
  `domain` varchar(50) DEFAULT NULL COMMENT '域名',
  `isSelf` bigint(20) DEFAULT '0' COMMENT '归属',
  `isIcp` bigint(20) DEFAULT '0' COMMENT '是否备案',
  `projectInfo` varchar(255) DEFAULT NULL COMMENT '项目信息',
  `siteName` varchar(50) DEFAULT NULL COMMENT '网站名称',
  `contactor` varchar(50) DEFAULT NULL COMMENT '联系人',
  `tel` varchar(50) DEFAULT NULL COMMENT '公司座机',
  `zipCode` varchar(50) DEFAULT NULL COMMENT '传真',
  `phone` varchar(50) DEFAULT NULL COMMENT '手机',
  `email` varchar(50) DEFAULT NULL COMMENT 'email',
  `addess` varchar(50) DEFAULT NULL COMMENT '地址',
  `qq` varchar(50) DEFAULT NULL COMMENT 'qq',
  `siteUrl` varchar(50) DEFAULT NULL COMMENT '模板地址',
  `agent` bigint(20) DEFAULT NULL COMMENT '渠道',
  `WaiterId` bigint(20) DEFAULT NULL COMMENT '客服',
  `technicianId` bigint(20) DEFAULT NULL COMMENT '技术员',
  `siteManagerId` bigint(20) DEFAULT NULL COMMENT '项目对接人',
  `price` double DEFAULT NULL COMMENT '总价',
  `Deposit` double DEFAULT NULL COMMENT '定金',
  `Surplus` double DEFAULT NULL COMMENT '尾款',
  `rebate` double DEFAULT NULL COMMENT '返佣',
  `payee` varchar(50) DEFAULT NULL COMMENT '收款方',
  `paytype` varchar(50) DEFAULT NULL COMMENT '收款方式',
  `orderNum` bigint(20) DEFAULT '99' COMMENT '排序',
  `state` bigint(20) DEFAULT '0' COMMENT '项目进度',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `status` int(10) DEFAULT '1' COMMENT '状态',
  `payDate` varchar(50) DEFAULT NULL COMMENT '收款日期',
  `level` bigint(20) DEFAULT '1' COMMENT '层级',
  `parentId` bigint(20) DEFAULT '0' COMMENT '父级',
  `subNum` bigint(20) DEFAULT NULL COMMENT '子订单序号',
  PRIMARY KEY (`id`),
  KEY `idx_dzh_crm_order_deleted_at` (`deleted_at`),
  KEY `idx_dzh_crm_order_uid` (`uid`),
  KEY `idx_dzh_crm_order_project_num` (`projectNum`),
  KEY `idx_dzh_crm_order_customer_id` (`customerId`),
  KEY `idx_dzh_crm_order_waiter_id` (`WaiterId`),
  KEY `idx_dzh_crm_order_order_id` (`orderCode`),
  KEY `idx_dzh_crm_order_order_code` (`orderCode`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dzh_crm_order
-- ----------------------------
BEGIN;
INSERT INTO `dzh_crm_order` (`id`, `createTime`, `updateTime`, `deleted_at`, `uid`, `projectNum`, `orderCode`, `customerId`, `orderType`, `completeDate`, `endDate`, `domain`, `isSelf`, `isIcp`, `projectInfo`, `siteName`, `contactor`, `tel`, `zipCode`, `phone`, `email`, `addess`, `qq`, `siteUrl`, `agent`, `WaiterId`, `technicianId`, `siteManagerId`, `price`, `Deposit`, `Surplus`, `rebate`, `payee`, `paytype`, `orderNum`, `state`, `remark`, `status`, `payDate`, `level`, `parentId`, `subNum`) VALUES (16, '2023-05-18 22:49:10.000', '2023-05-19 15:12:12.524', NULL, 1, 3100, '3100', 1, 'web', NULL, '2023-05-31', '360.cn\n361.cn\n362.cn\n363.cn\n364.cn', 1, 0, NULL, '官网', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 99, 0, NULL, 1, NULL, 1, 0, NULL);
INSERT INTO `dzh_crm_order` (`id`, `createTime`, `updateTime`, `deleted_at`, `uid`, `projectNum`, `orderCode`, `customerId`, `orderType`, `completeDate`, `endDate`, `domain`, `isSelf`, `isIcp`, `projectInfo`, `siteName`, `contactor`, `tel`, `zipCode`, `phone`, `email`, `addess`, `qq`, `siteUrl`, `agent`, `WaiterId`, `technicianId`, `siteManagerId`, `price`, `Deposit`, `Surplus`, `rebate`, `payee`, `paytype`, `orderNum`, `state`, `remark`, `status`, `payDate`, `level`, `parentId`, `subNum`) VALUES (17, '2023-05-19 14:39:18.000', '2023-05-19 15:12:39.369', NULL, 1, 3101, '3101', 1, 'web', NULL, '2023-05-31', 'baidu.com\nwww.baidu.com', 1, 0, NULL, '百度官网', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 99, 0, NULL, 1, NULL, 1, 0, NULL);
INSERT INTO `dzh_crm_order` (`id`, `createTime`, `updateTime`, `deleted_at`, `uid`, `projectNum`, `orderCode`, `customerId`, `orderType`, `completeDate`, `endDate`, `domain`, `isSelf`, `isIcp`, `projectInfo`, `siteName`, `contactor`, `tel`, `zipCode`, `phone`, `email`, `addess`, `qq`, `siteUrl`, `agent`, `WaiterId`, `technicianId`, `siteManagerId`, `price`, `Deposit`, `Surplus`, `rebate`, `payee`, `paytype`, `orderNum`, `state`, `remark`, `status`, `payDate`, `level`, `parentId`, `subNum`) VALUES (18, '2023-05-19 14:39:51.000', '2023-05-19 15:12:42.470', NULL, 1, 3102, '3102', 1, 'app', NULL, '2023-05-31', 'sougou.com\nwww.sougou.com', 1, 0, NULL, '搜狗官网', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 99, 0, NULL, 1, NULL, 1, 0, NULL);
INSERT INTO `dzh_crm_order` (`id`, `createTime`, `updateTime`, `deleted_at`, `uid`, `projectNum`, `orderCode`, `customerId`, `orderType`, `completeDate`, `endDate`, `domain`, `isSelf`, `isIcp`, `projectInfo`, `siteName`, `contactor`, `tel`, `zipCode`, `phone`, `email`, `addess`, `qq`, `siteUrl`, `agent`, `WaiterId`, `technicianId`, `siteManagerId`, `price`, `Deposit`, `Surplus`, `rebate`, `payee`, `paytype`, `orderNum`, `state`, `remark`, `status`, `payDate`, `level`, `parentId`, `subNum`) VALUES (19, '2023-05-23 20:53:33.074', '2023-05-23 20:53:33.074', NULL, 1, 3103, '3103', 1, 'app', NULL, '2023-05-31', 'xxyev.cn\nwww.xxyev.cn', 1, 0, NULL, '雄心壮志', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 99, 0, NULL, 1, NULL, 1, 0, NULL);
INSERT INTO `dzh_crm_order` (`id`, `createTime`, `updateTime`, `deleted_at`, `uid`, `projectNum`, `orderCode`, `customerId`, `orderType`, `completeDate`, `endDate`, `domain`, `isSelf`, `isIcp`, `projectInfo`, `siteName`, `contactor`, `tel`, `zipCode`, `phone`, `email`, `addess`, `qq`, `siteUrl`, `agent`, `WaiterId`, `technicianId`, `siteManagerId`, `price`, `Deposit`, `Surplus`, `rebate`, `payee`, `paytype`, `orderNum`, `state`, `remark`, `status`, `payDate`, `level`, `parentId`, `subNum`) VALUES (31, '2023-05-24 16:54:59.308', '2023-05-24 16:54:59.308', NULL, 1, 3103, '3103-05241654', NULL, 'subRenewal', NULL, '2024-05-24', NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1000, NULL, NULL, 0, NULL, 'wxpay', 99, 0, NULL, 1, '2023-05-24', 2, 19, NULL);
INSERT INTO `dzh_crm_order` (`id`, `createTime`, `updateTime`, `deleted_at`, `uid`, `projectNum`, `orderCode`, `customerId`, `orderType`, `completeDate`, `endDate`, `domain`, `isSelf`, `isIcp`, `projectInfo`, `siteName`, `contactor`, `tel`, `zipCode`, `phone`, `email`, `addess`, `qq`, `siteUrl`, `agent`, `WaiterId`, `technicianId`, `siteManagerId`, `price`, `Deposit`, `Surplus`, `rebate`, `payee`, `paytype`, `orderNum`, `state`, `remark`, `status`, `payDate`, `level`, `parentId`, `subNum`) VALUES (33, '2023-05-24 17:10:25.408', '2023-05-24 17:10:25.408', NULL, 1, 3103, '3103-05241710', NULL, 'subRenewal', NULL, '2024-05-24', NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1000, NULL, NULL, 0, NULL, 'wxpay', 99, 0, NULL, 1, '2023-05-24', 2, 19, NULL);
INSERT INTO `dzh_crm_order` (`id`, `createTime`, `updateTime`, `deleted_at`, `uid`, `projectNum`, `orderCode`, `customerId`, `orderType`, `completeDate`, `endDate`, `domain`, `isSelf`, `isIcp`, `projectInfo`, `siteName`, `contactor`, `tel`, `zipCode`, `phone`, `email`, `addess`, `qq`, `siteUrl`, `agent`, `WaiterId`, `technicianId`, `siteManagerId`, `price`, `Deposit`, `Surplus`, `rebate`, `payee`, `paytype`, `orderNum`, `state`, `remark`, `status`, `payDate`, `level`, `parentId`, `subNum`) VALUES (34, '2023-05-24 17:45:14.483', '2023-05-24 17:45:14.483', NULL, 1, 3103, '3103-05241745', NULL, 'subRenewal', NULL, '2024-05-24', NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1000, NULL, NULL, 0, NULL, 'wxpay', 99, 0, NULL, 1, '2023-05-24', 2, 19, NULL);
COMMIT;

-- ----------------------------
-- Table structure for dzh_member_user
-- ----------------------------
DROP TABLE IF EXISTS `dzh_member_user`;
CREATE TABLE `dzh_member_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `avatarUrl` varchar(200) DEFAULT NULL COMMENT '头像',
  `userName` varchar(50) NOT NULL COMMENT '会员账号',
  `password` varchar(50) NOT NULL COMMENT '会员密码',
  `passwordV` int(11) NOT NULL DEFAULT '1',
  `nickName` varchar(50) DEFAULT NULL COMMENT '会员昵称',
  `levelName` varchar(50) DEFAULT NULL COMMENT '等级',
  `sex` int(11) DEFAULT '1' COMMENT '性别',
  `qq` varchar(255) DEFAULT NULL COMMENT 'qq',
  `wx` varchar(50) DEFAULT NULL COMMENT '微信',
  `wxImg` varchar(255) DEFAULT NULL COMMENT '微信二维码',
  `email` varchar(50) DEFAULT NULL COMMENT 'email',
  `role` longtext COMMENT '家庭角色',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `openid` longtext COMMENT 'openid',
  `unionid` longtext COMMENT 'unionid',
  `session_key` longtext COMMENT 'session_key',
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_dzh_member_user_nick_name` (`nickName`),
  KEY `idx_dzh_member_user_qq` (`qq`),
  KEY `idx_dzh_member_user_wx` (`wx`),
  KEY `idx_dzh_member_user_email` (`email`),
  KEY `idx_dzh_member_user_deleted_at` (`deleted_at`),
  KEY `idx_dzh_member_user_username` (`userName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dzh_member_user
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for space_info
-- ----------------------------
DROP TABLE IF EXISTS `space_info`;
CREATE TABLE `space_info` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `url` varchar(255) NOT NULL COMMENT '地址',
  `type` varchar(255) NOT NULL COMMENT '类型',
  `classifyId` bigint(20) DEFAULT NULL COMMENT '分类ID',
  PRIMARY KEY (`id`),
  KEY `idx_space_info_deleted_at` (`deleted_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of space_info
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for space_type
-- ----------------------------
DROP TABLE IF EXISTS `space_type`;
CREATE TABLE `space_type` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `name` varchar(255) NOT NULL COMMENT '类别名称 ',
  `parentId` int(11) DEFAULT NULL COMMENT '父分类ID',
  PRIMARY KEY (`id`),
  KEY `idx_space_type_deleted_at` (`deleted_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of space_type
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for task_info
-- ----------------------------
DROP TABLE IF EXISTS `task_info`;
CREATE TABLE `task_info` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `jobId` varchar(255) DEFAULT NULL COMMENT '任务ID',
  `repeatConf` longtext COMMENT '重复配置',
  `name` varchar(255) DEFAULT NULL COMMENT '任务名称',
  `cron` varchar(255) DEFAULT NULL COMMENT 'cron表达式',
  `limit` bigint(20) DEFAULT NULL COMMENT '限制次数 不传为不限制',
  `every` bigint(20) DEFAULT NULL COMMENT '间隔时间 单位秒',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `status` bigint(20) DEFAULT NULL COMMENT '状态 0:关闭 1:开启',
  `startDate` datetime(3) DEFAULT NULL COMMENT '开始时间',
  `endDate` datetime(3) DEFAULT NULL COMMENT '结束时间',
  `data` varchar(255) DEFAULT NULL COMMENT '数据',
  `service` varchar(255) DEFAULT NULL COMMENT '执行的服务',
  `type` bigint(20) DEFAULT NULL COMMENT '类型 0:系统 1:用户',
  `nextRunTime` datetime(3) DEFAULT NULL COMMENT '下次执行时间',
  `taskType` bigint(20) DEFAULT NULL COMMENT '任务类型 0:cron 1:时间间隔',
  PRIMARY KEY (`id`),
  KEY `idx_task_info_deleted_at` (`deleted_at`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of task_info
-- ----------------------------
BEGIN;
INSERT INTO `task_info` (`id`, `createTime`, `updateTime`, `deleted_at`, `jobId`, `repeatConf`, `name`, `cron`, `limit`, `every`, `remark`, `status`, `startDate`, `endDate`, `data`, `service`, `type`, `nextRunTime`, `taskType`) VALUES (1, '2023-05-16 15:50:22.495', '2023-05-26 05:27:22.581', NULL, NULL, NULL, '清理日志', '1 2 3 * * *', NULL, NULL, '每天03:02:01执行清理缓存任务', 1, NULL, NULL, NULL, 'BaseFuncClearLog(false)', 0, '2023-05-27 03:02:01.000', 0);
COMMIT;

-- ----------------------------
-- Table structure for task_log
-- ----------------------------
DROP TABLE IF EXISTS `task_log`;
CREATE TABLE `task_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `taskId` bigint(20) unsigned DEFAULT NULL COMMENT '任务ID',
  `status` tinyint(3) unsigned NOT NULL COMMENT '状态 0:失败 1:成功',
  `detail` longtext COMMENT '详情',
  PRIMARY KEY (`id`),
  KEY `idx_task_log_deleted_at` (`deleted_at`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of task_log
-- ----------------------------
BEGIN;
INSERT INTO `task_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `taskId`, `status`, `detail`) VALUES (1, '2023-05-19 03:02:01.303', '2023-05-19 03:02:01.303', NULL, 1, 1, '任务执行成功');
INSERT INTO `task_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `taskId`, `status`, `detail`) VALUES (2, '2023-05-23 03:02:01.191', '2023-05-23 03:02:01.191', NULL, 1, 1, '任务执行成功');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
