/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 50743
 Source Host           : 127.0.0.1:3306
 Source Schema         : dzh3121_go

 Target Server Type    : MySQL
 Target Server Version : 50743
 File Encoding         : 65001

 Date: 09/09/2023 00:02:36
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for base_eps_admin
-- ----------------------------
DROP TABLE IF EXISTS `base_eps_admin`;
CREATE TABLE `base_eps_admin` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `module` longtext,
  `method` longtext,
  `path` longtext,
  `prefix` longtext,
  `summary` longtext,
  `tag` longtext,
  `dts` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=915 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of base_eps_admin
-- ----------------------------
BEGIN;
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (800, 'base', 'POST', '/logout', '/admin/base/comm', 'logout', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (801, 'base', 'GET', '/permmenu', '/admin/base/comm', 'permmenu', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (802, 'base', 'GET', '/person', '/admin/base/comm', 'person', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (803, 'base', 'POST', '/personUpdate', '/admin/base/comm', 'personUpdate', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (804, 'base', 'POST', '/upload', '/admin/base/comm', 'upload', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (805, 'base', 'GET', '/uploadMode', '/admin/base/comm', 'uploadMode', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (806, 'base', 'GET', '/captcha', '/admin/base/open', 'captcha', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (807, 'base', 'GET', '/eps', '/admin/base/open', 'eps', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (808, 'base', 'POST', '/login', '/admin/base/open', 'login', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (809, 'base', 'GET', '/refreshToken', '/admin/base/open', 'refreshToken', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (810, 'base', 'POST', '/add', '/admin/base/sys/department', 'add', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (811, 'base', 'POST', '/delete', '/admin/base/sys/department', 'delete', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (812, 'base', 'GET', '/info', '/admin/base/sys/department', 'info', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (813, 'base', 'POST', '/list', '/admin/base/sys/department', 'list', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (814, 'base', 'GET', '/order', '/admin/base/sys/department', 'order', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (815, 'base', 'POST', '/page', '/admin/base/sys/department', 'page', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (816, 'base', 'POST', '/update', '/admin/base/sys/department', 'update', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (817, 'base', 'POST', '/add', '/admin/base/sys/log', 'add', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (818, 'base', 'POST', '/clear', '/admin/base/sys/log', 'clear', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (819, 'base', 'POST', '/delete', '/admin/base/sys/log', 'delete', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (820, 'base', 'GET', '/getKeep', '/admin/base/sys/log', 'getKeep', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (821, 'base', 'GET', '/info', '/admin/base/sys/log', 'info', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (822, 'base', 'POST', '/list', '/admin/base/sys/log', 'list', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (823, 'base', 'POST', '/page', '/admin/base/sys/log', 'page', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (824, 'base', 'POST', '/setKeep', '/admin/base/sys/log', 'setKeep', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (825, 'base', 'POST', '/update', '/admin/base/sys/log', 'update', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (826, 'base', 'POST', '/add', '/admin/base/sys/menu', 'add', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (827, 'base', 'POST', '/delete', '/admin/base/sys/menu', 'delete', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (828, 'base', 'GET', '/info', '/admin/base/sys/menu', 'info', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (829, 'base', 'POST', '/list', '/admin/base/sys/menu', 'list', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (830, 'base', 'POST', '/page', '/admin/base/sys/menu', 'page', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (831, 'base', 'POST', '/update', '/admin/base/sys/menu', 'update', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (832, 'base', 'POST', '/add', '/admin/base/sys/param', 'add', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (833, 'base', 'POST', '/delete', '/admin/base/sys/param', 'delete', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (834, 'base', 'GET', '/html', '/admin/base/sys/param', 'html', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (835, 'base', 'GET', '/info', '/admin/base/sys/param', 'info', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (836, 'base', 'POST', '/list', '/admin/base/sys/param', 'list', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (837, 'base', 'POST', '/page', '/admin/base/sys/param', 'page', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (838, 'base', 'POST', '/update', '/admin/base/sys/param', 'update', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (839, 'base', 'POST', '/add', '/admin/base/sys/role', 'add', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (840, 'base', 'POST', '/delete', '/admin/base/sys/role', 'delete', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (841, 'base', 'GET', '/info', '/admin/base/sys/role', 'info', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (842, 'base', 'POST', '/list', '/admin/base/sys/role', 'list', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (843, 'base', 'POST', '/page', '/admin/base/sys/role', 'page', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (844, 'base', 'POST', '/update', '/admin/base/sys/role', 'update', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (845, 'base', 'POST', '/add', '/admin/base/sys/user', 'add', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (846, 'base', 'POST', '/delete', '/admin/base/sys/user', 'delete', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (847, 'base', 'GET', '/info', '/admin/base/sys/user', 'info', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (848, 'base', 'POST', '/list', '/admin/base/sys/user', 'list', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (849, 'base', 'GET', '/move', '/admin/base/sys/user', 'move', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (850, 'base', 'POST', '/page', '/admin/base/sys/user', 'page', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (851, 'base', 'POST', '/update', '/admin/base/sys/user', 'update', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (852, 'cms', 'POST', '/add', '/admin/cms/form', 'add', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (853, 'cms', 'POST', '/delete', '/admin/cms/form', 'delete', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (854, 'cms', 'GET', '/info', '/admin/cms/form', 'info', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (855, 'cms', 'POST', '/list', '/admin/cms/form', 'list', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (856, 'cms', 'POST', '/page', '/admin/cms/form', 'page', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (857, 'cms', 'POST', '/update', '/admin/cms/form', 'update', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (858, 'cms', 'GET', '/welcome', '/admin/cms/form', 'welcome', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (859, 'cms', 'POST', '/add', '/admin/cms/lottery', 'add', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (860, 'cms', 'POST', '/delete', '/admin/cms/lottery', 'delete', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (861, 'cms', 'GET', '/info', '/admin/cms/lottery', 'info', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (862, 'cms', 'POST', '/list', '/admin/cms/lottery', 'list', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (863, 'cms', 'POST', '/page', '/admin/cms/lottery', 'page', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (864, 'cms', 'POST', '/update', '/admin/cms/lottery', 'update', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (865, 'common', 'POST', '/add', '/admin/common/setting', 'add', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (866, 'common', 'POST', '/delete', '/admin/common/setting', 'delete', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (867, 'common', 'GET', '/info', '/admin/common/setting', 'info', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (868, 'common', 'POST', '/list', '/admin/common/setting', 'list', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (869, 'common', 'POST', '/page', '/admin/common/setting', 'page', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (870, 'common', 'POST', '/update', '/admin/common/setting', 'update', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (871, 'common', 'GET', '/welcome', '/admin/common/setting', 'welcome', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (872, 'demo', 'GET', '/welcome', '/admin/demo/common_sms', 'welcome', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (873, 'dict', 'POST', '/add', '/admin/dict/info', 'add', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (874, 'dict', 'POST', '/data', '/admin/dict/info', 'data', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (875, 'dict', 'POST', '/delete', '/admin/dict/info', 'delete', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (876, 'dict', 'GET', '/info', '/admin/dict/info', 'info', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (877, 'dict', 'POST', '/list', '/admin/dict/info', 'list', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (878, 'dict', 'POST', '/page', '/admin/dict/info', 'page', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (879, 'dict', 'POST', '/update', '/admin/dict/info', 'update', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (880, 'dict', 'POST', '/add', '/admin/dict/type', 'add', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (881, 'dict', 'POST', '/delete', '/admin/dict/type', 'delete', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (882, 'dict', 'GET', '/info', '/admin/dict/type', 'info', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (883, 'dict', 'POST', '/list', '/admin/dict/type', 'list', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (884, 'dict', 'POST', '/page', '/admin/dict/type', 'page', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (885, 'dict', 'POST', '/update', '/admin/dict/type', 'update', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (886, 'member', 'POST', '/add', '/admin/member/user', 'add', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (887, 'member', 'POST', '/delete', '/admin/member/user', 'delete', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (888, 'member', 'GET', '/info', '/admin/member/user', 'info', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (889, 'member', 'POST', '/list', '/admin/member/user', 'list', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (890, 'member', 'POST', '/page', '/admin/member/user', 'page', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (891, 'member', 'POST', '/update', '/admin/member/user', 'update', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (892, 'member', 'GET', '/welcome', '/admin/member/user', 'welcome', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (893, 'space', 'POST', '/add', '/admin/space/info', 'add', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (894, 'space', 'POST', '/delete', '/admin/space/info', 'delete', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (895, 'space', 'GET', '/info', '/admin/space/info', 'info', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (896, 'space', 'POST', '/list', '/admin/space/info', 'list', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (897, 'space', 'POST', '/page', '/admin/space/info', 'page', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (898, 'space', 'POST', '/update', '/admin/space/info', 'update', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (899, 'space', 'POST', '/add', '/admin/space/type', 'add', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (900, 'space', 'POST', '/delete', '/admin/space/type', 'delete', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (901, 'space', 'GET', '/info', '/admin/space/type', 'info', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (902, 'space', 'POST', '/list', '/admin/space/type', 'list', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (903, 'space', 'POST', '/page', '/admin/space/type', 'page', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (904, 'space', 'POST', '/update', '/admin/space/type', 'update', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (905, 'task', 'POST', '/add', '/admin/task/info', 'add', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (906, 'task', 'POST', '/delete', '/admin/task/info', 'delete', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (907, 'task', 'GET', '/info', '/admin/task/info', 'info', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (908, 'task', 'POST', '/list', '/admin/task/info', 'list', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (909, 'task', 'GET', '/log', '/admin/task/info', 'log', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (910, 'task', 'POST', '/once', '/admin/task/info', 'once', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (911, 'task', 'POST', '/page', '/admin/task/info', 'page', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (912, 'task', 'GET', '/start', '/admin/task/info', 'start', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (913, 'task', 'GET', '/stop', '/admin/task/info', 'stop', '', '');
INSERT INTO `base_eps_admin` (`id`, `module`, `method`, `path`, `prefix`, `summary`, `tag`, `dts`) VALUES (914, 'task', 'POST', '/update', '/admin/task/info', 'update', '', '');
COMMIT;

-- ----------------------------
-- Table structure for base_eps_app
-- ----------------------------
DROP TABLE IF EXISTS `base_eps_app`;
CREATE TABLE `base_eps_app` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `module` longtext,
  `method` longtext,
  `path` longtext,
  `prefix` longtext,
  `summary` longtext,
  `tag` longtext,
  `dts` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of base_eps_app
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for base_sys_conf
-- ----------------------------
DROP TABLE IF EXISTS `base_sys_conf`;
CREATE TABLE `base_sys_conf` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `cKey` varchar(255) NOT NULL,
  `cValue` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_base_sys_conf_deleted_at` (`deleted_at`),
  KEY `idx_base_sys_conf_c_key` (`cKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of base_sys_conf
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for base_sys_department
-- ----------------------------
DROP TABLE IF EXISTS `base_sys_department`;
CREATE TABLE `base_sys_department` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `parentId` bigint(20) DEFAULT NULL,
  `orderNum` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_base_sys_department_deleted_at` (`deleted_at`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of base_sys_department
-- ----------------------------
BEGIN;
INSERT INTO `base_sys_department` (`id`, `createTime`, `updateTime`, `deleted_at`, `name`, `parentId`, `orderNum`) VALUES (1, '2023-09-08 10:24:20.359', '2023-09-08 10:24:20.359', NULL, 'COOL', NULL, 0);
INSERT INTO `base_sys_department` (`id`, `createTime`, `updateTime`, `deleted_at`, `name`, `parentId`, `orderNum`) VALUES (11, '2023-09-08 10:24:20.359', '2023-09-08 10:24:20.359', NULL, '开发', 1, 0);
INSERT INTO `base_sys_department` (`id`, `createTime`, `updateTime`, `deleted_at`, `name`, `parentId`, `orderNum`) VALUES (12, '2023-09-08 10:24:20.359', '2023-09-08 10:24:20.359', NULL, '测试', 1, 0);
INSERT INTO `base_sys_department` (`id`, `createTime`, `updateTime`, `deleted_at`, `name`, `parentId`, `orderNum`) VALUES (13, '2023-09-08 10:24:20.359', '2023-09-08 10:24:20.359', NULL, '游客', 1, 0);
COMMIT;

-- ----------------------------
-- Table structure for base_sys_init
-- ----------------------------
DROP TABLE IF EXISTS `base_sys_init`;
CREATE TABLE `base_sys_init` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `table` varchar(191) NOT NULL,
  `group` varchar(191) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_base_sys_init_table` (`table`),
  KEY `idx_base_sys_init_group` (`group`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of base_sys_init
-- ----------------------------
BEGIN;
INSERT INTO `base_sys_init` (`id`, `table`, `group`) VALUES (1, 'base_sys_menu', 'default');
INSERT INTO `base_sys_init` (`id`, `table`, `group`) VALUES (2, 'base_sys_user', 'default');
INSERT INTO `base_sys_init` (`id`, `table`, `group`) VALUES (3, 'base_sys_user_role', 'default');
INSERT INTO `base_sys_init` (`id`, `table`, `group`) VALUES (4, 'base_sys_role', 'default');
INSERT INTO `base_sys_init` (`id`, `table`, `group`) VALUES (5, 'base_sys_role_menu', 'default');
INSERT INTO `base_sys_init` (`id`, `table`, `group`) VALUES (6, 'base_sys_department', 'default');
INSERT INTO `base_sys_init` (`id`, `table`, `group`) VALUES (7, 'base_sys_role_department', 'default');
INSERT INTO `base_sys_init` (`id`, `table`, `group`) VALUES (8, 'base_sys_param', 'default');
INSERT INTO `base_sys_init` (`id`, `table`, `group`) VALUES (9, 'dict_info', 'default');
INSERT INTO `base_sys_init` (`id`, `table`, `group`) VALUES (10, 'dict_type', 'default');
INSERT INTO `base_sys_init` (`id`, `table`, `group`) VALUES (11, 'task_info', 'default');
COMMIT;

-- ----------------------------
-- Table structure for base_sys_log
-- ----------------------------
DROP TABLE IF EXISTS `base_sys_log`;
CREATE TABLE `base_sys_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `userId` bigint(20) unsigned DEFAULT NULL,
  `action` varchar(191) NOT NULL,
  `ip` varchar(191) DEFAULT NULL,
  `ipAddr` varchar(191) DEFAULT NULL,
  `params` longtext,
  PRIMARY KEY (`id`),
  KEY `idx_base_sys_log_deleted_at` (`deleted_at`),
  KEY `IDX_51a2caeb5713efdfcb343a8772` (`userId`),
  KEY `IDX_938f886fb40e163db174b7f6c3` (`action`),
  KEY `IDX_24e18767659f8c7142580893f2` (`ip`),
  KEY `IDX_a03a27f75cf8d502b3060823e1` (`ipAddr`)
) ENGINE=InnoDB AUTO_INCREMENT=340 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of base_sys_log
-- ----------------------------
BEGIN;
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (1, '2023-09-08 10:24:51.442', '2023-09-08 10:24:51.442', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (2, '2023-09-08 10:24:52.069', '2023-09-08 10:24:52.069', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (3, '2023-09-08 10:24:52.069', '2023-09-08 10:24:52.069', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (4, '2023-09-08 10:24:52.282', '2023-09-08 10:24:52.282', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (5, '2023-09-08 10:29:04.572', '2023-09-08 10:29:04.572', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (6, '2023-09-08 10:29:04.835', '2023-09-08 10:29:04.835', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (7, '2023-09-08 10:29:04.835', '2023-09-08 10:29:04.835', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (8, '2023-09-08 10:29:04.916', '2023-09-08 10:29:04.916', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (9, '2023-09-08 10:29:11.223', '2023-09-08 10:29:11.223', NULL, 1, 'POST:/admin/base/sys/department/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (10, '2023-09-08 10:29:11.249', '2023-09-08 10:29:11.249', NULL, 1, 'POST:/admin/base/sys/user/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"departmentIds\":[1,11,12,13],\"sort\":\"desc\",\"order\":\"createTime\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (11, '2023-09-08 10:29:12.761', '2023-09-08 10:29:12.761', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (12, '2023-09-08 10:30:51.267', '2023-09-08 10:30:51.267', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (13, '2023-09-08 10:31:15.371', '2023-09-08 10:31:15.371', NULL, 1, 'POST:/admin/base/sys/menu/add', '127.0.0.1', '127.0.0.1', '{\"type\":0,\"name\":\"内容管理\",\"isShow\":1,\"icon\":\"icon-dept\",\"orderNum\":1}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (14, '2023-09-08 10:31:15.399', '2023-09-08 10:31:15.399', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (15, '2023-09-08 10:31:39.313', '2023-09-08 10:31:39.313', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (16, '2023-09-08 10:31:39.315', '2023-09-08 10:31:39.315', NULL, 1, 'GET:/admin/base/sys/menu/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (17, '2023-09-08 10:31:44.839', '2023-09-08 10:31:44.839', NULL, 1, 'POST:/admin/base/sys/menu/update', '127.0.0.1', '127.0.0.1', '{\"type\":0,\"name\":\"工作台\",\"parentId\":null,\"isShow\":1,\"icon\":\"icon-workbench\",\"orderNum\":99,\"createTime\":\"2023-09-08 10:24:20\",\"deleted_at\":null,\"id\":1,\"updateTime\":\"2023-09-08 10:24:20\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (18, '2023-09-08 10:31:44.869', '2023-09-08 10:31:44.869', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (19, '2023-09-08 10:31:47.290', '2023-09-08 10:31:47.290', NULL, 1, 'GET:/admin/base/sys/menu/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (20, '2023-09-08 10:31:47.291', '2023-09-08 10:31:47.291', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (21, '2023-09-08 10:31:50.636', '2023-09-08 10:31:50.636', NULL, 1, 'POST:/admin/base/sys/menu/update', '127.0.0.1', '127.0.0.1', '{\"type\":0,\"name\":\"系统管理\",\"parentId\":null,\"isShow\":1,\"icon\":\"icon-system\",\"orderNum\":99,\"createTime\":\"2023-09-08 10:24:20\",\"deleted_at\":null,\"id\":2,\"updateTime\":\"2023-09-08 10:24:20\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (22, '2023-09-08 10:31:50.670', '2023-09-08 10:31:50.670', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (23, '2023-09-08 10:31:52.148', '2023-09-08 10:31:52.148', NULL, 1, 'GET:/admin/base/sys/menu/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (24, '2023-09-08 10:31:52.149', '2023-09-08 10:31:52.149', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (25, '2023-09-08 10:31:55.172', '2023-09-08 10:31:55.172', NULL, 1, 'POST:/admin/base/sys/menu/update', '127.0.0.1', '127.0.0.1', '{\"type\":0,\"name\":\"字典管理\",\"parentId\":null,\"isShow\":1,\"icon\":\"icon-log\",\"orderNum\":99,\"createTime\":\"2023-09-08 10:24:20\",\"deleted_at\":null,\"id\":197,\"updateTime\":\"2023-09-08 10:24:20\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (26, '2023-09-08 10:31:55.203', '2023-09-08 10:31:55.203', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (27, '2023-09-08 10:31:56.439', '2023-09-08 10:31:56.439', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (28, '2023-09-08 10:31:56.438', '2023-09-08 10:31:56.438', NULL, 1, 'GET:/admin/base/sys/menu/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (29, '2023-09-08 10:31:59.561', '2023-09-08 10:31:59.561', NULL, 1, 'POST:/admin/base/sys/menu/update', '127.0.0.1', '127.0.0.1', '{\"type\":0,\"name\":\"框架教程\",\"parentId\":null,\"isShow\":1,\"icon\":\"icon-task\",\"orderNum\":99,\"createTime\":\"2023-09-08 10:24:20\",\"deleted_at\":null,\"id\":47,\"updateTime\":\"2023-09-08 10:24:20\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (30, '2023-09-08 10:31:59.593', '2023-09-08 10:31:59.593', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (31, '2023-09-08 10:32:01.153', '2023-09-08 10:32:01.153', NULL, 1, 'GET:/admin/base/sys/menu/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (32, '2023-09-08 10:32:01.153', '2023-09-08 10:32:01.153', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (33, '2023-09-08 10:32:04.098', '2023-09-08 10:32:04.098', NULL, 1, 'POST:/admin/base/sys/menu/update', '127.0.0.1', '127.0.0.1', '{\"type\":0,\"name\":\"任务管理\",\"parentId\":null,\"isShow\":1,\"icon\":\"icon-activity\",\"orderNum\":99,\"createTime\":\"2023-09-08 10:24:20\",\"deleted_at\":null,\"id\":117,\"updateTime\":\"2023-09-08 10:24:20\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (34, '2023-09-08 10:32:04.121', '2023-09-08 10:32:04.121', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (35, '2023-09-08 10:32:06.996', '2023-09-08 10:32:06.996', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (36, '2023-09-08 10:32:07.060', '2023-09-08 10:32:07.060', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (37, '2023-09-08 10:32:30.577', '2023-09-08 10:32:30.577', NULL, 1, 'POST:/admin/base/sys/menu/add', '127.0.0.1', '127.0.0.1', '{\"type\":1,\"isShow\":true,\"viewPath\":\"modules/cms/views/lottery.vue\",\"module\":\"cms\",\"entity\":[\"cms\",\"lottery\"],\"name\":\"开奖列表\",\"router\":\"/cms/lottery\",\"parentId\":207,\"keepAlive\":true,\"icon\":\"icon-monitor\",\"orderNum\":1}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (38, '2023-09-08 10:32:30.598', '2023-09-08 10:32:30.598', NULL, 1, 'POST:/admin/base/sys/menu/add', '127.0.0.1', '127.0.0.1', '[{\"type\":2,\"parentId\":208,\"name\":\"add\",\"perms\":\"cms:lottery:add\"},{\"type\":2,\"parentId\":208,\"name\":\"delete\",\"perms\":\"cms:lottery:delete\"},{\"type\":2,\"parentId\":208,\"name\":\"info\",\"perms\":\"cms:lottery:info\"},{\"type\":2,\"parentId\":208,\"name\":\"list\",\"perms\":\"cms:lottery:list\"},{\"type\":2,\"parentId\":208,\"name\":\"page\",\"perms\":\"cms:lottery:page\"},{\"type\":2,\"parentId\":208,\"name\":\"update\",\"perms\":\"cms:lottery:update,cms:lottery:info\"}]');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (39, '2023-09-08 10:32:31.281', '2023-09-08 10:32:31.281', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (40, '2023-09-08 10:32:31.474', '2023-09-08 10:32:31.474', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (41, '2023-09-08 10:32:31.474', '2023-09-08 10:32:31.474', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (42, '2023-09-08 10:32:31.508', '2023-09-08 10:32:31.508', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (43, '2023-09-08 10:32:31.807', '2023-09-08 10:32:31.807', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (44, '2023-09-08 10:32:35.128', '2023-09-08 10:32:35.128', NULL, 1, 'POST:/admin/cms/lottery/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (45, '2023-09-08 10:34:27.341', '2023-09-08 10:34:27.341', NULL, 1, 'POST:/admin/cms/lottery/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (46, '2023-09-08 10:59:47.302', '2023-09-08 10:59:47.302', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":1,\"status\":1,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (47, '2023-09-08 10:59:47.302', '2023-09-08 10:59:47.302', NULL, 1, 'GET:/admin/task/info/log', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (48, '2023-09-08 10:59:47.302', '2023-09-08 10:59:47.302', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":null,\"status\":0,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (49, '2023-09-08 10:59:47.302', '2023-09-08 10:59:47.302', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":0,\"status\":1,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (50, '2023-09-08 10:59:50.911', '2023-09-08 10:59:50.911', NULL, 1, 'GET:/admin/task/info/log', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (51, '2023-09-08 10:59:52.684', '2023-09-08 10:59:52.684', NULL, 1, 'GET:/admin/task/info/log', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (52, '2023-09-08 10:59:53.710', '2023-09-08 10:59:53.710', NULL, 1, 'GET:/admin/task/info/log', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (53, '2023-09-08 10:59:56.593', '2023-09-08 10:59:56.593', NULL, 1, 'GET:/admin/task/info/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (54, '2023-09-08 11:04:11.965', '2023-09-08 11:04:11.965', NULL, 1, 'GET:/admin/task/info/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (55, '2023-09-08 11:13:50.909', '2023-09-08 11:13:50.909', NULL, 1, 'POST:/admin/task/info/add', '127.0.0.1', '127.0.0.1', '{\"type\":1,\"service\":\"CmsCrontab()\",\"name\":\"测试\",\"cron\":\"*/1 * * * *\",\"limit\":null,\"taskType\":0,\"startDate\":\"2023-09-08 11:13:47\",\"status\":1,\"every\":null}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (56, '2023-09-08 11:13:50.945', '2023-09-08 11:13:50.945', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":0,\"status\":1,\"size\":10,\"page\":1,\"total\":1}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (57, '2023-09-08 11:13:50.946', '2023-09-08 11:13:50.946', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":null,\"status\":0,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (58, '2023-09-08 11:13:50.946', '2023-09-08 11:13:50.946', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":1,\"status\":1,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (59, '2023-09-08 11:13:53.999', '2023-09-08 11:13:53.999', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":null,\"status\":0,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (60, '2023-09-08 11:13:53.999', '2023-09-08 11:13:53.999', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":0,\"status\":1,\"size\":10,\"page\":1,\"total\":1}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (61, '2023-09-08 11:13:53.999', '2023-09-08 11:13:53.999', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":1,\"status\":1,\"size\":10,\"page\":1,\"total\":1}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (62, '2023-09-08 11:13:56.447', '2023-09-08 11:13:56.447', NULL, 1, 'GET:/admin/task/info/log', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (63, '2023-09-08 11:13:58.313', '2023-09-08 11:13:58.313', NULL, 1, 'GET:/admin/task/info/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (64, '2023-09-08 11:14:01.546', '2023-09-08 11:14:01.546', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":0,\"status\":1,\"size\":10,\"page\":1,\"total\":1}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (65, '2023-09-08 11:14:01.545', '2023-09-08 11:14:01.545', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":1,\"status\":1,\"size\":10,\"page\":1,\"total\":1}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (66, '2023-09-08 11:14:01.546', '2023-09-08 11:14:01.546', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":null,\"status\":0,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (67, '2023-09-08 11:14:01.922', '2023-09-08 11:14:01.922', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":0,\"status\":1,\"size\":10,\"page\":1,\"total\":1}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (68, '2023-09-08 11:14:01.922', '2023-09-08 11:14:01.922', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":1,\"status\":1,\"size\":10,\"page\":1,\"total\":1}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (69, '2023-09-08 11:14:01.922', '2023-09-08 11:14:01.922', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":null,\"status\":0,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (70, '2023-09-08 11:14:03.425', '2023-09-08 11:14:03.425', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (71, '2023-09-08 11:14:03.604', '2023-09-08 11:14:03.604', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (72, '2023-09-08 11:14:03.605', '2023-09-08 11:14:03.605', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (73, '2023-09-08 11:14:03.628', '2023-09-08 11:14:03.628', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (74, '2023-09-08 11:14:03.706', '2023-09-08 11:14:03.706', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":0,\"status\":1,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (75, '2023-09-08 11:14:03.706', '2023-09-08 11:14:03.706', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":1,\"status\":1,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (76, '2023-09-08 11:14:03.706', '2023-09-08 11:14:03.706', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":null,\"status\":0,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (77, '2023-09-08 11:14:03.713', '2023-09-08 11:14:03.713', NULL, 1, 'GET:/admin/task/info/log', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (78, '2023-09-08 11:14:08.546', '2023-09-08 11:14:08.546', NULL, 1, 'GET:/admin/task/info/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (79, '2023-09-08 11:14:14.420', '2023-09-08 11:14:14.420', NULL, 1, 'GET:/admin/task/info/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (80, '2023-09-08 11:14:21.851', '2023-09-08 11:14:21.851', NULL, 1, 'GET:/admin/task/info/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (81, '2023-09-08 11:14:26.167', '2023-09-08 11:14:26.167', NULL, 1, 'POST:/admin/task/info/update', '127.0.0.1', '127.0.0.1', '{\"createTime\":\"2023-09-08 11:13:50\",\"cron\":\"1 * * * *\",\"data\":null,\"deleted_at\":null,\"endDate\":null,\"every\":null,\"id\":2,\"jobId\":null,\"limit\":null,\"name\":\"测试\",\"nextRunTime\":null,\"remark\":null,\"repeatConf\":null,\"service\":\"CmsCrontab()\",\"startDate\":\"2023-09-08 11:13:47\",\"status\":1,\"taskType\":0,\"type\":1,\"updateTime\":\"2023-09-08 11:13:50\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (82, '2023-09-08 11:14:26.194', '2023-09-08 11:14:26.194', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":1,\"status\":1,\"size\":10,\"page\":1,\"total\":1}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (83, '2023-09-08 11:14:26.195', '2023-09-08 11:14:26.195', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":null,\"status\":0,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (84, '2023-09-08 11:14:26.194', '2023-09-08 11:14:26.194', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":0,\"status\":1,\"size\":10,\"page\":1,\"total\":1}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (85, '2023-09-08 11:14:29.864', '2023-09-08 11:14:29.864', NULL, 1, 'GET:/admin/task/info/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (86, '2023-09-08 11:14:37.106', '2023-09-08 11:14:37.106', NULL, 1, 'POST:/admin/task/info/update', '127.0.0.1', '127.0.0.1', '{\"createTime\":\"2023-09-08 11:13:50\",\"cron\":\"1 2 3 * *\",\"data\":null,\"deleted_at\":null,\"endDate\":null,\"every\":null,\"id\":2,\"jobId\":null,\"limit\":null,\"name\":\"测试\",\"nextRunTime\":null,\"remark\":null,\"repeatConf\":null,\"service\":\"CmsCrontab()\",\"startDate\":\"2023-09-08 11:13:47\",\"status\":1,\"taskType\":0,\"type\":1,\"updateTime\":\"2023-09-08 11:14:26\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (87, '2023-09-08 11:14:37.135', '2023-09-08 11:14:37.135', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":null,\"status\":0,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (88, '2023-09-08 11:14:37.135', '2023-09-08 11:14:37.135', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":0,\"status\":1,\"size\":10,\"page\":1,\"total\":1}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (89, '2023-09-08 11:14:37.135', '2023-09-08 11:14:37.135', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":1,\"status\":1,\"size\":10,\"page\":1,\"total\":1}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (90, '2023-09-08 11:14:38.954', '2023-09-08 11:14:38.954', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (91, '2023-09-08 11:14:39.025', '2023-09-08 11:14:39.025', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (92, '2023-09-08 11:14:39.025', '2023-09-08 11:14:39.025', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (93, '2023-09-08 11:14:39.074', '2023-09-08 11:14:39.074', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (94, '2023-09-08 11:14:39.231', '2023-09-08 11:14:39.231', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":0,\"status\":1,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (95, '2023-09-08 11:14:39.231', '2023-09-08 11:14:39.231', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":1,\"status\":1,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (96, '2023-09-08 11:14:39.231', '2023-09-08 11:14:39.231', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":null,\"status\":0,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (97, '2023-09-08 11:14:39.234', '2023-09-08 11:14:39.234', NULL, 1, 'GET:/admin/task/info/log', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (98, '2023-09-08 11:14:40.614', '2023-09-08 11:14:40.614', NULL, 1, 'GET:/admin/task/info/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (99, '2023-09-08 11:14:45.012', '2023-09-08 11:14:45.012', NULL, 1, 'GET:/admin/task/info/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (100, '2023-09-08 11:14:59.819', '2023-09-08 11:14:59.819', NULL, 1, 'POST:/admin/task/info/update', '127.0.0.1', '127.0.0.1', '{\"createTime\":\"2023-09-08 11:13:50\",\"cron\":\"*/1 * * * * *\",\"data\":null,\"deleted_at\":null,\"endDate\":null,\"every\":null,\"id\":2,\"jobId\":null,\"limit\":null,\"name\":\"测试\",\"nextRunTime\":null,\"remark\":null,\"repeatConf\":null,\"service\":\"CmsCrontab()\",\"startDate\":\"2023-09-08 11:13:47\",\"status\":1,\"taskType\":0,\"type\":1,\"updateTime\":\"2023-09-08 11:14:37\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (101, '2023-09-08 11:14:59.856', '2023-09-08 11:14:59.856', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":0,\"status\":1,\"size\":10,\"page\":1,\"total\":1}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (102, '2023-09-08 11:14:59.856', '2023-09-08 11:14:59.856', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":1,\"status\":1,\"size\":10,\"page\":1,\"total\":1}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (103, '2023-09-08 11:14:59.866', '2023-09-08 11:14:59.866', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":null,\"status\":0,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (104, '2023-09-08 11:15:01.664', '2023-09-08 11:15:01.664', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (105, '2023-09-08 11:15:01.728', '2023-09-08 11:15:01.728', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (106, '2023-09-08 11:15:01.728', '2023-09-08 11:15:01.728', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (107, '2023-09-08 11:15:01.752', '2023-09-08 11:15:01.752', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (108, '2023-09-08 11:15:01.868', '2023-09-08 11:15:01.868', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":0,\"status\":1,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (109, '2023-09-08 11:15:01.868', '2023-09-08 11:15:01.868', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":1,\"status\":1,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (110, '2023-09-08 11:15:01.868', '2023-09-08 11:15:01.868', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":null,\"status\":0,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (111, '2023-09-08 11:15:01.877', '2023-09-08 11:15:01.877', NULL, 1, 'GET:/admin/task/info/log', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (112, '2023-09-08 11:15:03.305', '2023-09-08 11:15:03.305', NULL, 1, 'GET:/admin/task/info/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (113, '2023-09-08 11:15:16.202', '2023-09-08 11:15:16.202', NULL, 1, 'GET:/admin/task/info/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (114, '2023-09-08 11:15:19.454', '2023-09-08 11:15:19.454', NULL, 1, 'POST:/admin/task/info/update', '127.0.0.1', '127.0.0.1', '{\"createTime\":\"2023-09-08 11:13:50\",\"cron\":\"1 2 3 * * *\",\"data\":null,\"deleted_at\":null,\"endDate\":null,\"every\":null,\"id\":2,\"jobId\":null,\"limit\":null,\"name\":\"测试\",\"nextRunTime\":null,\"remark\":null,\"repeatConf\":null,\"service\":\"CmsCrontab()\",\"startDate\":\"2023-09-08 11:13:47\",\"status\":1,\"taskType\":0,\"type\":1,\"updateTime\":\"2023-09-08 11:14:59\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (115, '2023-09-08 11:15:19.487', '2023-09-08 11:15:19.487', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":null,\"status\":0,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (116, '2023-09-08 11:15:19.487', '2023-09-08 11:15:19.487', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":1,\"status\":1,\"size\":10,\"page\":1,\"total\":1}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (117, '2023-09-08 11:15:19.487', '2023-09-08 11:15:19.487', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":0,\"status\":1,\"size\":10,\"page\":1,\"total\":1}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (118, '2023-09-08 11:15:20.484', '2023-09-08 11:15:20.484', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (119, '2023-09-08 11:15:20.563', '2023-09-08 11:15:20.563', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (120, '2023-09-08 11:15:20.547', '2023-09-08 11:15:20.547', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (121, '2023-09-08 11:15:20.591', '2023-09-08 11:15:20.591', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (122, '2023-09-08 11:15:20.700', '2023-09-08 11:15:20.700', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":null,\"status\":0,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (123, '2023-09-08 11:15:20.700', '2023-09-08 11:15:20.700', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":0,\"status\":1,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (124, '2023-09-08 11:15:20.700', '2023-09-08 11:15:20.700', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":1,\"status\":1,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (125, '2023-09-08 11:15:20.720', '2023-09-08 11:15:20.720', NULL, 1, 'GET:/admin/task/info/log', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (126, '2023-09-08 11:15:22.742', '2023-09-08 11:15:22.742', NULL, 1, 'GET:/admin/task/info/stop', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (127, '2023-09-08 11:15:22.774', '2023-09-08 11:15:22.774', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":0,\"status\":1,\"size\":10,\"page\":1,\"total\":1}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (128, '2023-09-08 11:15:22.774', '2023-09-08 11:15:22.774', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":1,\"status\":1,\"size\":10,\"page\":1,\"total\":1}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (129, '2023-09-08 11:15:22.774', '2023-09-08 11:15:22.774', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":null,\"status\":0,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (130, '2023-09-08 11:15:26.000', '2023-09-08 11:15:26.000', NULL, 1, 'GET:/admin/task/info/start', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (131, '2023-09-08 11:15:26.032', '2023-09-08 11:15:26.032', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":1,\"status\":1,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (132, '2023-09-08 11:15:26.032', '2023-09-08 11:15:26.032', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":0,\"status\":1,\"size\":10,\"page\":1,\"total\":1}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (133, '2023-09-08 11:15:26.032', '2023-09-08 11:15:26.032', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":null,\"status\":0,\"size\":10,\"page\":1,\"total\":1}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (134, '2023-09-08 11:15:28.491', '2023-09-08 11:15:28.491', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (135, '2023-09-08 11:15:28.574', '2023-09-08 11:15:28.574', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (136, '2023-09-08 11:15:28.574', '2023-09-08 11:15:28.574', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (137, '2023-09-08 11:15:28.605', '2023-09-08 11:15:28.605', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (138, '2023-09-08 11:15:28.695', '2023-09-08 11:15:28.695', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":1,\"status\":1,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (139, '2023-09-08 11:15:28.695', '2023-09-08 11:15:28.695', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":0,\"status\":1,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (140, '2023-09-08 11:15:28.695', '2023-09-08 11:15:28.695', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":null,\"status\":0,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (141, '2023-09-08 11:15:28.698', '2023-09-08 11:15:28.698', NULL, 1, 'GET:/admin/task/info/log', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (142, '2023-09-08 11:15:35.800', '2023-09-08 11:15:35.800', NULL, 1, 'GET:/admin/task/info/stop', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (143, '2023-09-08 11:15:35.825', '2023-09-08 11:15:35.825', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":1,\"status\":1,\"size\":10,\"page\":1,\"total\":1}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (144, '2023-09-08 11:15:35.827', '2023-09-08 11:15:35.827', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":null,\"status\":0,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (145, '2023-09-08 11:15:35.826', '2023-09-08 11:15:35.826', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":0,\"status\":1,\"size\":10,\"page\":1,\"total\":1}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (146, '2023-09-08 11:15:40.206', '2023-09-08 11:15:40.206', NULL, 1, 'GET:/admin/task/info/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (147, '2023-09-08 11:15:46.857', '2023-09-08 11:15:46.857', NULL, 1, 'POST:/admin/task/info/delete', '127.0.0.1', '127.0.0.1', '{\"ids\":[2]}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (148, '2023-09-08 11:15:46.869', '2023-09-08 11:15:46.869', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":null,\"status\":0,\"size\":10,\"page\":1,\"total\":1}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (149, '2023-09-08 11:15:46.869', '2023-09-08 11:15:46.869', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":1,\"status\":1,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (150, '2023-09-08 11:15:46.869', '2023-09-08 11:15:46.869', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":0,\"status\":1,\"size\":10,\"page\":1,\"total\":1}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (151, '2023-09-08 11:16:13.932', '2023-09-08 11:16:13.932', NULL, 1, 'POST:/admin/task/info/add', '127.0.0.1', '127.0.0.1', '{\"type\":0,\"service\":\"CmsCrontab()\",\"name\":\"测试\",\"cron\":\"1 2 3 * * *\",\"limit\":null,\"taskType\":0,\"startDate\":\"2023-09-08 11:16:10\",\"status\":1,\"every\":null}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (152, '2023-09-08 11:16:13.965', '2023-09-08 11:16:13.965', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":1,\"status\":1,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (153, '2023-09-08 11:16:13.966', '2023-09-08 11:16:13.966', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":0,\"status\":1,\"size\":10,\"page\":1,\"total\":1}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (154, '2023-09-08 11:16:13.966', '2023-09-08 11:16:13.966', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":null,\"status\":0,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (155, '2023-09-08 11:17:47.048', '2023-09-08 11:17:47.048', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (156, '2023-09-08 11:17:47.191', '2023-09-08 11:17:47.191', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (157, '2023-09-08 11:17:47.191', '2023-09-08 11:17:47.191', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (158, '2023-09-08 11:17:47.287', '2023-09-08 11:17:47.287', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (159, '2023-09-08 11:17:47.365', '2023-09-08 11:17:47.365', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":1,\"status\":1,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (160, '2023-09-08 11:17:47.365', '2023-09-08 11:17:47.365', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":null,\"status\":0,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (161, '2023-09-08 11:17:47.365', '2023-09-08 11:17:47.365', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":0,\"status\":1,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (162, '2023-09-08 11:17:47.367', '2023-09-08 11:17:47.367', NULL, 1, 'GET:/admin/task/info/log', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (163, '2023-09-08 11:17:50.073', '2023-09-08 11:17:50.073', NULL, 1, 'GET:/admin/task/info/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (164, '2023-09-08 11:18:00.176', '2023-09-08 11:18:00.176', NULL, 1, 'POST:/admin/task/info/update', '127.0.0.1', '127.0.0.1', '{\"createTime\":\"2023-09-08 11:16:13\",\"cron\":\"*/1 * * * * *\",\"data\":null,\"deleted_at\":null,\"endDate\":null,\"every\":null,\"id\":3,\"jobId\":null,\"limit\":null,\"name\":\"测试\",\"nextRunTime\":\"2023-09-09 03:02:01\",\"remark\":null,\"repeatConf\":null,\"service\":\"CmsCrontab()\",\"startDate\":\"2023-09-08 11:16:10\",\"status\":1,\"taskType\":0,\"type\":0,\"updateTime\":\"2023-09-08 11:17:44\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (165, '2023-09-08 11:18:00.207', '2023-09-08 11:18:00.207', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":0,\"status\":1,\"size\":10,\"page\":1,\"total\":2}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (166, '2023-09-08 11:18:00.206', '2023-09-08 11:18:00.206', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":null,\"status\":0,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (167, '2023-09-08 11:18:00.207', '2023-09-08 11:18:00.207', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":1,\"status\":1,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (168, '2023-09-08 11:18:03.101', '2023-09-08 11:18:03.101', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (169, '2023-09-08 11:18:03.165', '2023-09-08 11:18:03.165', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (170, '2023-09-08 11:18:03.165', '2023-09-08 11:18:03.165', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (171, '2023-09-08 11:18:03.195', '2023-09-08 11:18:03.195', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (172, '2023-09-08 11:18:03.269', '2023-09-08 11:18:03.269', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":0,\"status\":1,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (173, '2023-09-08 11:18:03.269', '2023-09-08 11:18:03.269', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":1,\"status\":1,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (174, '2023-09-08 11:18:03.269', '2023-09-08 11:18:03.269', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":null,\"status\":0,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (175, '2023-09-08 11:18:03.272', '2023-09-08 11:18:03.272', NULL, 1, 'GET:/admin/task/info/log', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (176, '2023-09-08 11:18:21.922', '2023-09-08 11:18:21.922', NULL, 1, 'GET:/admin/task/info/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (177, '2023-09-08 11:18:37.227', '2023-09-08 11:18:37.227', NULL, 1, 'POST:/admin/task/info/update', '127.0.0.1', '127.0.0.1', '{\"createTime\":\"2023-09-08 11:16:13\",\"cron\":\"*/1 * * * * *\",\"data\":null,\"deleted_at\":null,\"endDate\":null,\"every\":null,\"id\":3,\"jobId\":null,\"limit\":null,\"name\":\"测试\",\"nextRunTime\":\"2023-09-08 11:18:01\",\"remark\":null,\"repeatConf\":null,\"service\":\"CmsCrontab()\",\"startDate\":\"2023-09-08 11:16:10\",\"status\":0,\"taskType\":0,\"type\":0,\"updateTime\":\"2023-09-08 11:18:00\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (178, '2023-09-08 11:18:37.274', '2023-09-08 11:18:37.274', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":0,\"status\":1,\"size\":10,\"page\":1,\"total\":2}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (179, '2023-09-08 11:18:37.274', '2023-09-08 11:18:37.274', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":1,\"status\":1,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (180, '2023-09-08 11:18:37.275', '2023-09-08 11:18:37.275', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":null,\"status\":0,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (181, '2023-09-08 11:18:47.540', '2023-09-08 11:18:47.540', NULL, 1, 'GET:/admin/task/info/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (182, '2023-09-08 11:38:17.103', '2023-09-08 11:38:17.103', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (183, '2023-09-08 11:38:17.212', '2023-09-08 11:38:17.212', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (184, '2023-09-08 11:38:17.213', '2023-09-08 11:38:17.213', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (185, '2023-09-08 11:38:17.257', '2023-09-08 11:38:17.257', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (186, '2023-09-08 11:38:17.612', '2023-09-08 11:38:17.612', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":0,\"status\":1,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (187, '2023-09-08 11:38:17.612', '2023-09-08 11:38:17.612', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":null,\"status\":0,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (188, '2023-09-08 11:38:17.612', '2023-09-08 11:38:17.612', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":1,\"status\":1,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (189, '2023-09-08 11:38:17.620', '2023-09-08 11:38:17.620', NULL, 1, 'GET:/admin/task/info/log', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (190, '2023-09-08 15:48:10.469', '2023-09-08 15:48:10.469', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (191, '2023-09-08 15:48:10.626', '2023-09-08 15:48:10.626', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (192, '2023-09-08 15:48:10.626', '2023-09-08 15:48:10.626', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (193, '2023-09-08 15:48:10.655', '2023-09-08 15:48:10.655', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (194, '2023-09-08 15:48:10.757', '2023-09-08 15:48:10.757', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":0,\"status\":1,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (195, '2023-09-08 15:48:10.757', '2023-09-08 15:48:10.757', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":1,\"status\":1,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (196, '2023-09-08 15:48:10.757', '2023-09-08 15:48:10.757', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":null,\"status\":0,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (197, '2023-09-08 15:48:10.760', '2023-09-08 15:48:10.760', NULL, 1, 'GET:/admin/task/info/log', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (198, '2023-09-08 15:48:17.975', '2023-09-08 15:48:17.975', NULL, 1, 'POST:/admin/task/info/delete', '127.0.0.1', '127.0.0.1', '{\"ids\":[3]}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (199, '2023-09-08 15:48:18.013', '2023-09-08 15:48:18.013', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":0,\"status\":1,\"size\":10,\"page\":1,\"total\":1}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (200, '2023-09-08 15:48:18.013', '2023-09-08 15:48:18.013', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":1,\"status\":1,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (201, '2023-09-08 15:48:18.013', '2023-09-08 15:48:18.013', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":null,\"status\":0,\"size\":10,\"page\":1,\"total\":1}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (202, '2023-09-08 15:49:37.786', '2023-09-08 15:49:37.786', NULL, 1, 'POST:/admin/task/info/add', '127.0.0.1', '127.0.0.1', '{\"type\":1,\"service\":\"CmsCrontab()\",\"name\":\"生成当天42期开奖号码\",\"cron\":\"* * 6 * * *\",\"limit\":null,\"taskType\":0,\"startDate\":\"2023-09-08 15:49:32\",\"status\":0,\"every\":null}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (203, '2023-09-08 15:49:37.818', '2023-09-08 15:49:37.818', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":0,\"status\":1,\"size\":10,\"page\":1,\"total\":1}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (204, '2023-09-08 15:49:37.818', '2023-09-08 15:49:37.818', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":1,\"status\":1,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (205, '2023-09-08 15:49:37.818', '2023-09-08 15:49:37.818', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":null,\"status\":0,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (206, '2023-09-08 15:49:40.750', '2023-09-08 15:49:40.750', NULL, 1, 'GET:/admin/task/info/start', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (207, '2023-09-08 15:49:40.775', '2023-09-08 15:49:40.775', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":null,\"status\":0,\"size\":10,\"page\":1,\"total\":1}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (208, '2023-09-08 15:49:40.775', '2023-09-08 15:49:40.775', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":1,\"status\":1,\"size\":10,\"page\":1,\"total\":0}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (209, '2023-09-08 15:49:40.775', '2023-09-08 15:49:40.775', NULL, 1, 'POST:/admin/task/info/page', '127.0.0.1', '127.0.0.1', '{\"type\":0,\"status\":1,\"size\":10,\"page\":1,\"total\":1}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (210, '2023-09-08 15:50:02.119', '2023-09-08 15:50:02.119', NULL, 1, 'GET:/admin/task/info/log', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (211, '2023-09-08 15:50:03.894', '2023-09-08 15:50:03.894', NULL, 1, 'GET:/admin/task/info/log', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (212, '2023-09-08 15:50:04.685', '2023-09-08 15:50:04.685', NULL, 1, 'GET:/admin/task/info/log', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (213, '2023-09-08 15:50:05.629', '2023-09-08 15:50:05.629', NULL, 1, 'GET:/admin/task/info/log', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (214, '2023-09-08 15:50:08.312', '2023-09-08 15:50:08.312', NULL, 1, 'GET:/admin/task/info/log', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (215, '2023-09-08 15:50:13.764', '2023-09-08 15:50:13.764', NULL, 1, 'GET:/admin/task/info/log', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (216, '2023-09-08 15:50:22.553', '2023-09-08 15:50:22.553', NULL, 1, 'GET:/admin/task/info/log', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (217, '2023-09-08 15:50:24.881', '2023-09-08 15:50:24.881', NULL, 1, 'GET:/admin/task/info/log', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (218, '2023-09-08 15:50:27.964', '2023-09-08 15:50:27.964', NULL, 1, 'GET:/admin/task/info/log', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (219, '2023-09-08 15:50:30.041', '2023-09-08 15:50:30.041', NULL, 1, 'GET:/admin/task/info/log', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (220, '2023-09-08 15:50:33.650', '2023-09-08 15:50:33.650', NULL, 1, 'GET:/admin/task/info/log', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (221, '2023-09-08 17:24:00.478', '2023-09-08 17:24:00.478', NULL, 1, 'POST:/admin/cms/lottery/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (222, '2023-09-08 17:24:08.185', '2023-09-08 17:24:08.185', NULL, 1, 'GET:/admin/cms/lottery/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (223, '2023-09-08 17:25:53.407', '2023-09-08 17:25:53.407', NULL, 1, 'POST:/admin/cms/lottery/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (224, '2023-09-08 17:25:56.295', '2023-09-08 17:25:56.295', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (225, '2023-09-08 17:25:56.434', '2023-09-08 17:25:56.434', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (226, '2023-09-08 17:25:56.434', '2023-09-08 17:25:56.434', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (227, '2023-09-08 17:25:56.475', '2023-09-08 17:25:56.475', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (228, '2023-09-08 17:25:56.685', '2023-09-08 17:25:56.685', NULL, 1, 'POST:/admin/cms/lottery/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (229, '2023-09-08 17:26:18.648', '2023-09-08 17:26:18.648', NULL, 1, 'POST:/admin/cms/lottery/page', '127.0.0.1', '127.0.0.1', '{\"page\":3,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (230, '2023-09-08 17:26:22.468', '2023-09-08 17:26:22.468', NULL, 1, 'POST:/admin/cms/lottery/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (231, '2023-09-08 17:38:08.667', '2023-09-08 17:38:08.667', NULL, 1, 'POST:/admin/cms/lottery/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (232, '2023-09-08 17:38:10.136', '2023-09-08 17:38:10.136', NULL, 1, 'POST:/admin/cms/lottery/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (233, '2023-09-08 17:38:32.548', '2023-09-08 17:38:32.548', NULL, 1, 'POST:/admin/cms/lottery/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (234, '2023-09-08 17:38:42.059', '2023-09-08 17:38:42.059', NULL, 1, 'POST:/admin/cms/lottery/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (235, '2023-09-08 17:38:54.990', '2023-09-08 17:38:54.990', NULL, 1, 'POST:/admin/cms/lottery/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (236, '2023-09-08 17:39:18.750', '2023-09-08 17:39:18.750', NULL, 1, 'POST:/admin/cms/lottery/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (237, '2023-09-08 17:39:23.101', '2023-09-08 17:39:23.101', NULL, 1, 'GET:/admin/cms/lottery/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (238, '2023-09-08 17:39:50.622', '2023-09-08 17:39:50.622', NULL, 1, 'POST:/admin/cms/lottery/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (239, '2023-09-08 17:39:52.602', '2023-09-08 17:39:52.602', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (240, '2023-09-08 17:39:52.716', '2023-09-08 17:39:52.716', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (241, '2023-09-08 17:39:52.716', '2023-09-08 17:39:52.716', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (242, '2023-09-08 17:39:52.743', '2023-09-08 17:39:52.743', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (243, '2023-09-08 17:39:52.933', '2023-09-08 17:39:52.933', NULL, 1, 'POST:/admin/cms/lottery/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (244, '2023-09-08 17:39:55.758', '2023-09-08 17:39:55.758', NULL, 1, 'GET:/admin/cms/lottery/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (245, '2023-09-08 19:12:26.671', '2023-09-08 19:12:26.671', NULL, 1, 'POST:/admin/base/sys/department/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (246, '2023-09-08 19:12:26.701', '2023-09-08 19:12:26.701', NULL, 1, 'POST:/admin/base/sys/user/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"departmentIds\":[1,11,12,13],\"sort\":\"desc\",\"order\":\"createTime\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (247, '2023-09-08 19:12:34.386', '2023-09-08 19:12:34.386', NULL, 1, 'POST:/admin/base/sys/user/delete', '127.0.0.1', '127.0.0.1', '{\"ids\":[24,25,26,27,28]}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (248, '2023-09-08 19:12:34.417', '2023-09-08 19:12:34.417', NULL, 1, 'POST:/admin/base/sys/user/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"departmentIds\":[1,11,12,13],\"sort\":\"desc\",\"order\":\"createTime\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (249, '2023-09-08 19:12:42.359', '2023-09-08 19:12:42.359', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (250, '2023-09-08 19:12:46.210', '2023-09-08 19:12:46.210', NULL, 1, 'POST:/admin/base/sys/role/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"sort\":\"desc\",\"order\":\"createTime\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (251, '2023-09-08 19:12:49.418', '2023-09-08 19:12:49.418', NULL, 1, 'GET:/admin/base/sys/role/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (252, '2023-09-08 19:12:49.455', '2023-09-08 19:12:49.455', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (253, '2023-09-08 19:12:49.455', '2023-09-08 19:12:49.455', NULL, 1, 'POST:/admin/base/sys/department/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (254, '2023-09-08 19:13:00.863', '2023-09-08 19:13:00.863', NULL, 1, 'POST:/admin/base/sys/role/update', '127.0.0.1', '127.0.0.1', '{\"name\":\"系统管理员\",\"label\":\"admin-sys\",\"remark\":{},\"menuIdList\":[207,208,209,210,211,212,213,214,84,90,85,117,118,119],\"relevance\":1,\"departmentIdList\":[1],\"createTime\":\"2023-09-08 10:24:20\",\"deleted_at\":{},\"id\":10,\"updateTime\":\"2023-09-08 10:24:20\",\"userId\":\"1\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (255, '2023-09-08 19:14:17.109', '2023-09-08 19:14:17.109', NULL, 1, 'POST:/admin/base/sys/role/update', '127.0.0.1', '127.0.0.1', '{\"name\":\"系统管理员\",\"label\":\"admin-sys\",\"remark\":{},\"menuIdList\":[207,208,209,210,211,212,213,214,84,90,85,117,118,119],\"relevance\":1,\"departmentIdList\":[1],\"createTime\":\"2023-09-08 10:24:20\",\"deleted_at\":{},\"id\":10,\"updateTime\":\"2023-09-08 10:24:20\",\"userId\":\"1\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (256, '2023-09-08 19:14:37.148', '2023-09-08 19:14:37.148', NULL, 1, 'POST:/admin/base/sys/role/update', '127.0.0.1', '127.0.0.1', '{\"name\":\"系统管理员\",\"label\":\"admin-sys\",\"remark\":{},\"menuIdList\":[207,208,209,210,211,212,213,214,84,90,85,117,118,119],\"relevance\":1,\"departmentIdList\":[1],\"createTime\":\"2023-09-08 10:24:20\",\"deleted_at\":{},\"id\":10,\"updateTime\":\"2023-09-08 10:24:20\",\"userId\":\"1\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (257, '2023-09-08 19:14:46.756', '2023-09-08 19:14:46.756', NULL, 1, 'POST:/admin/base/sys/role/update', '127.0.0.1', '127.0.0.1', '{\"name\":\"系统管理员\",\"label\":\"admin-sys\",\"remark\":{},\"menuIdList\":[207,208,209,210,211,212,213,214,84,90,85,117,118,119],\"relevance\":1,\"departmentIdList\":[1],\"createTime\":\"2023-09-08 10:24:20\",\"deleted_at\":{},\"id\":10,\"updateTime\":\"2023-09-08 10:24:20\",\"userId\":\"1\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (258, '2023-09-08 19:15:13.197', '2023-09-08 19:15:13.197', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (259, '2023-09-08 19:15:13.414', '2023-09-08 19:15:13.414', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (260, '2023-09-08 19:15:13.414', '2023-09-08 19:15:13.414', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (261, '2023-09-08 19:15:13.423', '2023-09-08 19:15:13.423', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (262, '2023-09-08 19:15:13.513', '2023-09-08 19:15:13.513', NULL, 1, 'POST:/admin/base/sys/role/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"sort\":\"desc\",\"order\":\"createTime\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (263, '2023-09-08 19:15:15.417', '2023-09-08 19:15:15.417', NULL, 1, 'GET:/admin/base/sys/role/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (264, '2023-09-08 19:15:15.437', '2023-09-08 19:15:15.437', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (265, '2023-09-08 19:15:15.437', '2023-09-08 19:15:15.437', NULL, 1, 'POST:/admin/base/sys/department/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (266, '2023-09-08 19:15:17.524', '2023-09-08 19:15:17.524', NULL, 1, 'POST:/admin/base/sys/role/update', '127.0.0.1', '127.0.0.1', '{\"name\":\"系统管理员\",\"label\":\"admin-sys\",\"remark\":{},\"menuIdList\":[1,96,45,43,49,86,2,27,97,59,60,61,62,63,65,98,99,100,101,8,10,11,12,13,22,23,24,25,26,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,105,102,103,29,30,47,48,84,90,85],\"relevance\":1,\"departmentIdList\":[1],\"createTime\":\"2023-09-08 10:24:20\",\"deleted_at\":{},\"id\":10,\"updateTime\":\"2023-09-08 10:24:20\",\"userId\":\"1\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (267, '2023-09-08 19:15:38.956', '2023-09-08 19:15:38.956', NULL, 1, 'POST:/admin/base/sys/role/update', '127.0.0.1', '127.0.0.1', '{\"name\":\"系统管理员\",\"label\":\"admin-sys\",\"remark\":{},\"menuIdList\":[1,96,45,43,49,86,2,27,97,59,60,61,62,63,65,98,99,100,101,8,10,11,12,13,22,23,24,25,26,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,105,102,103,29,30,47,48,84,90,85],\"relevance\":1,\"departmentIdList\":[1],\"createTime\":\"2023-09-08 10:24:20\",\"deleted_at\":{},\"id\":10,\"updateTime\":\"2023-09-08 10:24:20\",\"userId\":\"1\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (268, '2023-09-08 19:16:02.130', '2023-09-08 19:16:02.130', NULL, 1, 'GET:/admin/base/sys/role/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (269, '2023-09-08 19:16:02.184', '2023-09-08 19:16:02.184', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (270, '2023-09-08 19:16:02.184', '2023-09-08 19:16:02.184', NULL, 1, 'POST:/admin/base/sys/department/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (271, '2023-09-08 19:16:03.934', '2023-09-08 19:16:03.934', NULL, 1, 'POST:/admin/base/sys/role/update', '127.0.0.1', '127.0.0.1', '{\"name\":\"系统管理员\",\"label\":\"admin-sys\",\"remark\":{},\"menuIdList\":[1,96,45,43,49,86,2,27,97,59,60,61,62,63,65,98,99,100,101,8,10,11,12,13,22,23,24,25,26,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,105,102,103,29,30,47,48,84,90,85],\"relevance\":1,\"departmentIdList\":[1],\"createTime\":\"2023-09-08 10:24:20\",\"deleted_at\":\"2023-09-08 10:24:20\",\"id\":10,\"updateTime\":\"2023-09-08 10:24:20\",\"userId\":\"1\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (272, '2023-09-08 19:16:03.991', '2023-09-08 19:16:03.991', NULL, 1, 'POST:/admin/base/sys/role/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"sort\":\"desc\",\"order\":\"createTime\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (273, '2023-09-08 19:16:07.432', '2023-09-08 19:16:07.432', NULL, 1, 'GET:/admin/base/sys/role/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (274, '2023-09-08 19:16:07.489', '2023-09-08 19:16:07.489', NULL, 1, 'POST:/admin/base/sys/menu/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (275, '2023-09-08 19:16:07.489', '2023-09-08 19:16:07.489', NULL, 1, 'POST:/admin/base/sys/department/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (276, '2023-09-08 19:16:15.505', '2023-09-08 19:16:15.505', NULL, 1, 'POST:/admin/base/sys/role/update', '127.0.0.1', '127.0.0.1', '{\"name\":\"系统管理员\",\"label\":\"admin-sys\",\"remark\":\"{}\",\"menuIdList\":[207,208,209,210,211,212,213,214,84,90,85],\"relevance\":1,\"departmentIdList\":[1],\"createTime\":\"2023-09-08 10:24:20\",\"deleted_at\":\"2023-09-08 10:24:20\",\"id\":10,\"updateTime\":\"2023-09-08 19:16:03\",\"userId\":\"1\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (277, '2023-09-08 19:16:15.542', '2023-09-08 19:16:15.542', NULL, 1, 'POST:/admin/base/sys/role/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"sort\":\"desc\",\"order\":\"createTime\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (278, '2023-09-08 19:16:19.529', '2023-09-08 19:16:19.529', NULL, 1, 'POST:/admin/base/sys/department/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (279, '2023-09-08 19:16:19.559', '2023-09-08 19:16:19.559', NULL, 1, 'POST:/admin/base/sys/user/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"departmentIds\":[1,11,12,13],\"sort\":\"desc\",\"order\":\"createTime\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (280, '2023-09-08 19:16:21.216', '2023-09-08 19:16:21.216', NULL, 1, 'POST:/admin/base/sys/role/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (281, '2023-09-08 19:16:46.116', '2023-09-08 19:16:46.116', NULL, 1, 'POST:/admin/base/sys/user/add', '127.0.0.1', '127.0.0.1', '{\"name\":\"admin\",\"nickName\":\"admin\",\"username\":\"adminking\",\"password\":\"king1234\",\"roleIdList\":[10],\"status\":1,\"departmentId\":1}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (282, '2023-09-08 19:16:46.152', '2023-09-08 19:16:46.152', NULL, 1, 'POST:/admin/base/sys/user/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"departmentIds\":[1,11,12,13],\"sort\":\"desc\",\"order\":\"createTime\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (283, '2023-09-08 19:16:51.495', '2023-09-08 19:16:51.495', NULL, 1, 'GET:/admin/base/sys/user/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (284, '2023-09-08 19:16:51.510', '2023-09-08 19:16:51.510', NULL, 1, 'POST:/admin/base/sys/role/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (285, '2023-09-08 19:16:56.040', '2023-09-08 19:16:56.040', NULL, 1, 'POST:/admin/base/comm/logout', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (286, '2023-09-08 19:16:56.160', '2023-09-08 19:16:56.160', NULL, 0, 'GET:/admin/base/open/captcha', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (287, '2023-09-08 19:17:07.070', '2023-09-08 19:17:07.070', NULL, 0, 'POST:/admin/base/open/login', '127.0.0.1', '127.0.0.1', '{\"username\":\"adminking\",\"password\":\"king1234\",\"captchaId\":\"1eb6dhefvf0cvdhsbwj2qy0200kcw9y0\",\"verifyCode\":\"7837\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (288, '2023-09-08 19:17:07.115', '2023-09-08 19:17:07.115', NULL, 0, 'GET:/admin/base/open/captcha', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (289, '2023-09-08 19:17:16.034', '2023-09-08 19:17:16.034', NULL, 0, 'POST:/admin/base/open/login', '127.0.0.1', '127.0.0.1', '{\"username\":\"admin\",\"password\":\"adminwanyu\",\"captchaId\":\"1eb6dhefvf0cvdhsgxshlcw3004z3nra\",\"verifyCode\":\"1275\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (290, '2023-09-08 19:17:16.070', '2023-09-08 19:17:16.070', NULL, 0, 'GET:/admin/base/open/captcha', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (291, '2023-09-08 19:17:21.763', '2023-09-08 19:17:21.763', NULL, 0, 'POST:/admin/base/open/login', '127.0.0.1', '127.0.0.1', '{\"username\":\"admin\",\"password\":\"adminwanyu\",\"captchaId\":\"1eb6dhefvf0cvdhsl1ss9kw400um23pl\",\"verifyCode\":\"8399\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (292, '2023-09-08 19:17:21.800', '2023-09-08 19:17:21.800', NULL, 0, 'GET:/admin/base/open/captcha', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (293, '2023-09-08 19:17:23.804', '2023-09-08 19:17:23.804', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (294, '2023-09-08 19:17:23.983', '2023-09-08 19:17:23.983', NULL, 0, 'GET:/admin/base/open/captcha', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (295, '2023-09-08 19:17:29.361', '2023-09-08 19:17:29.361', NULL, 0, 'POST:/admin/base/open/login', '127.0.0.1', '127.0.0.1', '{\"username\":\"admin\",\"password\":\"adminwanyu\",\"captchaId\":\"1eb6dhefvf0cvdhsoors2ao600uz2qqa\",\"verifyCode\":\"8327\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (296, '2023-09-08 19:17:29.390', '2023-09-08 19:17:29.390', NULL, 0, 'GET:/admin/base/open/captcha', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (297, '2023-09-08 19:17:35.873', '2023-09-08 19:17:35.873', NULL, 0, 'POST:/admin/base/open/login', '127.0.0.1', '127.0.0.1', '{\"username\":\"admin\",\"password\":\"123456\",\"captchaId\":\"1eb6dhefvf0cvdhsr63of2w700ri7kls\",\"verifyCode\":\"8977\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (298, '2023-09-08 19:17:35.917', '2023-09-08 19:17:35.917', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (299, '2023-09-08 19:17:35.922', '2023-09-08 19:17:35.922', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (300, '2023-09-08 19:17:35.916', '2023-09-08 19:17:35.916', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (301, '2023-09-08 19:17:44.445', '2023-09-08 19:17:44.445', NULL, 1, 'POST:/admin/base/comm/personUpdate', '127.0.0.1', '127.0.0.1', '{\"headImg\":\"https://cool-admin-pro.oss-cn-shanghai.aliyuncs.com/app/c8128c24-d0e9-4e07-9c0d-6f65446e105b.png\",\"nickName\":\"管理员\",\"password\":\"adminwanyu\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (302, '2023-09-08 19:17:44.467', '2023-09-08 19:17:44.467', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (303, '2023-09-08 19:17:47.911', '2023-09-08 19:17:47.911', NULL, 1, 'POST:/admin/base/sys/department/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (304, '2023-09-08 19:17:47.938', '2023-09-08 19:17:47.938', NULL, 1, 'POST:/admin/base/sys/user/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"departmentIds\":[1,11,12,13],\"sort\":\"desc\",\"order\":\"createTime\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (305, '2023-09-08 19:17:50.893', '2023-09-08 19:17:50.893', NULL, 1, 'GET:/admin/base/sys/user/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (306, '2023-09-08 19:17:50.924', '2023-09-08 19:17:50.924', NULL, 1, 'POST:/admin/base/sys/role/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (307, '2023-09-08 19:17:53.984', '2023-09-08 19:17:53.984', NULL, 1, 'POST:/admin/base/sys/user/update', '127.0.0.1', '127.0.0.1', '{\"headImg\":null,\"name\":\"admin\",\"nickName\":\"admin\",\"username\":\"adminking\",\"roleIdList\":[10],\"phone\":null,\"email\":null,\"remark\":null,\"status\":1,\"createTime\":\"2023-09-08 19:16:46\",\"deleted_at\":null,\"departmentId\":1,\"id\":29,\"passwordV\":1,\"socketId\":null,\"updateTime\":\"2023-09-08 19:16:46\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (308, '2023-09-08 19:17:54.015', '2023-09-08 19:17:54.015', NULL, 1, 'POST:/admin/base/sys/user/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"departmentIds\":[1,11,12,13],\"sort\":\"desc\",\"order\":\"createTime\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (309, '2023-09-08 19:17:57.013', '2023-09-08 19:17:57.013', NULL, 1, 'POST:/admin/base/comm/logout', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (310, '2023-09-08 19:17:57.049', '2023-09-08 19:17:57.049', NULL, 0, 'GET:/admin/base/open/captcha', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (311, '2023-09-08 19:18:06.389', '2023-09-08 19:18:06.389', NULL, 0, 'POST:/admin/base/open/login', '127.0.0.1', '127.0.0.1', '{\"username\":\"adminking\",\"password\":\"king1234\",\"captchaId\":\"1eb6dhefvf0cvdht3vie46wa00p6k8ch\",\"verifyCode\":\"8101\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (312, '2023-09-08 19:18:06.449', '2023-09-08 19:18:06.449', NULL, 29, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (313, '2023-09-08 19:18:06.449', '2023-09-08 19:18:06.449', NULL, 29, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (314, '2023-09-08 19:18:10.716', '2023-09-08 19:18:10.716', NULL, 29, 'POST:/admin/cms/lottery/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (315, '2023-09-08 19:18:25.499', '2023-09-08 19:18:25.499', NULL, 29, 'GET:/admin/base/comm/uploadMode', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (316, '2023-09-08 19:18:26.197', '2023-09-08 19:18:26.197', NULL, 29, 'POST:/admin/base/comm/personUpdate', '127.0.0.1', '127.0.0.1', '{\"headImg\":\"/dzh/public/uploads/20230908/cvdhtgzw6pg8eqkzdo.jpg\",\"nickName\":\"admin\",\"password\":\"adminwanyu\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (317, '2023-09-08 19:18:26.239', '2023-09-08 19:18:26.239', NULL, 29, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (318, '2023-09-08 20:53:09.306', '2023-09-08 20:53:09.306', NULL, 0, 'GET:/admin/base/open/eps', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (319, '2023-09-08 20:53:09.946', '2023-09-08 20:53:09.946', NULL, 29, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (320, '2023-09-08 20:53:09.946', '2023-09-08 20:53:09.946', NULL, 29, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (321, '2023-09-08 20:53:10.329', '2023-09-08 20:53:10.329', NULL, 0, 'GET:/admin/base/open/captcha', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (322, '2023-09-08 20:53:13.922', '2023-09-08 20:53:13.922', NULL, 0, 'POST:/admin/base/open/login', '127.0.0.1', '127.0.0.1', '{\"username\":\"adminking\",\"password\":\"king1234\",\"captchaId\":\"1eb6dhekfx0cvdju0iv106o200x14pmv\",\"verifyCode\":\"2113\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (323, '2023-09-08 20:53:13.953', '2023-09-08 20:53:13.953', NULL, 0, 'GET:/admin/base/open/captcha', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (324, '2023-09-08 20:53:24.081', '2023-09-08 20:53:24.081', NULL, 0, 'POST:/admin/base/open/login', '127.0.0.1', '127.0.0.1', '{\"username\":\"admin\",\"password\":\"adminwanyu\",\"captchaId\":\"1eb6dhekfx0cvdju26nna603009o1lcw\",\"verifyCode\":\"3320\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (325, '2023-09-08 20:53:24.119', '2023-09-08 20:53:24.119', NULL, 1, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (326, '2023-09-08 20:53:24.121', '2023-09-08 20:53:24.121', NULL, 1, 'POST:/admin/dict/info/data', '127.0.0.1', '127.0.0.1', '{}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (327, '2023-09-08 20:53:24.121', '2023-09-08 20:53:24.121', NULL, 1, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (328, '2023-09-08 20:53:27.631', '2023-09-08 20:53:27.631', NULL, 1, 'POST:/admin/cms/lottery/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (329, '2023-09-08 20:53:36.697', '2023-09-08 20:53:36.697', NULL, 1, 'POST:/admin/base/sys/department/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (330, '2023-09-08 20:53:36.726', '2023-09-08 20:53:36.726', NULL, 1, 'POST:/admin/base/sys/user/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"departmentIds\":[1,11,12,13],\"sort\":\"desc\",\"order\":\"createTime\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (331, '2023-09-08 20:53:38.167', '2023-09-08 20:53:38.167', NULL, 1, 'GET:/admin/base/sys/user/info', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (332, '2023-09-08 20:53:38.199', '2023-09-08 20:53:38.199', NULL, 1, 'POST:/admin/base/sys/role/list', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (333, '2023-09-08 20:53:43.572', '2023-09-08 20:53:43.572', NULL, 1, 'POST:/admin/base/sys/user/update', '127.0.0.1', '127.0.0.1', '{\"headImg\":\"/dzh/public/uploads/20230908/cvdhtgzw6pg8eqkzdo.jpg\",\"name\":\"admin\",\"nickName\":\"admin\",\"username\":\"adminking\",\"password\":\"king1234\",\"roleIdList\":[10],\"phone\":null,\"email\":null,\"remark\":null,\"status\":1,\"createTime\":\"2023-09-08 19:16:46\",\"deleted_at\":null,\"departmentId\":1,\"id\":29,\"passwordV\":2,\"socketId\":null,\"updateTime\":\"2023-09-08 19:18:26\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (334, '2023-09-08 20:53:43.593', '2023-09-08 20:53:43.593', NULL, 1, 'POST:/admin/base/sys/user/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20,\"departmentIds\":[1,11,12,13],\"sort\":\"desc\",\"order\":\"createTime\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (335, '2023-09-08 23:58:08.793', '2023-09-08 23:58:08.793', NULL, 0, 'GET:/admin/base/open/captcha', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (336, '2023-09-08 23:58:56.965', '2023-09-08 23:58:56.965', NULL, 0, 'POST:/admin/base/open/login', '127.0.0.1', '127.0.0.1', '{\"username\":\"adminking\",\"password\":\"king1234\",\"captchaId\":\"10yol1etue0cvdnrn31xysw200h71cnk\",\"verifyCode\":\"5946\"}');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (337, '2023-09-08 23:58:57.014', '2023-09-08 23:58:57.014', NULL, 29, 'GET:/admin/base/comm/person', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (338, '2023-09-08 23:58:57.014', '2023-09-08 23:58:57.014', NULL, 29, 'GET:/admin/base/comm/permmenu', '127.0.0.1', '127.0.0.1', '');
INSERT INTO `base_sys_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `action`, `ip`, `ipAddr`, `params`) VALUES (339, '2023-09-08 23:59:01.106', '2023-09-08 23:59:01.106', NULL, 29, 'POST:/admin/cms/lottery/page', '127.0.0.1', '127.0.0.1', '{\"page\":1,\"size\":20}');
COMMIT;

-- ----------------------------
-- Table structure for base_sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `base_sys_menu`;
CREATE TABLE `base_sys_menu` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `parentId` bigint(20) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `router` varchar(255) DEFAULT NULL,
  `perms` varchar(255) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `orderNum` int(11) NOT NULL DEFAULT '0',
  `viewPath` varchar(255) DEFAULT NULL,
  `keepAlive` int(11) NOT NULL DEFAULT '1',
  `isShow` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_base_sys_menu_deleted_at` (`deleted_at`)
) ENGINE=InnoDB AUTO_INCREMENT=215 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of base_sys_menu
-- ----------------------------
BEGIN;
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (1, '2023-09-08 10:24:20.000', '2023-09-08 10:31:44.845', NULL, NULL, '工作台', '/', NULL, 0, 'icon-workbench', 99, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (2, '2023-09-08 10:24:20.000', '2023-09-08 10:31:50.639', NULL, NULL, '系统管理', '/sys', NULL, 0, 'icon-system', 99, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (8, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 27, '菜单列表', '/sys/menu', NULL, 1, 'icon-menu', 2, 'cool/modules/base/views/menu.vue', 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (10, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 8, '新增', NULL, 'base:sys:menu:add', 2, NULL, 1, NULL, 0, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (11, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 8, '删除', NULL, 'base:sys:menu:delete', 2, NULL, 2, NULL, 0, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (12, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 8, '修改', NULL, 'base:sys:menu:update', 2, NULL, 3, NULL, 0, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (13, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 8, '查询', NULL, 'base:sys:menu:page,base:sys:menu:list,base:sys:menu:info', 2, NULL, 4, NULL, 0, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (22, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 27, '角色列表', '/sys/role', NULL, 1, 'icon-common', 3, 'cool/modules/base/views/role.vue', 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (23, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 22, '新增', NULL, 'base:sys:role:add', 2, NULL, 1, NULL, 0, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (24, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 22, '删除', NULL, 'base:sys:role:delete', 2, NULL, 2, NULL, 0, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (25, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 22, '修改', NULL, 'base:sys:role:update', 2, NULL, 3, NULL, 0, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (26, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 22, '查询', NULL, 'base:sys:role:page,base:sys:role:list,base:sys:role:info', 2, NULL, 4, NULL, 0, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (27, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 2, '权限管理', NULL, NULL, 0, 'icon-auth', 1, NULL, 0, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (29, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 105, '请求日志', '/sys/log', NULL, 1, 'icon-log', 1, 'cool/modules/base/views/log.vue', 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (30, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 29, '权限', NULL, 'base:sys:log:page,base:sys:log:clear,base:sys:log:getKeep,base:sys:log:setKeep', 2, NULL, 1, NULL, 0, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (43, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 45, 'crud 示例', '/crud', NULL, 1, 'icon-favor', 1, 'cool/modules/demo/views/crud.vue', 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (45, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 1, '组件库', '/ui-lib', NULL, 0, 'icon-common', 2, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (47, '2023-09-08 10:24:20.000', '2023-09-08 10:31:59.569', NULL, NULL, '框架教程', '/tutorial', NULL, 0, 'icon-task', 99, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (48, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 47, '文档', '/tutorial/doc', NULL, 1, 'icon-log', 0, 'https://cool-js.com', 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (49, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 45, 'quill 富文本编辑器', '/editor-quill', NULL, 1, 'icon-favor', 2, 'cool/modules/demo/views/editor-quill.vue', 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (59, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 97, '部门列表', NULL, 'base:sys:department:list', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (60, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 97, '新增部门', NULL, 'base:sys:department:add', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (61, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 97, '更新部门', NULL, 'base:sys:department:update', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (62, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 97, '删除部门', NULL, 'base:sys:department:delete', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (63, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 97, '部门排序', NULL, 'base:sys:department:order', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (65, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 97, '用户转移', NULL, 'base:sys:user:move', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (78, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 2, '参数配置', NULL, NULL, 0, 'icon-common', 4, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (79, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 78, '参数列表', '/sys/param', NULL, 1, 'icon-menu', 0, 'cool/modules/base/views/param.vue', 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (80, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 79, '新增', NULL, 'base:sys:param:add', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (81, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 79, '修改', NULL, 'base:sys:param:info,base:sys:param:update', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (82, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 79, '删除', NULL, 'base:sys:param:delete', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (83, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 79, '查看', NULL, 'base:sys:param:page,base:sys:param:list,base:sys:param:info', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (84, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, NULL, '通用', NULL, NULL, 0, 'icon-radioboxfill', 99, NULL, 1, 0);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (85, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 84, '图片上传', NULL, 'space:info:page,space:info:list,space:info:info,space:info:add,space:info:delete,space:info:update,space:type:page,space:type:list,space:type:info,space:type:add,space:type:delete,space:type:update', 2, NULL, 1, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (86, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 45, '文件上传', '/upload', NULL, 1, 'icon-favor', 3, 'cool/modules/demo/views/upload.vue', 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (90, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 84, '客服聊天', NULL, 'base:app:im:message:read,base:app:im:message:page,base:app:im:session:page,base:app:im:session:list,base:app:im:session:unreadCount,base:app:im:session:delete', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (96, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 1, '组件预览', '/demo', NULL, 1, 'icon-favor', 0, 'cool/modules/demo/views/demo.vue', 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (97, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 27, '用户列表', '/sys/user', NULL, 1, 'icon-user', 0, 'cool/modules/base/views/user.vue', 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (98, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 97, '新增', NULL, 'base:sys:user:add', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (99, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 97, '删除', NULL, 'base:sys:user:delete', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (100, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 97, '修改', NULL, 'base:sys:user:delete,base:sys:user:update', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (101, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 97, '查询', NULL, 'base:sys:user:page,base:sys:user:list,base:sys:user:info', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (105, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 2, '监控管理', NULL, NULL, 0, 'icon-rank', 6, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (117, '2023-09-08 10:24:20.000', '2023-09-08 10:32:04.100', NULL, NULL, '任务管理', NULL, NULL, 0, 'icon-activity', 99, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (118, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 117, '任务列表', '/task', NULL, 1, 'icon-menu', 0, 'cool/modules/task/views/task.vue', 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (119, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 118, '权限', NULL, 'task:info:page,task:info:list,task:info:info,task:info:add,task:info:delete,task:info:update,task:info:stop,task:info:start,task:info:once,task:info:log', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (197, '2023-09-08 10:24:20.000', '2023-09-08 10:31:55.177', NULL, NULL, '字典管理', NULL, NULL, 0, 'icon-log', 99, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (198, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 197, '字典列表', '/dict/list', NULL, 1, 'icon-menu', 1, 'modules/dict/views/list.vue', 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (199, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 198, '删除', NULL, 'dict:info:delete', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (200, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 198, '修改', NULL, 'dict:info:update,dict:info:info', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (201, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 198, '获得字典数据', NULL, 'dict:info:data', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (202, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 198, '单个信息', NULL, 'dict:info:info', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (203, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 198, '列表查询', NULL, 'dict:info:list', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (204, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 198, '分页查询', NULL, 'dict:info:page', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (205, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 198, '新增', NULL, 'dict:info:add', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (206, '2023-09-08 10:24:20.277', '2023-09-08 10:24:20.277', NULL, 198, '组权限', NULL, 'dict:type:list,dict:type:update,dict:type:delete,dict:type:add', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (207, '2023-09-08 10:31:15.374', '2023-09-08 10:31:15.374', NULL, NULL, '内容管理', NULL, NULL, 0, 'icon-dept', 1, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (208, '2023-09-08 10:32:30.587', '2023-09-08 10:32:30.587', NULL, 207, '开奖列表', '/cms/lottery', NULL, 1, 'icon-monitor', 1, 'modules/cms/views/lottery.vue', 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (209, '2023-09-08 10:32:30.600', '2023-09-08 10:32:30.600', NULL, 208, 'add', NULL, 'cms:lottery:add', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (210, '2023-09-08 10:32:30.600', '2023-09-08 10:32:30.600', NULL, 208, 'delete', NULL, 'cms:lottery:delete', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (211, '2023-09-08 10:32:30.600', '2023-09-08 10:32:30.600', NULL, 208, 'info', NULL, 'cms:lottery:info', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (212, '2023-09-08 10:32:30.600', '2023-09-08 10:32:30.600', NULL, 208, 'list', NULL, 'cms:lottery:list', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (213, '2023-09-08 10:32:30.600', '2023-09-08 10:32:30.600', NULL, 208, 'page', NULL, 'cms:lottery:page', 2, NULL, 0, NULL, 1, 1);
INSERT INTO `base_sys_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `parentId`, `name`, `router`, `perms`, `type`, `icon`, `orderNum`, `viewPath`, `keepAlive`, `isShow`) VALUES (214, '2023-09-08 10:32:30.600', '2023-09-08 10:32:30.600', NULL, 208, 'update', NULL, 'cms:lottery:update,cms:lottery:info', 2, NULL, 0, NULL, 1, 1);
COMMIT;

-- ----------------------------
-- Table structure for base_sys_param
-- ----------------------------
DROP TABLE IF EXISTS `base_sys_param`;
CREATE TABLE `base_sys_param` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `keyName` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `data` text NOT NULL,
  `dataType` int(11) NOT NULL DEFAULT '0',
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_base_sys_param_deleted_at` (`deleted_at`),
  KEY `IDX_cf19b5e52d8c71caa9c4534454` (`keyName`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of base_sys_param
-- ----------------------------
BEGIN;
INSERT INTO `base_sys_param` (`id`, `createTime`, `updateTime`, `deleted_at`, `keyName`, `name`, `data`, `dataType`, `remark`) VALUES (1, '2023-09-08 10:24:20.396', '2023-09-08 10:24:20.396', NULL, 'text', '富文本参数', '<p><strong class=\"ql-size-huge\">111xxxxx2222<span class=\"ql-cursor\">﻿﻿</span></strong></p>', 0, NULL);
INSERT INTO `base_sys_param` (`id`, `createTime`, `updateTime`, `deleted_at`, `keyName`, `name`, `data`, `dataType`, `remark`) VALUES (2, '2023-09-08 10:24:20.396', '2023-09-08 10:24:20.396', NULL, 'json', 'JSON参数', '{\n    code: 111\n}', 0, NULL);
COMMIT;

-- ----------------------------
-- Table structure for base_sys_role
-- ----------------------------
DROP TABLE IF EXISTS `base_sys_role`;
CREATE TABLE `base_sys_role` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `userId` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `label` varchar(50) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `relevance` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_base_sys_role_deleted_at` (`deleted_at`),
  KEY `IDX_469d49a5998170e9550cf113da` (`name`),
  KEY `IDX_f3f24fbbccf00192b076e549a7` (`label`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of base_sys_role
-- ----------------------------
BEGIN;
INSERT INTO `base_sys_role` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `name`, `label`, `remark`, `relevance`) VALUES (1, '2023-09-08 10:24:20.330', '2023-09-08 10:24:20.330', '2023-09-08 10:24:20.330', '1', '超管', 'admin', '最高权限的角色', 1);
INSERT INTO `base_sys_role` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `name`, `label`, `remark`, `relevance`) VALUES (10, '2023-09-08 10:24:20.000', '2023-09-08 19:16:15.509', '2023-09-08 10:24:20.000', '1', '系统管理员', 'admin-sys', '{}', 1);
INSERT INTO `base_sys_role` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `name`, `label`, `remark`, `relevance`) VALUES (11, '2023-09-08 10:24:20.330', '2023-09-08 10:24:20.330', NULL, '1', '游客', 'visitor', NULL, 0);
INSERT INTO `base_sys_role` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `name`, `label`, `remark`, `relevance`) VALUES (12, '2023-09-08 10:24:20.330', '2023-09-08 10:24:20.330', NULL, '1', '开发', 'dev', NULL, 0);
INSERT INTO `base_sys_role` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `name`, `label`, `remark`, `relevance`) VALUES (13, '2023-09-08 10:24:20.330', '2023-09-08 10:24:20.330', NULL, '1', '测试', 'test', NULL, 0);
COMMIT;

-- ----------------------------
-- Table structure for base_sys_role_department
-- ----------------------------
DROP TABLE IF EXISTS `base_sys_role_department`;
CREATE TABLE `base_sys_role_department` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `roleId` bigint(20) NOT NULL,
  `departmentId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_base_sys_role_department_deleted_at` (`deleted_at`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of base_sys_role_department
-- ----------------------------
BEGIN;
INSERT INTO `base_sys_role_department` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `departmentId`) VALUES (1, '2023-09-08 10:24:20.379', '2023-09-08 10:24:20.379', NULL, 8, 4);
INSERT INTO `base_sys_role_department` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `departmentId`) VALUES (2, '2023-09-08 10:24:20.379', '2023-09-08 10:24:20.379', NULL, 9, 1);
INSERT INTO `base_sys_role_department` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `departmentId`) VALUES (3, '2023-09-08 10:24:20.379', '2023-09-08 10:24:20.379', NULL, 9, 4);
INSERT INTO `base_sys_role_department` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `departmentId`) VALUES (4, '2023-09-08 10:24:20.379', '2023-09-08 10:24:20.379', NULL, 9, 5);
INSERT INTO `base_sys_role_department` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `departmentId`) VALUES (5, '2023-09-08 10:24:20.379', '2023-09-08 10:24:20.379', NULL, 9, 8);
INSERT INTO `base_sys_role_department` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `departmentId`) VALUES (6, '2023-09-08 10:24:20.379', '2023-09-08 10:24:20.379', NULL, 9, 9);
INSERT INTO `base_sys_role_department` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `departmentId`) VALUES (23, '2023-09-08 10:24:20.379', '2023-09-08 10:24:20.379', NULL, 12, 11);
INSERT INTO `base_sys_role_department` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `departmentId`) VALUES (27, '2023-09-08 10:24:20.379', '2023-09-08 10:24:20.379', NULL, 13, 12);
INSERT INTO `base_sys_role_department` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `departmentId`) VALUES (29, '2023-09-08 19:16:15.520', '2023-09-08 19:16:15.520', NULL, 10, 1);
COMMIT;

-- ----------------------------
-- Table structure for base_sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `base_sys_role_menu`;
CREATE TABLE `base_sys_role_menu` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `roleId` bigint(20) NOT NULL,
  `menuId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_base_sys_role_menu_deleted_at` (`deleted_at`)
) ENGINE=InnoDB AUTO_INCREMENT=582 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of base_sys_role_menu
-- ----------------------------
BEGIN;
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (1, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 1);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (2, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 96);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (3, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 45);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (4, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 43);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (5, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 49);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (6, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 86);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (7, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 2);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (8, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 27);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (9, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 97);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (10, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 59);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (11, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 60);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (12, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 61);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (13, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 62);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (14, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 63);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (15, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 65);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (16, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 98);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (17, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 99);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (18, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 100);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (19, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 101);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (20, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 8);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (21, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 10);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (22, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 11);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (23, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 12);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (24, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 13);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (25, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 22);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (26, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 23);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (27, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 24);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (28, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 25);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (29, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 26);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (30, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 69);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (31, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 70);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (32, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 71);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (33, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 72);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (34, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 73);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (35, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 74);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (36, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 75);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (37, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 76);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (38, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 77);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (39, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 78);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (40, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 79);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (41, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 80);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (42, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 81);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (43, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 82);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (44, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 83);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (45, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 105);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (46, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 102);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (47, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 103);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (48, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 29);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (49, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 30);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (50, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 47);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (51, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 48);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (52, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 84);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (53, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 90);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (54, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 8, 85);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (55, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 1);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (56, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 96);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (57, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 45);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (58, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 43);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (59, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 49);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (60, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 86);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (61, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 2);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (62, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 27);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (63, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 97);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (64, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 59);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (65, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 60);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (66, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 61);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (67, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 62);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (68, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 63);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (69, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 65);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (70, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 98);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (71, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 99);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (72, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 100);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (73, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 101);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (74, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 8);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (75, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 10);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (76, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 11);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (77, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 12);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (78, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 13);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (79, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 22);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (80, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 23);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (81, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 24);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (82, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 25);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (83, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 26);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (84, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 69);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (85, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 70);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (86, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 71);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (87, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 72);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (88, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 73);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (89, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 74);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (90, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 75);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (91, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 76);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (92, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 77);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (93, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 78);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (94, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 79);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (95, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 80);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (96, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 81);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (97, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 82);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (98, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 83);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (99, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 105);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (100, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 102);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (101, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 103);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (102, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 29);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (103, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 30);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (104, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 47);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (105, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 48);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (106, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 84);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (107, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 90);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (108, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 9, 85);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (161, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 11, 1);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (162, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 11, 96);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (163, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 11, 45);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (164, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 11, 43);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (165, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 11, 49);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (166, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 11, 86);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (167, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 11, 47);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (168, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 11, 48);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (169, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 11, 85);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (170, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 11, 84);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (290, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 1);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (291, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 96);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (292, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 45);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (293, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 43);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (294, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 49);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (295, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 86);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (296, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 2);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (297, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 27);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (298, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 97);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (299, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 59);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (300, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 60);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (301, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 61);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (302, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 62);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (303, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 63);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (304, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 65);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (305, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 98);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (306, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 99);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (307, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 100);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (308, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 101);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (309, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 8);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (310, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 10);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (311, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 11);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (312, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 12);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (313, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 13);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (314, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 22);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (315, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 23);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (316, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 24);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (317, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 25);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (318, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 26);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (319, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 69);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (320, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 70);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (321, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 71);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (322, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 72);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (323, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 73);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (324, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 74);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (325, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 75);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (326, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 76);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (327, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 77);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (328, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 78);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (329, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 79);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (330, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 80);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (331, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 81);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (332, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 82);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (333, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 83);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (334, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 105);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (335, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 102);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (336, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 103);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (337, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 29);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (338, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 30);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (339, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 47);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (340, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 48);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (341, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 84);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (342, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 90);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (343, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 12, 85);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (463, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 1);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (464, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 96);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (465, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 45);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (466, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 43);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (467, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 49);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (468, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 86);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (469, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 2);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (470, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 27);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (471, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 97);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (472, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 59);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (473, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 60);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (474, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 61);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (475, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 62);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (476, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 63);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (477, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 65);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (478, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 98);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (479, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 99);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (480, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 100);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (481, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 101);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (482, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 8);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (483, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 10);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (484, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 11);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (485, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 12);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (486, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 13);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (487, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 22);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (488, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 23);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (489, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 24);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (490, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 25);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (491, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 26);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (492, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 69);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (493, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 70);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (494, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 71);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (495, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 72);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (496, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 73);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (497, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 74);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (498, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 75);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (499, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 76);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (500, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 77);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (501, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 78);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (502, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 79);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (503, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 80);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (504, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 81);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (505, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 82);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (506, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 83);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (507, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 105);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (508, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 102);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (509, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 103);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (510, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 29);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (511, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 30);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (512, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 47);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (513, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 48);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (514, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 84);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (515, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 90);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (516, '2023-09-08 10:24:20.348', '2023-09-08 10:24:20.348', NULL, 13, 85);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (571, '2023-09-08 19:16:15.512', '2023-09-08 19:16:15.512', NULL, 10, 207);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (572, '2023-09-08 19:16:15.512', '2023-09-08 19:16:15.512', NULL, 10, 208);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (573, '2023-09-08 19:16:15.512', '2023-09-08 19:16:15.512', NULL, 10, 209);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (574, '2023-09-08 19:16:15.512', '2023-09-08 19:16:15.512', NULL, 10, 210);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (575, '2023-09-08 19:16:15.512', '2023-09-08 19:16:15.512', NULL, 10, 211);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (576, '2023-09-08 19:16:15.512', '2023-09-08 19:16:15.512', NULL, 10, 212);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (577, '2023-09-08 19:16:15.512', '2023-09-08 19:16:15.512', NULL, 10, 213);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (578, '2023-09-08 19:16:15.512', '2023-09-08 19:16:15.512', NULL, 10, 214);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (579, '2023-09-08 19:16:15.512', '2023-09-08 19:16:15.512', NULL, 10, 84);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (580, '2023-09-08 19:16:15.512', '2023-09-08 19:16:15.512', NULL, 10, 90);
INSERT INTO `base_sys_role_menu` (`id`, `createTime`, `updateTime`, `deleted_at`, `roleId`, `menuId`) VALUES (581, '2023-09-08 19:16:15.512', '2023-09-08 19:16:15.512', NULL, 10, 85);
COMMIT;

-- ----------------------------
-- Table structure for base_sys_user
-- ----------------------------
DROP TABLE IF EXISTS `base_sys_user`;
CREATE TABLE `base_sys_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `departmentId` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `passwordV` int(11) NOT NULL DEFAULT '1',
  `nickName` varchar(255) DEFAULT NULL,
  `headImg` varchar(255) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `remark` varchar(255) DEFAULT NULL,
  `socketId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_base_sys_user_deleted_at` (`deleted_at`),
  KEY `idx_base_sys_user_department_id` (`departmentId`),
  KEY `idx_base_sys_user_username` (`username`),
  KEY `idx_base_sys_user_phone` (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of base_sys_user
-- ----------------------------
BEGIN;
INSERT INTO `base_sys_user` (`id`, `createTime`, `updateTime`, `deleted_at`, `departmentId`, `name`, `username`, `password`, `passwordV`, `nickName`, `headImg`, `phone`, `email`, `status`, `remark`, `socketId`) VALUES (1, '2023-09-08 10:24:20.282', '2023-09-08 19:17:44.449', NULL, 1, '超级管理员', 'admin', 'eebd18c49e04906c39a78d70e0be0abd', 4, '管理员', 'https://cool-admin-pro.oss-cn-shanghai.aliyuncs.com/app/c8128c24-d0e9-4e07-9c0d-6f65446e105b.png', '18000000000', 'team@cool-js.com', 1, '拥有最高权限的用户', NULL);
INSERT INTO `base_sys_user` (`id`, `createTime`, `updateTime`, `deleted_at`, `departmentId`, `name`, `username`, `password`, `passwordV`, `nickName`, `headImg`, `phone`, `email`, `status`, `remark`, `socketId`) VALUES (29, '2023-09-08 19:16:46.000', '2023-09-08 20:53:43.575', NULL, 1, 'admin', 'adminking', '73b69218288a489369dac1b430672a53', 3, 'admin', '/dzh/public/uploads/20230908/cvdhtgzw6pg8eqkzdo.jpg', NULL, NULL, 1, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for base_sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `base_sys_user_role`;
CREATE TABLE `base_sys_user_role` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `userId` bigint(20) NOT NULL,
  `roleId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_base_sys_user_role_deleted_at` (`deleted_at`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of base_sys_user_role
-- ----------------------------
BEGIN;
INSERT INTO `base_sys_user_role` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `roleId`) VALUES (1, '2023-09-08 10:24:20.310', '2023-09-08 10:24:20.310', NULL, 1, 1);
INSERT INTO `base_sys_user_role` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `roleId`) VALUES (2, '2023-09-08 10:24:20.310', '2023-09-08 10:24:20.310', NULL, 2, 1);
INSERT INTO `base_sys_user_role` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `roleId`) VALUES (3, '2023-09-08 10:24:20.310', '2023-09-08 10:24:20.310', NULL, 3, 1);
INSERT INTO `base_sys_user_role` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `roleId`) VALUES (4, '2023-09-08 10:24:20.310', '2023-09-08 10:24:20.310', NULL, 4, 1);
INSERT INTO `base_sys_user_role` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `roleId`) VALUES (5, '2023-09-08 10:24:20.310', '2023-09-08 10:24:20.310', NULL, 5, 1);
INSERT INTO `base_sys_user_role` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `roleId`) VALUES (6, '2023-09-08 10:24:20.310', '2023-09-08 10:24:20.310', NULL, 6, 1);
INSERT INTO `base_sys_user_role` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `roleId`) VALUES (7, '2023-09-08 10:24:20.310', '2023-09-08 10:24:20.310', NULL, 7, 1);
INSERT INTO `base_sys_user_role` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `roleId`) VALUES (8, '2023-09-08 10:24:20.310', '2023-09-08 10:24:20.310', NULL, 8, 1);
INSERT INTO `base_sys_user_role` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `roleId`) VALUES (9, '2023-09-08 10:24:20.310', '2023-09-08 10:24:20.310', NULL, 9, 1);
INSERT INTO `base_sys_user_role` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `roleId`) VALUES (10, '2023-09-08 10:24:20.310', '2023-09-08 10:24:20.310', NULL, 10, 1);
INSERT INTO `base_sys_user_role` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `roleId`) VALUES (11, '2023-09-08 10:24:20.310', '2023-09-08 10:24:20.310', NULL, 11, 1);
INSERT INTO `base_sys_user_role` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `roleId`) VALUES (12, '2023-09-08 10:24:20.310', '2023-09-08 10:24:20.310', NULL, 12, 1);
INSERT INTO `base_sys_user_role` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `roleId`) VALUES (13, '2023-09-08 10:24:20.310', '2023-09-08 10:24:20.310', NULL, 13, 1);
INSERT INTO `base_sys_user_role` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `roleId`) VALUES (14, '2023-09-08 10:24:20.310', '2023-09-08 10:24:20.310', NULL, 14, 1);
INSERT INTO `base_sys_user_role` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `roleId`) VALUES (16, '2023-09-08 10:24:20.310', '2023-09-08 10:24:20.310', NULL, 16, 1);
INSERT INTO `base_sys_user_role` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `roleId`) VALUES (17, '2023-09-08 10:24:20.310', '2023-09-08 10:24:20.310', NULL, 15, 1);
INSERT INTO `base_sys_user_role` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `roleId`) VALUES (19, '2023-09-08 10:24:20.310', '2023-09-08 10:24:20.310', NULL, 18, 1);
INSERT INTO `base_sys_user_role` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `roleId`) VALUES (21, '2023-09-08 10:24:20.310', '2023-09-08 10:24:20.310', NULL, 17, 1);
INSERT INTO `base_sys_user_role` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `roleId`) VALUES (22, '2023-09-08 10:24:20.310', '2023-09-08 10:24:20.310', NULL, 20, 1);
INSERT INTO `base_sys_user_role` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `roleId`) VALUES (24, '2023-09-08 10:24:20.310', '2023-09-08 10:24:20.310', NULL, 22, 1);
INSERT INTO `base_sys_user_role` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `roleId`) VALUES (27, '2023-09-08 10:24:20.310', '2023-09-08 10:24:20.310', NULL, 19, 1);
INSERT INTO `base_sys_user_role` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `roleId`) VALUES (28, '2023-09-08 10:24:20.310', '2023-09-08 10:24:20.310', NULL, 21, 8);
INSERT INTO `base_sys_user_role` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `roleId`) VALUES (29, '2023-09-08 10:24:20.310', '2023-09-08 10:24:20.310', NULL, 23, 8);
INSERT INTO `base_sys_user_role` (`id`, `createTime`, `updateTime`, `deleted_at`, `userId`, `roleId`) VALUES (45, '2023-09-08 19:17:53.993', '2023-09-08 19:17:53.993', NULL, 29, 10);
COMMIT;

-- ----------------------------
-- Table structure for dict_info
-- ----------------------------
DROP TABLE IF EXISTS `dict_info`;
CREATE TABLE `dict_info` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `typeId` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `orderNum` int(11) NOT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_dict_info_deleted_at` (`deleted_at`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dict_info
-- ----------------------------
BEGIN;
INSERT INTO `dict_info` (`id`, `createTime`, `updateTime`, `deleted_at`, `typeId`, `name`, `value`, `orderNum`, `remark`, `parentId`) VALUES (1, '2023-09-08 10:24:20.428', '2023-09-08 10:24:20.428', NULL, 1, '衣服', NULL, 2, NULL, NULL);
INSERT INTO `dict_info` (`id`, `createTime`, `updateTime`, `deleted_at`, `typeId`, `name`, `value`, `orderNum`, `remark`, `parentId`) VALUES (2, '2023-09-08 10:24:20.428', '2023-09-08 10:24:20.428', NULL, 1, '裤子', NULL, 1, NULL, NULL);
INSERT INTO `dict_info` (`id`, `createTime`, `updateTime`, `deleted_at`, `typeId`, `name`, `value`, `orderNum`, `remark`, `parentId`) VALUES (3, '2023-09-08 10:24:20.428', '2023-09-08 10:24:20.428', NULL, 1, '鞋子', NULL, 3, NULL, NULL);
INSERT INTO `dict_info` (`id`, `createTime`, `updateTime`, `deleted_at`, `typeId`, `name`, `value`, `orderNum`, `remark`, `parentId`) VALUES (4, '2023-09-08 10:24:20.428', '2023-09-08 10:24:20.428', NULL, 2, '闪酷', NULL, 2, NULL, NULL);
INSERT INTO `dict_info` (`id`, `createTime`, `updateTime`, `deleted_at`, `typeId`, `name`, `value`, `orderNum`, `remark`, `parentId`) VALUES (5, '2023-09-08 10:24:20.428', '2023-09-08 10:24:20.428', NULL, 2, 'COOL', NULL, 1, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for dict_type
-- ----------------------------
DROP TABLE IF EXISTS `dict_type`;
CREATE TABLE `dict_type` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_dict_type_deleted_at` (`deleted_at`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dict_type
-- ----------------------------
BEGIN;
INSERT INTO `dict_type` (`id`, `createTime`, `updateTime`, `deleted_at`, `name`, `key`) VALUES (1, '2023-09-08 10:24:20.429', '2023-09-08 10:24:20.429', NULL, '类别', 'type');
INSERT INTO `dict_type` (`id`, `createTime`, `updateTime`, `deleted_at`, `name`, `key`) VALUES (2, '2023-09-08 10:24:20.429', '2023-09-08 10:24:20.429', NULL, '品牌', 'brand');
COMMIT;

-- ----------------------------
-- Table structure for dzh_cms_form
-- ----------------------------
DROP TABLE IF EXISTS `dzh_cms_form`;
CREATE TABLE `dzh_cms_form` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `name` varchar(10) NOT NULL COMMENT '姓名',
  `phone` varchar(11) NOT NULL COMMENT '号码',
  `remark` longtext NOT NULL COMMENT '题目',
  PRIMARY KEY (`id`),
  KEY `idx_dzh_cms_form_deleted_at` (`deleted_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dzh_cms_form
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for dzh_cms_lottery
-- ----------------------------
DROP TABLE IF EXISTS `dzh_cms_lottery`;
CREATE TABLE `dzh_cms_lottery` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `lotteryTime` longtext NOT NULL COMMENT '开奖时间',
  `currentNumber` longtext NOT NULL COMMENT '当前期号',
  `lotteryNumber` longtext NOT NULL COMMENT '开奖号码',
  `numTrend` longtext NOT NULL COMMENT '号码走势',
  `count` longtext NOT NULL COMMENT '当天期号',
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_dzh_cms_lottery_deleted_at` (`deleted_at`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dzh_cms_lottery
-- ----------------------------
BEGIN;
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (1, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 09:30:00', '2023090801', '1,7,6,11,2', '1,7,6,11,2', '01', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (2, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 09:50:00', '2023090802', '3,7,10,5,2', '3,7,10,5,2', '02', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (3, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 10:10:00', '2023090803', '5,4,8,10,7', '5,4,8,10,7', '03', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (4, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 10:30:00', '2023090804', '3,4,8,1,5', '3,4,8,1,5', '04', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (5, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 10:50:00', '2023090805', '2,5,3,1,11', '2,5,3,1,11', '05', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (6, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 11:10:00', '2023090806', '1,8,4,10,7', '1,8,4,10,7', '06', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (7, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 11:30:00', '2023090807', '7,8,1,5,6', '7,8,1,5,6', '07', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (8, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 11:50:00', '2023090808', '6,7,4,8,3', '6,7,4,8,3', '08', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (9, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 12:10:00', '2023090809', '2,4,6,9,3', '2,4,6,9,3', '09', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (10, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 12:30:00', '2023090810', '2,11,10,7,6', '2,11,10,7,6', '10', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (11, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 12:50:00', '2023090811', '2,1,7,11,6', '2,1,7,11,6', '11', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (12, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 13:10:00', '2023090812', '6,2,3,5,1', '6,2,3,5,1', '12', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (13, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 13:30:00', '2023090813', '9,10,4,1,2', '9,10,4,1,2', '13', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (14, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 13:50:00', '2023090814', '9,4,8,6,2', '9,4,8,6,2', '14', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (15, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 14:10:00', '2023090815', '10,7,3,4,5', '10,7,3,4,5', '15', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (16, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 14:30:00', '2023090816', '10,11,7,3,9', '10,11,7,3,9', '16', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (17, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 14:50:00', '2023090817', '2,6,1,9,8', '2,6,1,9,8', '17', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (18, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 15:10:00', '2023090818', '5,3,7,10,1', '5,3,7,10,1', '18', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (19, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 15:30:00', '2023090819', '1,5,11,2,7', '1,5,11,2,7', '19', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (20, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 15:50:00', '2023090820', '5,1,4,11,9', '5,1,4,11,9', '20', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (21, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 16:10:00', '2023090821', '10,2,4,9,8', '10,2,4,9,8', '21', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (22, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 16:30:00', '2023090822', '8,2,5,11,6', '8,2,5,11,6', '22', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (23, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 16:50:00', '2023090823', '8,2,1,4,7', '8,2,1,4,7', '23', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (24, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 17:10:00', '2023090824', '11,1,3,10,5', '11,1,3,10,5', '24', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (25, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 17:30:00', '2023090825', '5,8,3,6,9', '5,8,3,6,9', '25', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (26, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 17:50:00', '2023090826', '3,6,11,9,7', '3,6,11,9,7', '26', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (27, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 18:10:00', '2023090827', '4,1,6,8,3', '4,1,6,8,3', '27', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (28, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 18:30:00', '2023090828', '3,9,6,1,5', '3,9,6,1,5', '28', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (29, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 18:50:00', '2023090829', '9,3,5,1,8', '9,3,5,1,8', '29', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (30, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 19:10:00', '2023090830', '1,9,6,3,2', '1,9,6,3,2', '30', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (31, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 19:30:00', '2023090831', '3,11,1,6,7', '3,11,1,6,7', '31', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (32, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 19:50:00', '2023090832', '5,9,3,4,8', '5,9,3,4,8', '32', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (33, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 20:10:00', '2023090833', '3,6,7,4,5', '3,6,7,4,5', '33', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (34, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 20:30:00', '2023090834', '7,4,3,10,8', '7,4,3,10,8', '34', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (35, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 20:50:00', '2023090835', '7,10,2,8,1', '7,10,2,8,1', '35', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (36, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 21:10:00', '2023090836', '6,10,4,11,5', '6,10,4,11,5', '36', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (37, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 21:30:00', '2023090837', '8,11,1,10,5', '8,11,1,10,5', '37', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (38, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 21:50:00', '2023090838', '3,5,6,10,9', '3,5,6,10,9', '38', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (39, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 22:10:00', '2023090839', '7,10,5,11,3', '7,10,5,11,3', '39', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (40, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 22:30:00', '2023090840', '9,1,5,3,11', '9,1,5,3,11', '40', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (41, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 22:50:00', '2023090841', '9,10,1,5,3', '9,10,1,5,3', '41', 1);
INSERT INTO `dzh_cms_lottery` (`id`, `createTime`, `updateTime`, `deleted_at`, `lotteryTime`, `currentNumber`, `lotteryNumber`, `numTrend`, `count`, `status`) VALUES (42, '2023-09-08 18:15:12.172', '2023-09-08 18:15:12.172', NULL, '2023-09-08 23:10:00', '2023090842', '8,7,6,9,11', '8,7,6,9,11', '42', 1);
COMMIT;

-- ----------------------------
-- Table structure for dzh_common_setting
-- ----------------------------
DROP TABLE IF EXISTS `dzh_common_setting`;
CREATE TABLE `dzh_common_setting` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `siteName` longtext COMMENT '网站名称',
  `domainName` varchar(50) DEFAULT NULL COMMENT '网站域名',
  `logo` varchar(50) DEFAULT NULL COMMENT 'logo',
  `company` varchar(50) DEFAULT NULL COMMENT '公司名称',
  `contact` varchar(50) DEFAULT NULL COMMENT '联系人',
  `contactWay` varchar(50) DEFAULT NULL COMMENT '座机',
  `mobile` varchar(50) DEFAULT NULL COMMENT '手机',
  `Address` varchar(50) DEFAULT NULL COMMENT '地址',
  `keyword` varchar(50) DEFAULT NULL COMMENT '关键词',
  `description` varchar(50) DEFAULT NULL COMMENT '描述',
  `smtp` varchar(50) DEFAULT NULL COMMENT 'smtp',
  `sendEmail` varchar(50) DEFAULT NULL COMMENT '发送邮箱',
  `pass` varchar(50) DEFAULT NULL COMMENT '邮箱授权码',
  `requestEmail` varchar(50) DEFAULT NULL COMMENT '接收邮箱',
  `remindEmail` int(11) DEFAULT '0' COMMENT '到期邮件开启 0关闭 1开启',
  `remindSms` int(11) DEFAULT '0' COMMENT '到期短信开启 0关闭 1开启',
  `accessKeyId` varchar(100) DEFAULT NULL COMMENT 'accessKeyId',
  `accessKeySecret` varchar(100) DEFAULT NULL COMMENT 'accessKeySecret',
  `signName` varchar(50) DEFAULT NULL COMMENT '签名',
  `templateCode` varchar(50) DEFAULT NULL COMMENT '模板',
  `endpoint` varchar(50) DEFAULT NULL COMMENT 'endpoint',
  `remindMobile` varchar(50) DEFAULT NULL COMMENT '通知手机号码',
  `remindDay` varchar(50) DEFAULT NULL COMMENT '到期提醒提前天数',
  `fieldJson` longtext COMMENT '自定义字段',
  PRIMARY KEY (`id`),
  KEY `idx_dzh_common_setting_deleted_at` (`deleted_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dzh_common_setting
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for dzh_member_user
-- ----------------------------
DROP TABLE IF EXISTS `dzh_member_user`;
CREATE TABLE `dzh_member_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `avatarUrl` varchar(200) DEFAULT NULL COMMENT '头像',
  `userName` varchar(50) NOT NULL COMMENT '会员账号',
  `password` varchar(50) NOT NULL COMMENT '会员密码',
  `passwordV` int(11) NOT NULL DEFAULT '1',
  `nickName` varchar(50) DEFAULT NULL COMMENT '会员昵称',
  `levelName` varchar(50) DEFAULT NULL COMMENT '等级',
  `sex` int(11) DEFAULT '1' COMMENT '性别',
  `qq` varchar(255) DEFAULT NULL COMMENT 'qq',
  `mobile` varchar(50) DEFAULT NULL COMMENT '手机号',
  `wx` varchar(50) DEFAULT NULL COMMENT '微信号',
  `wxImg` varchar(255) DEFAULT NULL COMMENT '微信二维码',
  `email` varchar(50) DEFAULT NULL COMMENT 'email',
  `role` longtext COMMENT '家庭角色',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `openid` longtext COMMENT 'openid',
  `unionid` longtext COMMENT 'unionid',
  `session_key` longtext COMMENT 'session_key',
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_dzh_member_user_email` (`email`),
  KEY `idx_dzh_member_user_deleted_at` (`deleted_at`),
  KEY `idx_dzh_member_user_username` (`userName`),
  KEY `idx_dzh_member_user_nick_name` (`nickName`),
  KEY `idx_dzh_member_user_qq` (`qq`),
  KEY `idx_dzh_member_user_mobile` (`mobile`),
  KEY `idx_dzh_member_user_wx` (`wx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dzh_member_user
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for space_info
-- ----------------------------
DROP TABLE IF EXISTS `space_info`;
CREATE TABLE `space_info` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `url` varchar(255) NOT NULL COMMENT '地址',
  `type` varchar(255) NOT NULL COMMENT '类型',
  `classifyId` bigint(20) DEFAULT NULL COMMENT '分类ID',
  PRIMARY KEY (`id`),
  KEY `idx_space_info_deleted_at` (`deleted_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of space_info
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for space_type
-- ----------------------------
DROP TABLE IF EXISTS `space_type`;
CREATE TABLE `space_type` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `name` varchar(255) NOT NULL COMMENT '类别名称 ',
  `parentId` int(11) DEFAULT NULL COMMENT '父分类ID',
  PRIMARY KEY (`id`),
  KEY `idx_space_type_deleted_at` (`deleted_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of space_type
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for task_info
-- ----------------------------
DROP TABLE IF EXISTS `task_info`;
CREATE TABLE `task_info` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `jobId` varchar(255) DEFAULT NULL COMMENT '任务ID',
  `repeatConf` longtext COMMENT '重复配置',
  `name` varchar(255) DEFAULT NULL COMMENT '任务名称',
  `cron` varchar(255) DEFAULT NULL COMMENT 'cron表达式',
  `limit` bigint(20) DEFAULT NULL COMMENT '限制次数 不传为不限制',
  `every` bigint(20) DEFAULT NULL COMMENT '间隔时间 单位秒',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `status` bigint(20) DEFAULT NULL COMMENT '状态 0:关闭 1:开启',
  `startDate` datetime(3) DEFAULT NULL COMMENT '开始时间',
  `endDate` datetime(3) DEFAULT NULL COMMENT '结束时间',
  `data` varchar(255) DEFAULT NULL COMMENT '数据',
  `service` varchar(255) DEFAULT NULL COMMENT '执行的服务',
  `type` bigint(20) DEFAULT NULL COMMENT '类型 0:系统 1:用户',
  `nextRunTime` datetime(3) DEFAULT NULL COMMENT '下次执行时间',
  `taskType` bigint(20) DEFAULT NULL COMMENT '任务类型 0:cron 1:时间间隔',
  PRIMARY KEY (`id`),
  KEY `idx_task_info_deleted_at` (`deleted_at`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of task_info
-- ----------------------------
BEGIN;
INSERT INTO `task_info` (`id`, `createTime`, `updateTime`, `deleted_at`, `jobId`, `repeatConf`, `name`, `cron`, `limit`, `every`, `remark`, `status`, `startDate`, `endDate`, `data`, `service`, `type`, `nextRunTime`, `taskType`) VALUES (1, '2023-09-08 10:24:20.486', '2023-09-08 23:57:53.034', NULL, NULL, NULL, '清理日志', '1 2 3 * * *', NULL, NULL, '每天03:02:01执行清理缓存任务', 1, NULL, NULL, NULL, 'BaseFuncClearLog(false)', 0, '2023-09-09 03:02:01.000', 0);
INSERT INTO `task_info` (`id`, `createTime`, `updateTime`, `deleted_at`, `jobId`, `repeatConf`, `name`, `cron`, `limit`, `every`, `remark`, `status`, `startDate`, `endDate`, `data`, `service`, `type`, `nextRunTime`, `taskType`) VALUES (4, '2023-09-08 15:49:37.794', '2023-09-08 23:57:53.037', NULL, NULL, NULL, '生成当天42期开奖号码', '* * 6 * * *', NULL, NULL, NULL, 1, '2023-09-08 15:49:32.000', NULL, NULL, 'CmsCrontab()', 1, '2023-09-09 06:00:00.000', 0);
COMMIT;

-- ----------------------------
-- Table structure for task_log
-- ----------------------------
DROP TABLE IF EXISTS `task_log`;
CREATE TABLE `task_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createTime` datetime(3) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(3) NOT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) DEFAULT NULL,
  `taskId` bigint(20) unsigned DEFAULT NULL COMMENT '任务ID',
  `status` tinyint(3) unsigned NOT NULL COMMENT '状态 0:失败 1:成功',
  `detail` longtext COMMENT '详情',
  PRIMARY KEY (`id`),
  KEY `idx_task_log_deleted_at` (`deleted_at`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of task_log
-- ----------------------------
BEGIN;
INSERT INTO `task_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `taskId`, `status`, `detail`) VALUES (18, '2023-09-08 11:18:18.180', '2023-09-08 11:18:18.180', NULL, 3, 1, '任务执行成功');
INSERT INTO `task_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `taskId`, `status`, `detail`) VALUES (19, '2023-09-08 11:18:19.180', '2023-09-08 11:18:19.180', NULL, 3, 1, '任务执行成功');
INSERT INTO `task_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `taskId`, `status`, `detail`) VALUES (20, '2023-09-08 11:18:20.179', '2023-09-08 11:18:20.179', NULL, 3, 1, '任务执行成功');
INSERT INTO `task_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `taskId`, `status`, `detail`) VALUES (21, '2023-09-08 11:18:21.180', '2023-09-08 11:18:21.180', NULL, 3, 1, '任务执行成功');
INSERT INTO `task_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `taskId`, `status`, `detail`) VALUES (22, '2023-09-08 11:18:22.184', '2023-09-08 11:18:22.184', NULL, 3, 1, '任务执行成功');
INSERT INTO `task_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `taskId`, `status`, `detail`) VALUES (23, '2023-09-08 11:18:23.180', '2023-09-08 11:18:23.180', NULL, 3, 1, '任务执行成功');
INSERT INTO `task_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `taskId`, `status`, `detail`) VALUES (24, '2023-09-08 11:18:24.180', '2023-09-08 11:18:24.180', NULL, 3, 1, '任务执行成功');
INSERT INTO `task_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `taskId`, `status`, `detail`) VALUES (25, '2023-09-08 11:18:25.180', '2023-09-08 11:18:25.180', NULL, 3, 1, '任务执行成功');
INSERT INTO `task_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `taskId`, `status`, `detail`) VALUES (26, '2023-09-08 11:18:26.180', '2023-09-08 11:18:26.180', NULL, 3, 1, '任务执行成功');
INSERT INTO `task_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `taskId`, `status`, `detail`) VALUES (27, '2023-09-08 11:18:27.179', '2023-09-08 11:18:27.179', NULL, 3, 1, '任务执行成功');
INSERT INTO `task_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `taskId`, `status`, `detail`) VALUES (28, '2023-09-08 11:18:28.180', '2023-09-08 11:18:28.180', NULL, 3, 1, '任务执行成功');
INSERT INTO `task_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `taskId`, `status`, `detail`) VALUES (29, '2023-09-08 11:18:29.180', '2023-09-08 11:18:29.180', NULL, 3, 1, '任务执行成功');
INSERT INTO `task_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `taskId`, `status`, `detail`) VALUES (30, '2023-09-08 11:18:30.180', '2023-09-08 11:18:30.180', NULL, 3, 1, '任务执行成功');
INSERT INTO `task_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `taskId`, `status`, `detail`) VALUES (31, '2023-09-08 11:18:31.180', '2023-09-08 11:18:31.180', NULL, 3, 1, '任务执行成功');
INSERT INTO `task_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `taskId`, `status`, `detail`) VALUES (32, '2023-09-08 11:18:32.180', '2023-09-08 11:18:32.180', NULL, 3, 1, '任务执行成功');
INSERT INTO `task_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `taskId`, `status`, `detail`) VALUES (33, '2023-09-08 11:18:33.180', '2023-09-08 11:18:33.180', NULL, 3, 1, '任务执行成功');
INSERT INTO `task_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `taskId`, `status`, `detail`) VALUES (34, '2023-09-08 11:18:34.180', '2023-09-08 11:18:34.180', NULL, 3, 1, '任务执行成功');
INSERT INTO `task_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `taskId`, `status`, `detail`) VALUES (35, '2023-09-08 11:18:35.180', '2023-09-08 11:18:35.180', NULL, 3, 1, '任务执行成功');
INSERT INTO `task_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `taskId`, `status`, `detail`) VALUES (36, '2023-09-08 11:18:36.179', '2023-09-08 11:18:36.179', NULL, 3, 1, '任务执行成功');
INSERT INTO `task_log` (`id`, `createTime`, `updateTime`, `deleted_at`, `taskId`, `status`, `detail`) VALUES (37, '2023-09-08 11:18:37.181', '2023-09-08 11:18:37.181', NULL, 3, 1, '任务执行成功');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
