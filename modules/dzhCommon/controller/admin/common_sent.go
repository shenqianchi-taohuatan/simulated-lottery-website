package admin

import (
	"context"

	"github.com/cool-team-official/cool-admin-go/cool"

	"github.com/gogf/gf/v2/encoding/gjson"
	"github.com/gogf/gf/v2/frame/g"
)

type CommonSentController struct {
	*cool.ControllerSimple
}

func init() {
	var common_sms_controller = &CommonSentController{
		&cool.ControllerSimple{
			Perfix: "/admin/demo/common_sms",
		},
	}
	// 注册路由
	cool.RegisterControllerSimple(common_sms_controller)
}

// 增加 Welcome 演示 方法
type CommonSentWelcomeReq struct {
	g.Meta `path:"/welcome" method:"GET"`
}
type CommonSentWelcomeRes struct {
	*cool.BaseRes
	Data interface{} `json:"data"`
}

func (c *CommonSentController) Welcome(ctx context.Context, req *CommonSentWelcomeReq) (res *CommonSentWelcomeRes, err error) {
	res = &CommonSentWelcomeRes{
		BaseRes: cool.Ok("Welcome to Cool Admin Go"),
		Data:    gjson.New(`{"name": "Cool Admin Go", "age":0}`),
	}
	return
}
