package admin

import (
	"context"
	"dzhcms/modules/dzhCommon/service"

	"github.com/cool-team-official/cool-admin-go/cool"

	"github.com/gogf/gf/v2/encoding/gjson"
	"github.com/gogf/gf/v2/frame/g"
)

type CommonSettingController struct {
	*cool.Controller
}

func init() {
	var common_setting_controller = &CommonSettingController{
		&cool.Controller{
			Perfix:  "/admin/common/setting",
			Api:     []string{"Add", "Delete", "Update", "Info", "List", "Page"},
			Service: service.NewCommonSettingService(),
		},
	}
	// 注册路由
	cool.RegisterController(common_setting_controller)
}

// 增加 Welcome 演示 方法
type CommonSettingWelcomeReq struct {
	g.Meta `path:"/welcome" method:"GET"`
}
type CommonSettingWelcomeRes struct {
	*cool.BaseRes
	Data interface{} `json:"data"`
}

func (c *CommonSettingController) Welcome(ctx context.Context, req *CommonSettingWelcomeReq) (res *CommonSettingWelcomeRes, err error) {
	res = &CommonSettingWelcomeRes{
		BaseRes: cool.Ok("Welcome to Cool Admin Go"),
		Data:    gjson.New(`{"name": "Cool Admin Go", "age":0}`),
	}
	return
}
