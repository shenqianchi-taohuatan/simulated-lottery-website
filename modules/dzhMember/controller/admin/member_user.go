package admin

import (
	"context"
	"dzhcms/modules/dzhMember/service"

	"github.com/cool-team-official/cool-admin-go/cool"

	"github.com/gogf/gf/v2/encoding/gjson"
	"github.com/gogf/gf/v2/frame/g"
)

type MemberUserController struct {
	*cool.Controller
}

func init() {
	var member_user_controller = &MemberUserController{
		&cool.Controller{
			Perfix:  "/admin/member/user",
			Api:     []string{"Add", "Delete", "Update", "Info", "List", "Page"},
			Service: service.NewMemberUserService(),
		},
	}
	// 注册路由
	cool.RegisterController(member_user_controller)
}

// 增加 Welcome 演示 方法
type MemberUserWelcomeReq struct {
	g.Meta `path:"/welcome" method:"GET"`
}
type MemberUserWelcomeRes struct {
	*cool.BaseRes
	Data interface{} `json:"data"`
}

func (c *MemberUserController) Welcome(ctx context.Context, req *MemberUserWelcomeReq) (res *MemberUserWelcomeRes, err error) {
	res = &MemberUserWelcomeRes{
		BaseRes: cool.Ok("Welcome to Cool Admin Go"),
		Data:    gjson.New(`{"name": "Cool Admin Go", "age":0}`),
	}
	return
}
