package app

import (
	"dzhcms/modules/dzhMember/service"

	"github.com/cool-team-official/cool-admin-go/cool"
)

type MemberUserController struct {
	*cool.Controller
}

func init() {
	var member_user_controller = &MemberUserController{
		&cool.Controller{
			Perfix:  "/app/member/user",
			Api:     []string{"Add", "Delete", "Update", "Info", "List", "Page"},
			Service: service.NewMemberUserService(),
		},
	}
	// 注册路由
	cool.RegisterController(member_user_controller)
}
