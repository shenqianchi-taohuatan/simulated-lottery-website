package service

import (
	"context"
	"dzhcms/modules/dzhCms/model"
	"dzhcms/utility/logger"
	"dzhcms/utility/util"
	"time"

	"github.com/cool-team-official/cool-admin-go/cool"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
	"github.com/gogf/gf/v2/util/gconv"
)

type CmsLotteryService struct {
	*cool.Service
}

// 生成开奖号码
func (s *CmsLotteryService) GenerateNumber(ctx g.Ctx) (data interface{}, err error) {

	var (
		lotteryArr   g.Slice
		lotteryMap   g.Map
		count        int = 42
		dayStartTime     = gtime.Now().Format("Y-m-d")
		pools        g.SliceInt
		year         int
		month        int
		day          int
		monthStr     string
		dayStr       string
		iStr         string
	)

	year = gtime.Now().Year()
	month = gtime.Now().Month()
	day = gtime.Now().Day()

	// 第一个开奖时间
	lotteryFirstTime := gtime.New(dayStartTime).Add(time.Duration(9) * time.Hour).Add(time.Duration(30) * time.Minute)

	// 所有开奖时间

	for i := 1; i <= count; i++ {
		// 5个随机不重复数字
		pools = util.RandCountArr(11, 5)

		t := lotteryFirstTime.Add(time.Duration(20*(i-1)) * time.Minute)

		monthStr = gconv.String(month)
		dayStr = gconv.String(day)
		iStr = gconv.String(i)
		if month < 10 {
			monthStr = "0" + gconv.String(month)
		}
		if day < 10 {
			dayStr = "0" + gconv.String(day)
		}

		if i < 10 {
			iStr = "0" + gconv.String(i)
		}

		poolsStr := util.ArrToStr(pools, ",")

		lotteryMap = g.Map{
			"lotteryNumber": poolsStr,
			"lotteryTime":   t,
			"currentNumber": gconv.String(year) + monthStr + gconv.String(dayStr) + gconv.String(iStr),
			"numTrend":      poolsStr,
			"count":         iStr,
		}

		lotteryArr = append(lotteryArr, lotteryMap)
	}

	_, err = cool.DBM(s.Model).Data(lotteryArr).Insert()
	if err != nil {
		logger.Error(ctx, "GenerateNumber insert error: %v", err.Error())
		return
	}

	data = lotteryArr
	return
}

// 获取当前开奖数据
func (s *CmsLotteryService) GetNowLottery(ctx g.Ctx) (data interface{}, err error) {
	// {
	// "upNumber":"上期期号",
	// "surplusTime":"剩余时间",
	// "lotteryNumber":"开奖号码",
	// "lotteryTime":"开奖时间",
	// "nextLotteryTime":"下期开奖时间"}

	startTime := gtime.Now().Format("Y-m-d")
	endTime := gtime.Now()

	between := g.Slice{startTime, endTime}
	condition := g.Map{"lotteryTime BETWEEN ? AND ?": between}

	lotteryData, _ := cool.DBM(s.Model).Where(condition).Order("count desc").One()

	bt := gtime.Now().Timestamp() - gtime.New(gconv.String(lotteryData["lotteryTime"])).Timestamp()

	lMap := g.Map{

		"upNumber":        gconv.Int(lotteryData["currentNumber"]),
		"lotteryNumber":   lotteryData["lotteryNumber"],
		"lotteryTime":     lotteryData["lotteryTime"],
		"surplusTime":     20*60 - gconv.Int(bt),
		"nextLotteryTime": gtime.New(gconv.String(lotteryData["lotteryTime"])).Add(time.Duration(20) * time.Minute),
	}

	if lotteryData.IsEmpty() {
		data = "没有数据"
	}
	data = lMap

	return
}

func NewCmsLotteryService() *CmsLotteryService {
	return &CmsLotteryService{
		&cool.Service{
			Model: model.NewCmsLottery(),
			ListQueryOp: &cool.QueryOp{
				FieldEQ:      []string{},
				KeyWordField: []string{},
				AddOrderby:   map[string]string{"`lotteryTime`": "DESC"},
				Where: func(ctx context.Context) [][]interface{} {
					condition := []g.Array{{"status", 1}}

					startTime := gtime.Now().Format("Y-m-d")
					endTime := gtime.Now()

					between := g.Slice{startTime, endTime}
					condition = append(condition, g.Array{"lotteryTime BETWEEN ? AND ?", between})

					return condition
				},
			},
			PageQueryOp: &cool.QueryOp{
				FieldEQ:      []string{},
				KeyWordField: []string{},
				AddOrderby:   map[string]string{"`lotteryTime`": "DESC"},
				// Where: func(ctx context.Context) [][]interface{} {
				// 	condition := []g.Array{{"status", 1}}

				// 	startTime := gtime.Now().Format("Y-m-d")
				// 	endTime := gtime.Now()

				// 	between := g.Slice{startTime, endTime}
				// 	condition = append(condition, g.Array{"lotteryTime BETWEEN ? AND ?", between})

				// 	return condition
				// },
			},
		},
	}
}
