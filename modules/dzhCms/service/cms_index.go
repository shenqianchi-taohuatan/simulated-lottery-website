package service

import (
	"context"
	v1 "dzhcms/api/v1"

	"github.com/cool-team-official/cool-admin-go/cool"
)

type CmsIndexService struct {
	*cool.Service
}

// 发送邮件
func (s *CmsIndexService) Index(ctx context.Context, req *v1.IndexReq) (result *interface{}, err error) {

	return
}

func NewCmsIndexService() *CmsIndexService {
	return &CmsIndexService{
		&cool.Service{},
	}
}
