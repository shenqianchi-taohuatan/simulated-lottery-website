package app

import (
	"context"
	v1 "dzhcms/api/v1"

	"github.com/cool-team-official/cool-admin-go/cool"

	"github.com/gogf/gf/v2/encoding/gjson"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gview"
)

type CmsIndexController struct {
	*cool.ControllerSimple
}

func init() {
	var cms_index_controller = &CmsIndexController{
		&cool.ControllerSimple{
			Perfix: "/app/cms/index",
		},
	}
	// 注册路由
	cool.RegisterControllerSimple(cms_index_controller)
}

// 增加 Welcome 演示 方法
type CmsIndexWelcomeReq struct {
	g.Meta `path:"/welcome" method:"GET"`
}
type CmsIndexWelcomeRes struct {
	*cool.BaseRes
	Data interface{} `json:"data"`
}

func (c *CmsIndexController) Welcome(ctx context.Context, req *CmsIndexWelcomeReq) (res *CmsIndexWelcomeRes, err error) {
	res = &CmsIndexWelcomeRes{
		BaseRes: cool.Ok("Welcome to Cool Admin Go"),
		Data:    gjson.New(`{"name": "Cool Admin Go", "age":0}`),
	}
	return
}

func (c *CmsIndexController) Index(ctx context.Context, req *v1.IndexReq) (res *cool.BaseRes, err error) {

	view := gview.New()
	// 设置模板目录
	view.SetPath("template")
	view.Assigns(map[string]interface{}{
		"name":  "john",
		"age":   18,
		"score": 100,
	})
	// 渲染模板
	content, err := view.Parse(ctx, "index.tpl")
	if err != nil {
		panic(err)
	}

	var r = g.RequestFromCtx(ctx)
	r.Response.Writefln(content)

	return
}

func (c *CmsIndexController) Index2(ctx context.Context, req *v1.Index2Req) (res *cool.BaseRes, err error) {

	var r = g.RequestFromCtx(ctx)
	r.Response.WriteTpl("index.tpl", g.Map{
		"name":  "姓名",
		"age":   18,
		"score": 100,
	})
	return
}
