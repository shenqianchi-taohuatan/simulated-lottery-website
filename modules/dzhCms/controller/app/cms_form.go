package app

import (
	"context"
	v1 "dzhcms/api/v1"
	"dzhcms/modules/dzhCms/service"

	"github.com/cool-team-official/cool-admin-go/cool"

	"github.com/gogf/gf/v2/encoding/gjson"
	"github.com/gogf/gf/v2/frame/g"
)

type CmsFormController struct {
	*cool.Controller
}

func init() {
	var cms_form_controller = &CmsFormController{
		&cool.Controller{
			Perfix:  "/app/cms/form",
			Api:     []string{"Add", "Delete", "Update", "Info", "List", "Page"},
			Service: service.NewCmsFormService(),
		},
	}
	// 注册路由
	cool.RegisterController(cms_form_controller)
}

// 增加 Welcome 演示 方法
type CmsFormWelcomeReq struct {
	g.Meta `path:"/welcome" method:"GET"`
}
type CmsFormWelcomeRes struct {
	*cool.BaseRes
	Data interface{} `json:"data"`
}

func (c *CmsFormController) Welcome(ctx context.Context, req *CmsFormWelcomeReq) (res *CmsFormWelcomeRes, err error) {
	res = &CmsFormWelcomeRes{
		BaseRes: cool.Ok("Welcome to Cool Admin Go"),
		Data:    gjson.New(`{"name": "Cool Admin Go", "age":0}`),
	}
	return
}

func (c *CmsFormController) FormAdd(ctx context.Context, req *v1.FormAddReq) (res *cool.BaseRes, err error) {
	data, err := service.NewCmsFormService().FormAdd(ctx, req)
	if err != nil {
		return
	}
	res = cool.Ok(data)
	return
}
