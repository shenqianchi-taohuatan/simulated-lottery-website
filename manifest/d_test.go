package test

import (
	_ "github.com/gogf/gf/contrib/nosql/redis/v2"
	"github.com/gogf/gf/v2/os/gtime"

	_ "github.com/cool-team-official/cool-admin-go/contrib/drivers/mysql"

	"github.com/gogf/gf/v2/frame/g"

	"testing"
)

func TestPath(t *testing.T) {

	g.Dump(20 * 60)
	g.Dump(gtime.Now().Timestamp() - gtime.New("2023-09-08 18:50:00").Timestamp())
	g.Dump(20*60 - (gtime.Now().Timestamp() - gtime.New("2023-09-08 18:50:00").Timestamp()))
	// g.Dump(gtime.New("2023-09-08 18:50:00").Timestamp())

}
